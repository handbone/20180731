/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class AES256CipherForCLiP {

//    private final static String TAG = "AES256Cipher";

    private final static String KeySpec = "AES";
    private final static String CipherPadding = "AES/CBC/PKCS5Padding";

    private final static byte[] IvParameter = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    public final static String CLIP_AES_KEY = "ktmocawalletpasswordusingz123456";
    public final static String DGPLUS_AES_KEY = "12345678901234567890123456789012";

    public static final String CLIP_AES_KEY_V2 = "ZWCloluqfenq4Dc1pVLJOFBbmnDZyfRV";

    public static byte[] AES_Encode(byte[] ivBytes, byte[] keyBytes,
                                    byte[] textBytes) throws java.io.UnsupportedEncodingException,
            NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {
        AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
        SecretKeySpec newKey = new SecretKeySpec(keyBytes, KeySpec);
        Cipher cipher = null;
        cipher = Cipher.getInstance(CipherPadding);
        cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
        return cipher.doFinal(textBytes);
    }

    public static byte[] AES_Decode(byte[] ivBytes, byte[] keyBytes,
                                    byte[] textBytes) throws java.io.UnsupportedEncodingException,
            NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {
        AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
        SecretKeySpec newKey = new SecretKeySpec(keyBytes, KeySpec);
        Cipher cipher = Cipher.getInstance(CipherPadding);
        cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
        return cipher.doFinal(textBytes);
    }

    /**
     * Encoding
     */
    public static String setAesMsg(String msg) {
        return setAesMsg(msg, CLIP_AES_KEY);
    }

    public static String setAesMsg(byte[] msg) {
        return setAesMsg(msg, CLIP_AES_KEY);
    }

    public static byte[] setAesMsgByte(byte[] msg) {
        return setAesMsgByte(msg, CLIP_AES_KEY);
    }

    public static String setAesMsg(String msg, String key) {
        if (msg == null || msg.length() == 0) {
            return "";
        }

        byte[] byteMsg = null;

        try {
            byteMsg = msg.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (byteMsg == null) {
            return "";
        }
        return setAesMsg(byteMsg, key);
    }

    public static String setAesMsg(byte[] msg, String key) {
        return Base64.encodeBase64String(setAesMsgByte(msg, key));
    }

    public static byte[] setAesMsgByte(byte[] msg, String key) {
        byte[] cipherData = null;

        try {
            cipherData = AES256CipherForCLiP.AES_Encode(IvParameter, key.getBytes(), msg);
        } catch (InvalidKeyException e1) {
            e1.printStackTrace();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (NoSuchPaddingException e1) {
            e1.printStackTrace();
        } catch (InvalidAlgorithmParameterException e1) {
            e1.printStackTrace();
        } catch (IllegalBlockSizeException e1) {
            e1.printStackTrace();
        } catch (BadPaddingException e1) {
            e1.printStackTrace();
        }

        return cipherData;
    }


    /**
     * Decoding
     */

    public static String getAesMsg(String msg) {
        return getAesMsg(msg, CLIP_AES_KEY);
    }

    public static byte[] getAesMsgByte(byte[] msg) {
        return getAesMsgByte(msg, CLIP_AES_KEY);
    }

    public static String getAesMsg(String msg, String key) {
        byte[] str = Base64.decodeBase64(msg);

        return new String(getAesMsgByte(str, key));
    }

    public static byte[] getAesMsgByte(byte[] msg, String key) {

        byte[] cipherData = null;

        try {
            cipherData = AES256CipherForCLiP.AES_Decode(IvParameter, key.getBytes(), msg);
        } catch (InvalidKeyException e1) {
            e1.printStackTrace();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (NoSuchPaddingException e1) {
            e1.printStackTrace();
        } catch (InvalidAlgorithmParameterException e1) {
            e1.printStackTrace();
        } catch (IllegalBlockSizeException e1) {
            e1.printStackTrace();
        } catch (BadPaddingException e1) {
            e1.printStackTrace();
        }

        return cipherData;
    }
}
