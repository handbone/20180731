/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.resources.repositories;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.ktmhows.clip.pointserver.config.exceptions.CommonException;
import com.ktmhows.clip.pointserver.config.exceptions.NotFoundException;
import com.ktmhows.clip.pointserver.resources.mappers.UserInfoMapper;
import com.ktmhows.clip.pointserver.utils.AesEcbCryptoUtil;
import com.ktmhows.clip.pointserver.utils.DateUtil;
import com.ktmhows.clip.pointserver.utils.HttpNetwork;
import com.ktmhows.clip.pointserver.utils.HttpNetworkHTTPS;
import com.ktmhows.clip.pointserver.vo.PointHistVo;
import com.ktmhows.clip.pointserver.vo.PointInfoVo;
import com.ktmhows.clip.pointserver.vo.UserInfoVo;

@Repository
@PropertySource("classpath:config.properties")
public class CLiPPointMapperRepository {

	@Resource(name="sqlSession")
	private SqlSession sqlSession;

	@Resource(name="transactionManager")
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private UserInfoMapper userInfoMapper;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private HttpNetwork httpNetwork;
	
	@Autowired
	private HttpNetworkHTTPS httpNetworkHTTPS;
	
	private String HTTPS_YN;
	
	private String API_PUSH_SEND_YN;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final String mapperNameSpace = "com.ktmhows.clip.pointserver.resources.mappers.";

	
	@PostConstruct
	public void init(){
		
		HTTPS_YN =  Objects.toString(this.env.getProperty("HTTPS_YN"),"Y");
		API_PUSH_SEND_YN =  Objects.toString(this.env.getProperty("API_PUSH_SEND_YN"),"N");
		
		logger.info("HTTPS_YN : {}",HTTPS_YN );
		logger.info("API_PUSH_SEND_YN : {}",API_PUSH_SEND_YN);
	}
	
	public int changePoint(int pointHistIdx, UserInfoVo uiv) throws java.lang.Exception {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		int joinLimitFailCnt = 0;
		TransactionStatus status = transactionManager.getTransaction(def);
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("point_hist_idx", pointHistIdx);
			param.put("status", "W");
			param.put("userCi", uiv.getUserCi());
			
			// 1. 전환 대기중인 포인트 이력 정보 가져오기
			PointHistVo phv = this.sqlSession.selectOne(mapperNameSpace + "PointHistMapper.getPointHist", param);
		
			if(phv == null){
				//transactionManager.rollback(status);
				throw new CommonException("999","전환 대상 포인트 없음!");
			}
			
			// 2. 매체별 적립 허용 횟수를 초과했는지 확인
			param.put("reg_source", phv.getRegSource());
			param.put("pointHistIdx", pointHistIdx);
			
			String point_info_insert = "Y";
			
			// 적립 횟수 제한이 걸려있는 적립건의 처리
			if("Y".equals(phv.getAcl_limit_yn()) ){
				int count = this.sqlSession.selectOne(mapperNameSpace + "PointHistMapper.getPointHistCount", param);
				
				// 3. 이미 적립 횟수를 다 채운경우
				if(  phv.getAcl_limit_cnt() <= count ){
					// 전환 대기중인 포인트 상태 변경 - 플래그값 F로 변경					
					this.sqlSession.update(mapperNameSpace + "PointHistMapper.changePointHistFail", pointHistIdx);
					joinLimitFailCnt++;
				
					point_info_insert = "N";
					
				// 적립횟수가 남아있는 경우 정상적립 후 COUNT테이블에 INSERT
				}else{
					this.sqlSession.update(mapperNameSpace + "PointHistMapper.changePointHist", pointHistIdx);
					phv.setUserCi(uiv.getUserCi());
					this.sqlSession.insert(mapperNameSpace + "PointHistMapper.insertPointHistCount", phv);
				}
			
			// 적립 횟수 제한이 걸려있지 않은 적립건의 처리
			}else{
				// 3. 전환 대기중인 포인트 상태 변경
				this.sqlSession.update(mapperNameSpace + "PointHistMapper.changePointHist", pointHistIdx);
				
			}
			
			if("Y".equals(point_info_insert)){
				// 4. 현재 최종 포인트 정보 가져오기
				PointInfoVo lastPiv = this.sqlSession.selectOne(mapperNameSpace + "PointInfoMapper.getPointInfo", uiv.getUserCi());
				long lastBalance = 0;
				
				if(lastPiv != null){
					lastBalance = lastPiv.getBalance();
				}
	
				this.logger.debug("last balance = " + lastBalance);
				
				// 5. 포인트 정보 입력
				PointInfoVo piv = new PointInfoVo();
				piv.setUserCi(uiv.getUserCi());
				piv.setPointHistIdx(pointHistIdx);
				piv.setPointType(phv.getPointType());
				piv.setPointValue(phv.getPointValue());
				if(StringUtils.equals(phv.getPointType(), "I")){
					piv.setBalance(lastBalance + phv.getPointValue());
				} else if(StringUtils.equals(phv.getPointType(), "O")){
					piv.setBalance(lastBalance - phv.getPointValue());
				}
				piv.setDescription(phv.getDescription());
				piv.setRegSource(phv.getRegSource());
				
				this.sqlSession.insert(mapperNameSpace + "PointInfoMapper.insertPointInfo", piv);

				if("Y".equals(API_PUSH_SEND_YN)) {
					if("Y".equals(HTTPS_YN)){
						httpNetworkHTTPS.sendPush(piv,uiv);
					}else{
						httpNetwork.sendPush(piv,uiv);
					}
				}	
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			transactionManager.rollback(status);
			throw ex;
		}
		transactionManager.commit(status);
		return joinLimitFailCnt;
	}
	
	/** 
	 *  getPointRefresh 로  임시포인트 적립/차감 발생시 실행 
	 */
	public int changePointRefresh(int pointHistIdx, UserInfoVo uiv) throws java.lang.Exception {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		int joinLimitFailCnt = 0;
		TransactionStatus status = transactionManager.getTransaction(def);
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("point_hist_idx", pointHistIdx);
			param.put("status", "W");
			param.put("userCi", uiv.getUserCi());
			
			// 1. 전환 대기중인 포인트 이력 정보 가져오기
			PointHistVo phv = this.sqlSession.selectOne(mapperNameSpace + "PointHistMapper.getPointHist", param);
		
			if(phv == null){
				//transactionManager.rollback(status);
				throw new CommonException("999","전환 대상 포인트 없음!");
			}
			
			// 2. 매체별 적립 허용 횟수를 초과했는지 확인
			param.put("reg_source", phv.getRegSource());
			param.put("pointHistIdx", pointHistIdx);
			
			String point_info_insert = "Y";
			
			// 적립 횟수 제한이 걸려있는 적립건의 처리
			if("Y".equals(phv.getAcl_limit_yn()) ){
				int count = this.sqlSession.selectOne(mapperNameSpace + "PointHistMapper.getPointHistCount", param);
				
				// 3. 이미 적립 횟수를 다 채운경우
				if(  phv.getAcl_limit_cnt() <= count ){
					// 전환 대기중인 포인트 상태 변경 - 플래그값 F로 변경					
					this.sqlSession.update(mapperNameSpace + "PointHistMapper.changePointHistFail", pointHistIdx);
					joinLimitFailCnt++;
				
					point_info_insert = "N";
					
				// 적립횟수가 남아있는 경우 정상적립 후 COUNT테이블에 INSERT
				}else{
					this.sqlSession.update(mapperNameSpace + "PointHistMapper.changePointHist", pointHistIdx);
					phv.setUserCi(uiv.getUserCi());
					this.sqlSession.insert(mapperNameSpace + "PointHistMapper.insertPointHistCount", phv);
				}
			
			// 적립 횟수 제한이 걸려있지 않은 적립건의 처리
			}else{
				// 3. 전환 대기중인 포인트 상태 변경
				this.sqlSession.update(mapperNameSpace + "PointHistMapper.changePointHist", pointHistIdx);
				
			}
			
			if("Y".equals(point_info_insert)){
				// 4. 현재 최종 포인트 정보 가져오기
				PointInfoVo lastPiv = this.sqlSession.selectOne(mapperNameSpace + "PointInfoMapper.getPointInfo", uiv.getUserCi());
				long lastBalance = 0;
				
				if(lastPiv != null){
					lastBalance = lastPiv.getBalance();
				}
	
				this.logger.debug("last balance = " + lastBalance);
				
				// 5. 포인트 정보 입력
				PointInfoVo piv = new PointInfoVo();
				piv.setUserCi(uiv.getUserCi());
				piv.setPointHistIdx(pointHistIdx);
				piv.setPointType(phv.getPointType());
				piv.setPointValue(phv.getPointValue());
				if(StringUtils.equals(phv.getPointType(), "I")){
					piv.setBalance(lastBalance + phv.getPointValue());
				} else if(StringUtils.equals(phv.getPointType(), "O")){
					piv.setBalance(lastBalance - phv.getPointValue());
				}
				piv.setDescription(phv.getDescription());
				piv.setRegSource(phv.getRegSource());
				
				this.sqlSession.insert(mapperNameSpace + "PointInfoMapper.insertPointInfo", piv);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			transactionManager.rollback(status);
			throw ex;
		}
		transactionManager.commit(status);
		return joinLimitFailCnt;
	}
	

	@SuppressWarnings("unused")
	public UserInfoVo exitUser(Map<String, String> parameters) throws java.lang.Exception {
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

		TransactionStatus status = transactionManager.getTransaction(def);
		UserInfoVo uiv = null;
		PointInfoVo piv = null;
		
		try {
			
			uiv = this.sqlSession.selectOne(mapperNameSpace + "UserInfoMapper.getUserInfo", parameters);
			
			if(uiv == null){
				throw new NotFoundException("404", "사용자 확인 불가!");
			}
			
			// String exitCi = AesEcbCryptoUtil.encryptECB(parameters.get("ctn") + DateUtil.getDate("yyyyMMddHHmmss"));
			// 파라미터로 받고있지 않는 ctn으로 암호를 만들기 때문에 null값이 들어간다.
			// 사용자 정보의 ctn으로 변경한다.
			String exitCi = AesEcbCryptoUtil.encryptECB(uiv.getCtn() + DateUtil.getDate("yyyyMMddHHmmss"));
			
			uiv.setExitCi(exitCi);
			uiv.setExitDate(DateUtil.getDate("yyyyMMdd"));
			uiv.setExitTime(DateUtil.getDate("HHmmss"));
			
			// 회원탈퇴시에 사용자가 그동안 적립했던 포인트를 다음에 다시 가입 한 후에도 유지하려면 piv를 그냥 null로 유지하고
			// 적립했던 포인트를 없애려면 아래를 사용한다.
			if(null != parameters.get("end_gbn") && !"".equals(parameters.get("end_gbn"))) {
				if(parameters.get("end_gbn").equalsIgnoreCase("Y")) {	// 최종 탈퇴 여부
					piv = this.sqlSession.selectOne(mapperNameSpace + "PointInfoMapper.getPointInfo", parameters.get("user_ci"));
				}
			}
			// 통계 부분에서  make_daily_remove 프로시저의 p.user_ci = u.exit_ci 부분을 상황에 맞게 변경해준다.
			
			if(this.sqlSession.update(mapperNameSpace + "UserInfoMapper.exitUserInfo", uiv) > 0){
				
				uiv.setUpdate_gbn("D");
				this.userInfoMapper.insertUserInfoLog(uiv);
				
				// Y 일 경우 로직 수행, 기존 userCi 값을 exitCi 값으로 치환
				if(piv != null){
					if(this.sqlSession.update(mapperNameSpace + "UserInfoMapper.exitUserFinal", uiv) > 0) {
						if(this.sqlSession.update(mapperNameSpace + "PointInfoMapper.updatePointInfoForExitUser", uiv) > 0){
							transactionManager.commit(status);
							return uiv;
						} else {
							transactionManager.rollback(status);
							throw new CommonException("801","탈퇴 후 탈퇴자에게 적립 된 포인트 정보의 user_ci값을 exit값으로 치환시 에러");
						}
					}else {
						transactionManager.rollback(status);
						throw new CommonException("803","완전탈퇴 후 탈퇴자 status를 F값으로 치환시 에러");
					}
				} else {
					transactionManager.commit(status);
					return uiv;
				}
			} else {
				transactionManager.rollback(status);
				throw new CommonException("802","사용자는 있으나 탈퇴 실패");
			}	
		} catch (Exception ex) {
			ex.printStackTrace();
			transactionManager.rollback(status);
			throw ex;
		}
	}
}
