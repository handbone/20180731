/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.utils;

import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

public class AesEcbCryptoUtil {
	private static final String key = "b5a8b5823616b3300d4f4b5e43cb5fa0";
	
	/**
	 * AES ECB ENCRYPT
	 * 256bit, 16byte, PKCS5Padding
	 * @param sKey
	 * @param sText
	 * @return
	 */
	 private static byte[] aesEncryptEcb(String sKey, String sText) {
        byte[] key = null;
        byte[] text = null;
        byte[] encrypted = null;
        final int AES_KEY_SIZE = 256;
 
        try {
            // UTF-8
            key = sKey.getBytes("UTF-8");
 
            // Key size 맞춤 (256bit, 16byte)
            key = Arrays.copyOf(key, AES_KEY_SIZE / 16);
 
            // UTF-8
            text = sText.getBytes("UTF-8");
 
            // AES/EBC/PKCS5Padding
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"));
            encrypted = cipher.doFinal(text);
        } catch (Exception e) {
            encrypted = null;
            e.printStackTrace();
        }
 
        return encrypted;
    }
	
	 
	/**
	 *  AES ECB DECRYPT
	 * 256bit, 16byte, PKCS5Padding
	 * @param sKey
	 * @param encrypted
	 * @return
	 */
	 private static byte[] aesDecryptEcb(String sKey, byte[] encrypted) {
        byte[] key = null;
        byte[] decrypted = null;
        final int AES_KEY_SIZE = 256;
 
        try {
            // UTF-8
            key = sKey.getBytes("UTF-8");
 
            // Key size 맞춤 (128bit, 16byte)
            key = Arrays.copyOf(key, AES_KEY_SIZE / 16);
 
            // AES/EBC/PKCS5Padding
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"));
            
            decrypted = cipher.doFinal(encrypted);
        } catch (Exception e) {
            decrypted = null;
            e.printStackTrace();
        }
 
        return decrypted;
    }
	 
	public static String toHexString(byte[] b) {
        StringBuffer sb = new StringBuffer();
 
        for (int i = 0; i < b.length; i++) {
            sb.append(String.format("%02X", b[i]));
            if ((i + 1) % 16 == 0 && ((i + 1) != b.length)) {
                sb.append(" ");
            	//sb.append("+");
            }
        }
 
        return sb.toString();
    }
	
	public static byte[] hexToByteArray(String hex) {
		
		if (hex == null || hex.length() == 0) {
	        return null;
	    }
		byte[] ba = new byte[hex.length() / 2];
	 
	    for (int i = 0; i < ba.length; i++) {
	        ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
	    }
		return ba;
	}
	
	//+ 제외 되는 현상 보정
	public static String plusReplace(String str) {
		String tempStr = "";
		if (StringUtils.isNotEmpty(str)){
			tempStr = str.replaceAll(" ", "+");
		}
		return tempStr;
	}
	
	public static String checkPhoneNo(String authKey, String phoneNo){
		
		byte[] requestBytes = null;
		byte[] decrypted = null;
		String decryptedData = null;
		
		try {
			
			requestBytes = Base64.decodeBase64(phoneNo);
			decrypted = aesDecryptEcb(authKey, requestBytes);
			
			if (decrypted != null) {
            	decryptedData = new String(decrypted, "UTF-8");
            }
			
		} catch (Exception e) {
			e.printStackTrace();
			decryptedData = null;
		}
		
		return decryptedData;
	}
	
	public static String encryptECB(String text){
		try{
			return Base64.encodeBase64String(aesEncryptEcb(key, text));
		} catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}

	public static String decryptECB(String text){
		try{
			
			String tText = plusReplace(text);
			byte[] sText = Base64.decodeBase64(tText);
			
			return new String(aesDecryptEcb(key, sText));
		} catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
//    public static void main(String[] args){
//        String sKey = "b5a8b5823616b3300d4f4b5e43cb5fa0";
//
//        String sText = "15886474";
//        byte[] encrypted = null;
//        byte[] decrypted = null;
// 
//        try {
//            System.out.println("* AES/ECB");
//            System.out.println("    - KEY : " + sKey);
//            System.out.println("    - TEXT : " + sText);
// 
//            // AES/ECB 암호화
//            encrypted = aesEncryptEcb(sKey, sText);
// 
//            if (encrypted == null) {
//                System.out.println("    - Encrypted : ERROR!!!");
//            } else {
////                System.out.println("    - Encrypted : " + toHexString(encrypted));
//            	System.out.println("    - Encrypted : " + Base64.encodeBase64String(encrypted));
//            }
//            
//            
//            // AES/ECB 복호화
//            decrypted = aesDecryptEcb(sKey, encrypted);
//            
//            if (decrypted == null) {
//                System.out.println("    - Decrypted : ERROR!!!");
//            } else {
//                System.out.println("    - Decrypted : " + new String(decrypted, "UTF-8"));
//            }
//            
//            
//            /*
//            String foo = toHexString(encrypted);
//            byte[] bytes = foo.getBytes();
//            String hexFoo = Hex.encodeHexString( bytes );
//            System.out.println("==>"+hexFoo);
//            */
//            
//            String aa = Base64.encodeBase64String(encrypted);
//            
//            //byte[] bytes2 = Base64.decodeBase64(aa.getBytes());
//            byte[] bytes2 = Base64.decodeBase64(aa);
//            decrypted = aesDecryptEcb(sKey, bytes2);
//            System.out.println("    - Decrypted : " + new String(decrypted, "UTF-8"));
//            
//            
//            /*
//            String sInitVector = "123가나다";
//            System.out.println("* AES/CBC");
//            System.out.println("    - KEY : " + sKey);
//            System.out.println("    - TEXT : " + sText);
//            System.out.println("    - IV : (Empty)");
//            
//            // AES/CBC 암호화
//            encrypted = aesEncryptCbc(sKey, sText);
// 
//            // AES/CBC 복호화
//            decrypted = aesDecryptCbc(sKey, encrypted);
// 
//            if (encrypted == null) {
//                System.out.println("    - Encrypted : ERROR!!!");
//            } else {
//                System.out.println("    - Encrypted : " + toHexString(encrypted));
//            }
// 
//            if (decrypted == null) {
//                System.out.println("    - Decrypted : ERROR!!!");
//            } else {
//                System.out.println("    - Decrypted : " + new String(decrypted, "UTF-8"));
//            }
// 
//            System.out.println("* AES/CBC/IV");
//            System.out.println("    - KEY : " + sKey);
//            System.out.println("    - TEXT : " + sText);
//            System.out.println("    - IV : " + sInitVector);
// 
//            // AES/CBC/IV 암호화
//            encrypted = aesEncryptCbc(sKey, sText, sInitVector);
// 
//            // AES/CBC/IV 복호화
//            encrypted = aesEncryptCbc(sKey, sText, sInitVector);
// 
//            if (encrypted == null) {
//                System.out.println("    - Encrypted : ERROR!!!");
//            } else {
//                System.out.println("    - Encrypted : " + toHexString(encrypted));
//            }
// 
//            if (decrypted == null) {
//                System.out.println("    - Decrypted : ERROR!!!");
//            } else {
//                System.out.println("    - Decrypted : " + new String(decrypted, "UTF-8"));
//            }
//            */
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
