package com.ktmhows.clip.pointserver.services;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ktmhows.clip.pointserver.resources.mappers.UserInfoMapper;
import com.ktmhows.clip.pointserver.utils.DateUtil;

@Service
public class VipLoungeService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserInfoMapper userInfoMapper;
	
	public String checkVip(Map<String, String> parameters) throws Exception{
		StringBuffer sbLog = new StringBuffer();
		String cust_id = parameters.get("cust_id");
		String ret = userInfoMapper.checkVipByLockScreen(cust_id);
		
		String result = "";
		if(StringUtils.isEmpty(ret)) {
			result = "N";
		} else {
			result = "Y";
		}

		sbLog.append("VipLoungeService : check result : "+result+", "+ DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");		
		logger.debug(sbLog.toString());
		
		return result;
		
	}
}
