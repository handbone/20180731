/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ktmhows.clip.pointserver.config.exceptions.BadRequestException;
import com.ktmhows.clip.pointserver.config.exceptions.CommonException;
import com.ktmhows.clip.pointserver.config.exceptions.NotFoundException;
import com.ktmhows.clip.pointserver.config.exceptions.UnAuthorizedException;
import com.ktmhows.clip.pointserver.resources.mappers.ACLInfoMapper;
import com.ktmhows.clip.pointserver.services.CLiPPointService;
import com.ktmhows.clip.pointserver.utils.HttpNetwork;
import com.ktmhows.clip.pointserver.utils.HttpNetworkHTTPS;
import com.ktmhows.clip.pointserver.vo.ACLInfoVo;
import com.ktmhows.clip.pointserver.vo.PointHistVo;
import com.ktmhows.clip.pointserver.vo.PointInfoVo;
import com.ktmhows.clip.pointserver.vo.UserInfoVo;

@Controller
@PropertySource("classpath:config.properties")
@SuppressWarnings("unchecked")
public class CLiPPointController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CLiPPointService clipPointService;
	
	@Autowired
	private HttpNetwork httpNetwork;
	
	@Autowired
	private HttpNetworkHTTPS httpNetworkHTTPS;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private ACLInfoMapper aclInfoMapper;
	

	@RequestMapping(
			value="getPointHistory.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String getPointHistory(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : user_ci");
		}

		if(!parameters.containsKey("start_date") || StringUtils.isEmpty(parameters.get("start_date"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : start_date");
		}

		if(!parameters.containsKey("end_date") || StringUtils.isEmpty(parameters.get("end_date"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : end_date");
		}
		
		UserInfoVo uiv = this.clipPointService.getUserInfo(parameters.get("user_ci"));
		
		if(uiv == null){
			throw new NotFoundException("404", "사용자 정보 없음!");
		}
		
		List<PointInfoVo> pointInfoList = this.clipPointService.getPointInfoList(parameters);
		
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("user_ci", parameters.get("user_ci"));
        jsonObj.put("start_date", parameters.get("start_date"));
        jsonObj.put("end_date", parameters.get("end_date"));

        JSONArray jsonPointArray = new JSONArray();
        if(pointInfoList != null && pointInfoList.size() > 0){
            for(int i=0; i<pointInfoList.size(); i++){
            	JSONObject jobj = new JSONObject();

            	jobj.put("datetime", pointInfoList.get(i).getRegDate() + pointInfoList.get(i).getRegTime());
            	jobj.put("point_type", pointInfoList.get(i).getPointType());
            	jobj.put("point_value", pointInfoList.get(i).getPointValue());
            	jobj.put("balance", pointInfoList.get(i).getBalance());
            	jobj.put("description", pointInfoList.get(i).getDescription());

            	jsonPointArray.put(jobj);
            }
        }
        jsonObj.put("history", jsonPointArray);
        
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}
	
	@RequestMapping(
			value="getPointHistoryNew.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String getPointHistoryNew(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : user_ci");
		}

		if(!parameters.containsKey("start_date") || StringUtils.isEmpty(parameters.get("start_date"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : start_date");
		}

		if(!parameters.containsKey("end_date") || StringUtils.isEmpty(parameters.get("end_date"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : end_date");
		}
		
		UserInfoVo uiv = this.clipPointService.getUserInfo(parameters.get("user_ci"));
		
		if(uiv == null){
			throw new NotFoundException("404", "사용자 정보 없음!");
		}
		
		List<PointInfoVo> pointInfoList = this.clipPointService.getPointInfoList(parameters);
		
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("user_ci", parameters.get("user_ci"));
        jsonObj.put("start_date", parameters.get("start_date"));
        jsonObj.put("end_date", parameters.get("end_date"));

        JSONArray jsonPointArray = new JSONArray();
        if(pointInfoList != null && pointInfoList.size() > 0){
            for(int i=0; i<pointInfoList.size(); i++){
            	JSONObject jobj = new JSONObject();

            	jobj.put("reg_source", pointInfoList.get(i).getRegSource());
            	jobj.put("datetime", pointInfoList.get(i).getRegDate() + pointInfoList.get(i).getRegTime());
            	jobj.put("point_type", pointInfoList.get(i).getPointType());
            	jobj.put("point_value", pointInfoList.get(i).getPointValue());
            	jobj.put("balance", pointInfoList.get(i).getBalance());
            	jobj.put("description", pointInfoList.get(i).getDescription());

            	jsonPointArray.put(jobj);
            }
        }
        jsonObj.put("history", jsonPointArray);
        
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}

	@RequestMapping(
			value="getPoint.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String getPoint(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : user_ci");
		}

		PointInfoVo piv = this.clipPointService.getPointInfo(parameters);

        JSONObject jsonObj = new JSONObject();
		jsonObj.put("user_ci", piv.getUserCi());
		jsonObj.put("balance", piv.getBalance());

		if( piv != null && piv.getLog() !=null ) {
			log.append(piv.getLog());
		
		}
		
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}


	@RequestMapping(
			value="getPointRefresh.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String getPointRefresh(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");
		
		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : user_ci");
		}

		PointInfoVo piv = this.clipPointService.getPointRefresh(parameters);

        JSONObject jsonObj = new JSONObject();
		jsonObj.put("user_ci", piv.getUserCi());
		jsonObj.put("balance", piv.getBalance());
		//jsonObj.put("joinLimitFailCnt", piv.getJoinLimitFailCnt());
		if(piv.getJoinLimitFailCnt() > 0){
			jsonObj.put("description", piv.getDescription());
		}else{
			jsonObj.put("description", "");
		}
		
		
		log.append("piv.getJoinLimitFailCnt()"+piv.getJoinLimitFailCnt()+"\r\n");
		log.append("description"+piv.getDescription()+"\r\n");
		log.append(piv.getLog()+"\r\n");
		
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}

	/**
	 * 외부 연동 등 일반적으로 쓰이는 url
	 * @param request
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(
			value="plusPoint.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String plusPoint(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		// ACL 체크
		if(!parameters.containsKey("requester_code") || StringUtils.isEmpty(parameters.get("requester_code"))){
			throw new UnAuthorizedException("401", "요청자 코드 값이 없음.");
		} else {
			if(!this.clipPointService.checkIP(parameters.get("requester_code"), request.getRemoteAddr())){
				throw new UnAuthorizedException("401", "잘못된 IP 주소.");
			}
		}
		
		if((!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))) &&
				(!parameters.containsKey("cust_id") || StringUtils.isEmpty(parameters.get("cust_id"))) &&
				(!parameters.containsKey("ctn") || StringUtils.isEmpty(parameters.get("ctn"))) &&
				(!parameters.containsKey("ga_id") || StringUtils.isEmpty(parameters.get("ga_id")))){
			throw new BadRequestException("400", "필수 파라미터 부재! : user_ci or cust_id or ctn or ga_id");
		}

		if(!parameters.containsKey("transaction_id") || StringUtils.isEmpty(parameters.get("transaction_id"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : transaction_id");
		}

		if(!parameters.containsKey("point_value") || StringUtils.isEmpty(parameters.get("point_value")) || Long.valueOf(parameters.get("point_value")) < 1){
			throw new BadRequestException("400", "필수 파라미터 부재! : point_value");
		}

		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", parameters.get("requester_code"));
		
		ACLInfoVo aiv = this.aclInfoMapper.getACLInfo(parameters.get("reg_source"));
		parameters.put("acl_limit_yn", aiv.acl_limit_yn);
		parameters.put("acl_limit_cnt", aiv.acl_limit_cnt);
		
		PointHistVo phv = null;
		try {
			phv = this.clipPointService.plusPoint(parameters);
		} catch (Exception e) {
			logger.error("",e);
			phv =null;
		}
		
		if(phv == null){
			throw new CommonException("601", "입력 실패!");
		}
		
        JSONObject jsonObj = new JSONObject();
        if(!StringUtils.isEmpty(phv.getUserCi())){
            jsonObj.put("user_ci", phv.getUserCi());
        }
        
        if(!StringUtils.isEmpty(phv.getCustId())){
        	jsonObj.put("cust_id", phv.getCustId());
        }

        if(!StringUtils.isEmpty(phv.getCtn())){
        	jsonObj.put("ctn", phv.getCtn());
        }

        if(!StringUtils.isEmpty(phv.getGaId())){
        	jsonObj.put("ga_id", phv.getGaId());
        }
        
        jsonObj.put("transaction_id", phv.getTransactionId());
        jsonObj.put("datetime", phv.getRegDate() + phv.getRegTime());
        if( phv.getJoinLimitFailCnt() > 0){
        	phv.setPointValue(0);
		}
        jsonObj.put("point_value", phv.getPointValue());
        jsonObj.put("description", phv.getDescription());
		
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}

	@RequestMapping(
			value="minusPoint.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String minusPoint(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		// ACL 체크
		if(!parameters.containsKey("requester_code") || StringUtils.isEmpty(parameters.get("requester_code"))){
			throw new UnAuthorizedException("401", "요청자 코드 값이 없음.");
		} else {
			if(!this.clipPointService.checkIP(parameters.get("requester_code"), request.getRemoteAddr())){
				throw new UnAuthorizedException("401", "잘못된 IP 주소.");
			}
		}
		
		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : user_ci");
		}

		if(!parameters.containsKey("transaction_id") || StringUtils.isEmpty(parameters.get("transaction_id"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : transaction_id");
		}

		if(!parameters.containsKey("point_value") || StringUtils.isEmpty(parameters.get("point_value")) || Long.valueOf(parameters.get("point_value")) < 1){
			throw new BadRequestException("400", "필수 파라미터 부재! : point_value");
		}
		
		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", parameters.get("requester_code"));
		
		PointInfoVo piv = null;
		
		try {
			piv = this.clipPointService.minusPoint(parameters);
		} catch (Exception e) {
			logger.error("",e);
			piv =null;
		}
		
		if(piv == null){
			throw new CommonException("602", "입력 실패!");
		}
		
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("user_ci", piv.getUserCi());
        jsonObj.put("transaction_id", parameters.get("transaction_id"));
        jsonObj.put("datetime", piv.getRegDate() + piv.getRegTime());
        jsonObj.put("point_value", piv.getPointValue());
        jsonObj.put("balance", piv.getBalance());
        jsonObj.put("description", piv.getDescription());

		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}

	@RequestMapping(
			value="exitUser.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String exitUser(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : user_ci");
		}

//		if(!parameters.containsKey("cust_id") || StringUtils.isEmpty(parameters.get("cust_id"))){
//			throw new BadRequestException("400", "필수 파라미터 부재! : cust_id");
//		}
//
//		if(!parameters.containsKey("ctn") || StringUtils.isEmpty(parameters.get("ctn"))){
//			throw new BadRequestException("400", "필수 파라미터 부재! : ctn");
//		}
//
//		if(!parameters.containsKey("ga_id") || StringUtils.isEmpty(parameters.get("ga_id"))){
//			throw new BadRequestException("400", "필수 파라미터 부재! : ga_id");
//		}

//		if(!parameters.containsKey("end_gbn") || StringUtils.isEmpty(parameters.get("end_gbn"))){	// 최종 탈퇴 여부
//			throw new BadRequestException("400", "필수 파라미터 부재! : end_gbn");
//		}

		
		UserInfoVo uiv = this.clipPointService.exitUser(parameters);
		
		if(uiv == null){
			throw new CommonException("701", "탈퇴 실패!");
		}
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("user_ci", uiv.getExitCi());

		PointInfoVo piv = this.clipPointService.getPointInfo(params);
		
		if(piv == null){
			throw new CommonException("702", "탈퇴 후 조회 실패!");
		}
		
		long last_point_value = piv.getBalance();
		
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("user_ci", uiv.getUserCi());
        jsonObj.put("cust_id", uiv.getCustId());
        jsonObj.put("ctn", uiv.getCtn());
        jsonObj.put("ga_id", uiv.getGaId());
        jsonObj.put("datetime", uiv.getExitDate() + uiv.getExitTime());
        jsonObj.put("last_point_value", last_point_value);

		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}
	
	
	
	
	
	@RequestMapping(
			value="getPlusCount.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String getPlusCount(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");


		// ACL 체크
		if(!parameters.containsKey("requester_code") || StringUtils.isEmpty(parameters.get("requester_code"))){
			throw new UnAuthorizedException("401", "요청자 코드 값이 없음.");
		} else {
			if(!this.clipPointService.checkIP(parameters.get("requester_code"), request.getRemoteAddr())){
				throw new UnAuthorizedException("401", "잘못된 IP 주소.");
			}
		}

		if(!parameters.containsKey("cust_id") || StringUtils.isEmpty(parameters.get("cust_id"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : cust_id");
		}

		if(!parameters.containsKey("ctn") || StringUtils.isEmpty(parameters.get("ctn"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : ctn");
		}

		//================================================================================================
		String user_ci = "";
		String result_msg = "";
		String strHtmlSource ="";
		
		
		String cust_id = parameters.get("cust_id");
		String ctn = parameters.get("ctn");
		String requester_code = parameters.get("requester_code");
		String cert_type ="1";
		//String decCtn = AES256CipherClip.AES_Decode(ctn);
		
		JSONObject jObject = new JSONObject();
		jObject.put("cust_id", cust_id);
		jObject.put("service_id", requester_code);
		jObject.put("cert_type", cert_type);
		jObject.put("cert_key", ctn);
		
		String params = jObject.toString();
		
		String result_code = "";
		
		try {
			
			//=================================== 테스트 용으로 json 타입으로 변형 =======================================
			//		JSONObject jo = new JSONObject();
			//		jo.put("ci", "6RjRO9j1ex9utXGo3QAEnrOOaON+AHAVz7t2PM85PBFk4bPhAdZ6L/YcqLo8nqdDR1iv8NgYtAG3FgT6IfOCtg==");
			//		jo.put("msg", "SUCCESS");
			//		jo.put("result", "OK");
			//============================================================================================================
			
			if("Y".equals(this.env.getProperty("HTTPS_YN"))){
				strHtmlSource = httpNetworkHTTPS.strGetUserCi(params);
			}else{
				strHtmlSource = httpNetwork.strGetUserCi(params);
			}
			
			
			log.append("params : " + params + "\r\n");
			//테스트용
			//strHtmlSource = jo.toString();
			
			log.append("strHtmlSource(Success) : " + strHtmlSource + "\r\n");
	        JSONParser jsonParser = new JSONParser();
	    	JSONObject jsonObject = (JSONObject) jsonParser.parse(strHtmlSource);
	    	result_msg = jsonObject.get("msg").toString();
	    	result_code = jsonObject.get("result").toString();
	    	
	    	if(result_msg.equals("SUCCESS")){
	    		user_ci = jsonObject.get("ci").toString();
	    	}
	    	
		} catch (Exception e ){
			//e.getMessage();
			log.append("strHtmlSource(Fail) : " + e.getMessage() + "\r\n");
			throw new CommonException("500", "서버 내부 에러. 관리자에게 문의하세요. "); // // 요청자는 500번 에러를 받는다.
		}
		
		if("-101".equals(result_code)){
    		throw new BadRequestException("401", "KT CLIP 서버에서 cert_key 복호화 에러"); // 요청자는 400번 에러를 받는다.
    	}else if("-103".equals(result_code)){
    		throw new NotFoundException("402", "KT CLIP 서버에 사용자 정보 없음 or 사용자 정보에 CI값이 없음"); // 요청자는 404번 에러를 받는다.
    	}else if("-500".equals(result_code)){
    		throw new CommonException("403", "KT CLIP 서버 내부 에러"); // // 요청자는 500번 에러를 받는다.
    	}
		 
		//================================================================================================

		
		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", parameters.get("requester_code"));
		
		// 받아온 user_ci 값을 파라미터에 "user_ci" 값으로 등록
		parameters.put("user_ci", user_ci);
		
		//parameters.put("user_ci", getUserCi.getUserCi());
		int plusCount = this.clipPointService.getPlusCount(parameters);
		
	    JSONObject jsonObj = new JSONObject();

	    jsonObj.put("cust_id", parameters.get("cust_id"));
		jsonObj.put("ctn", parameters.get("ctn"));
		jsonObj.put("count", plusCount);
		
		log.append("return String : " + jsonObj.toString() + "\r\n");
		
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}
	
	
}
