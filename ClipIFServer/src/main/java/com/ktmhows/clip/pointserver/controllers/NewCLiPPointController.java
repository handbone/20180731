/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */
package com.ktmhows.clip.pointserver.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ktmhows.clip.pointserver.config.exceptions.BadRequestException;
import com.ktmhows.clip.pointserver.config.exceptions.CommonException;
import com.ktmhows.clip.pointserver.config.exceptions.NotFoundException;
import com.ktmhows.clip.pointserver.config.exceptions.UnAuthorizedException;
import com.ktmhows.clip.pointserver.resources.mappers.ACLInfoMapper;
import com.ktmhows.clip.pointserver.services.CLiPPointService;
import com.ktmhows.clip.pointserver.services.VipLoungeService;
import com.ktmhows.clip.pointserver.vo.ACLInfoVo;
import com.ktmhows.clip.pointserver.vo.PointHistVo;
import com.ktmhows.clip.pointserver.vo.PointInfoVo;
import com.ktmhows.clip.pointserver.vo.UserInfoVo;

@Controller
@RequestMapping("/adv")
@PropertySource("classpath:config.properties")
@SuppressWarnings("unchecked")
public class NewCLiPPointController {
	
	@Autowired
	private CLiPPointService clipPointService;

	@Autowired
	private ACLInfoMapper aclInfoMapper;
	
	@Autowired
	private VipLoungeService vipLoungeService;
	
	private final String PAGE_SIZE ="20";
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	
	@RequestMapping(
			value="/getPointHistoryNew.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String getPointHistory(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");
		

		Map<String, String> parameters = (Map<String,String>)request.getAttribute("parametersMap");
		PointInfoVo historyParamVo = new PointInfoVo();
		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : userCi");
		}

		if(!parameters.containsKey("start_date") || StringUtils.isEmpty(parameters.get("start_date"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : start_date");
		}

		if(!parameters.containsKey("end_date") || StringUtils.isEmpty(parameters.get("end_date"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : end_date");
		}
		
		if(!parameters.containsKey("page_num") || StringUtils.isEmpty(parameters.get("page_num"))){
			parameters.put("page_num", "0");
		}
		
		if(!parameters.containsKey("page_size") || StringUtils.isEmpty(parameters.get("page_size"))){
			parameters.put("page_size", PAGE_SIZE );
		}
		
		UserInfoVo uiv = this.clipPointService.getUserInfo(parameters.get("user_ci"));
		
		if(uiv == null){
			throw new NotFoundException("404", "사용자 정보 없음!");
		}

		BeanUtils.populate(historyParamVo,parameters);
		historyParamVo.paingSetting();
		
		List<PointInfoVo> pointInfoList = this.clipPointService.getNewPointInfoList(historyParamVo);
		
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("user_ci", parameters.get("user_ci"));
        jsonObj.put("start_date", parameters.get("start_date"));
        jsonObj.put("end_date", parameters.get("end_date"));
        
        if(pointInfoList != null && pointInfoList.size() > 0 ) {
 			jsonObj.put("page_list_size", pointInfoList.size());
	    } else {
	     	jsonObj.put("page_list_size", 0);
	    }

        JSONArray jsonPointArray = new JSONArray();
        if(pointInfoList != null && pointInfoList.size() > 0){
            for(int i=0; i<pointInfoList.size(); i++){
            	JSONObject jobj = new JSONObject();

            	jobj.put("reg_source", pointInfoList.get(i).getRegSource());
            	jobj.put("datetime", pointInfoList.get(i).getRegDate() + pointInfoList.get(i).getRegTime());
            	jobj.put("point_type", pointInfoList.get(i).getPointType());
            	jobj.put("point_value", pointInfoList.get(i).getPointValue());
            	jobj.put("balance", pointInfoList.get(i).getBalance());
            	jobj.put("description", pointInfoList.get(i).getDescription());

            	jsonPointArray.put(jobj);
            }
        }
        jsonObj.put("history", jsonPointArray);
        
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}

	@RequestMapping(
			value="/getPoint.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String getPoint(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token"))) {
				throw new BadRequestException("400", "필수 파라미터 부재! : user_ci or user_token");
			} else {
				String userCi = clipPointService.getUserCiByUserToken(parameters.get("user_token"));
				if(userCi == null)
					throw new NotFoundException("404", "사용자 정보 없음!");
				else
					parameters.put("user_ci", userCi);
			}
		}

		PointInfoVo piv = this.clipPointService.getPointInfo(parameters);

        JSONObject jsonObj = new JSONObject();
        if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token")))
        	jsonObj.put("user_ci", parameters.get("user_ci"));
        else
        	jsonObj.put("user_token", parameters.get("user_token"));
        
		jsonObj.put("balance", piv.getBalance());
		log.append(piv.getLog());

		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}

	// 포인트 조회 시 유저가 없는 경우 가입하지 않는 함수
	@RequestMapping(
			value="/getPoint2.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String getPoint2(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token"))) {
				throw new BadRequestException("400", "필수 파라미터 부재! : user_ci or user_token");
			} else {
				String userCi = clipPointService.getUserCiByUserToken(parameters.get("user_token"));
				if(userCi == null)
					throw new NotFoundException("404", "사용자 정보 없음!");
				else
					parameters.put("user_ci", userCi);
			}
		}

		PointInfoVo piv = this.clipPointService.getPointInfo2(parameters);

        JSONObject jsonObj = new JSONObject();
        if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token")))
        	jsonObj.put("user_ci", parameters.get("user_ci"));
        else
        	jsonObj.put("user_token", parameters.get("user_token"));
        
		jsonObj.put("balance", piv.getBalance());
		log.append(piv.getLog());
		
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}
	
	@RequestMapping(
			value="/getPointForOtherService.do",

			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String getPointForOtherService(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");
		JSONObject jsonObj = new JSONObject();
		
		if(!parameters.containsKey("cust_id") || StringUtils.isEmpty(parameters.get("cust_id"))){
			
			jsonObj.put("cust_id",null);
			jsonObj.put("user_clip_point","0");
			jsonObj.put("result_code","01");
			
			log.append("CLiPPointService : getPointForOtherService cust_id is null");
			
			request.setAttribute("logSb", log);
			return jsonObj.toString();
			
		}

		PointInfoVo piv = this.clipPointService.getPointForOtherService(parameters);
		
		// 시스템 에러
		if( piv ==null ){
			
			jsonObj.put("cust_id",parameters.get("cust_id"));
			jsonObj.put("user_clip_point","0");
			jsonObj.put("result_code","02");
			
			request.setAttribute("logSb", log);
			log.append("CLiPPointService : getPointForOtherService error");
			
			return jsonObj.toString();
		}
		
		//  포인트 조회 결과 없음 
		if( piv != null && piv.getBalance() <0) {
			
			jsonObj.put("cust_id",parameters.get("cust_id"));
			jsonObj.put("user_clip_point","0");
			jsonObj.put("result_code","03");
			
			log.append(piv.getLog());
			log.append("CLiPPointService : getPointForOtherService error");
			request.setAttribute("logSb", log);
			
			return jsonObj.toString();
		}
		
		jsonObj.put("cust_id",parameters.get("cust_id"));
		jsonObj.put("user_clip_point",piv.getBalance());
		jsonObj.put("result_code","00");
		

		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}
	
	/**
	 * 카드 포인트 전환 시 쓰이는 url
	 * 기존 plusPoint와 파라미터 체크하는 부분만 다름
	 * @param request
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(
			value="/plusPoint.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String plusPoint(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		// ACL 체크
		if(!parameters.containsKey("requester_code") || StringUtils.isEmpty(parameters.get("requester_code"))){
			throw new UnAuthorizedException("401", "요청자 코드 값이 없음.");
		} else {
			if(!this.clipPointService.checkIP(parameters.get("requester_code"), request.getRemoteAddr())){
				throw new UnAuthorizedException("401", "잘못된 IP 주소.");
			}
		}
		
		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token"))) {
				throw new BadRequestException("400", "필수 파라미터 부재! : user_ci or user_token");
			} else {
				String userCi = clipPointService.getUserCiByUserToken(parameters.get("user_token"));
				if(userCi == null) {
					throw new NotFoundException("404", "사용자 정보 없음!");
				}else
					parameters.put("user_ci", userCi);
			}
		}

		if(!parameters.containsKey("transaction_id") || StringUtils.isEmpty(parameters.get("transaction_id"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : transaction_id");
		}

		if(!parameters.containsKey("point_value") || StringUtils.isEmpty(parameters.get("point_value")) || Long.valueOf(parameters.get("point_value")) < 1){
			throw new BadRequestException("400", "필수 파라미터 부재! : point_value");
		}

		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", parameters.get("requester_code"));
		
		ACLInfoVo aiv = this.aclInfoMapper.getACLInfo(parameters.get("reg_source"));
		parameters.put("acl_limit_yn", aiv.acl_limit_yn);
		parameters.put("acl_limit_cnt", aiv.acl_limit_cnt);
		
		PointHistVo phv = null;
		
		try {
			phv = this.clipPointService.plusPoint(parameters);
		} catch (Exception e) {
			logger.error("",e);
			phv =null;
		}
				
		
		if(phv == null){
			throw new CommonException("601", "입력 실패!");
		}
		
		PointInfoVo piv = this.clipPointService.getCurrPoint(parameters.get("user_ci"));
		
        JSONObject jsonObj = new JSONObject();
        if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token")))
        	jsonObj.put("user_ci", parameters.get("user_ci"));
        else
        	jsonObj.put("user_token", parameters.get("user_token"));
        
        jsonObj.put("transaction_id", phv.getTransactionId());
        jsonObj.put("datetime", phv.getRegDate() + phv.getRegTime());
        if( phv.getJoinLimitFailCnt() > 0){
        	phv.setPointValue(0);
		}

        jsonObj.put("requester_code", parameters.get("requester_code"));
        jsonObj.put("approve_no", piv.getPointHistIdx());
        jsonObj.put("balance", piv.getBalance());
        jsonObj.put("point_value", phv.getPointValue());
        jsonObj.put("description", phv.getDescription());
		
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}

	@RequestMapping(
			value="/plusPointCancel.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String plusPointCancel(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		// ACL 체크
		if(!parameters.containsKey("requester_code") || StringUtils.isEmpty(parameters.get("requester_code"))){
			throw new UnAuthorizedException("401", "요청자 코드 값이 없음.");
		} else {
			if(!this.clipPointService.checkIP(parameters.get("requester_code"), request.getRemoteAddr())){
				throw new UnAuthorizedException("401", "잘못된 IP 주소.");
			}
		}
		
		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token"))) {
				throw new BadRequestException("400", "필수 파라미터 부재! : user_ci or user_token");
			} else {
				String userCi = clipPointService.getUserCiByUserToken(parameters.get("user_token"));
				if(userCi == null)
					throw new NotFoundException("404", "사용자 정보 없음!");
				else
					parameters.put("user_ci", userCi);
			}
		}

		if(!parameters.containsKey("transaction_id") || StringUtils.isEmpty(parameters.get("transaction_id"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : transaction_id");
		}
		
		if(!parameters.containsKey("reference_id") || StringUtils.isEmpty(parameters.get("reference_id"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : reference_id");
		}

		parameters.put("reference_id", parameters.get("reference_id"));
		
		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", parameters.get("requester_code"));
		
		ACLInfoVo aiv = this.aclInfoMapper.getACLInfo(parameters.get("reg_source"));
		parameters.put("acl_limit_yn", aiv.acl_limit_yn);
		parameters.put("acl_limit_cnt", aiv.acl_limit_cnt);
		
		PointHistVo phv = this.clipPointService.plusPointCancel(parameters);
		
		if(phv == null){
			throw new CommonException("601", "입력 실패!");
		}
		
		PointInfoVo piv = this.clipPointService.getCurrPoint(parameters.get("user_ci"));
		
        JSONObject jsonObj = new JSONObject();
        if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token")))
        	jsonObj.put("user_ci", parameters.get("user_ci"));
        else
        	jsonObj.put("user_token", parameters.get("user_token"));
        
        jsonObj.put("transaction_id", phv.getTransactionId());
        jsonObj.put("requester_code", parameters.get("requester_code"));
        jsonObj.put("datetime", phv.getRegDate() + phv.getRegTime());
        if( phv.getJoinLimitFailCnt() > 0){
        	phv.setPointValue(0);
		}

        jsonObj.put("approve_no", piv.getPointHistIdx());
        jsonObj.put("balance", piv.getBalance());
        jsonObj.put("point_value", phv.getPointValue());
        jsonObj.put("description", phv.getDescription());
		
		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}
	
	@RequestMapping(
			value="/minusPoint.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String minusPoint(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		// ACL 체크
		if(!parameters.containsKey("requester_code") || StringUtils.isEmpty(parameters.get("requester_code"))){
			throw new UnAuthorizedException("401", "요청자 코드 값이 없음.");
		} else {
			if(!this.clipPointService.checkIP(parameters.get("requester_code"), request.getRemoteAddr())){
				throw new UnAuthorizedException("401", "잘못된 IP 주소.");
			}
		}
		
		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token"))) {
				throw new BadRequestException("400", "필수 파라미터 부재! : user_ci or user_token");
			} else {
				String userCi = clipPointService.getUserCiByUserToken(parameters.get("user_token"));
				if(userCi == null)
					throw new NotFoundException("404", "사용자 정보 없음!");
				else
					parameters.put("user_ci", userCi);
			}
		}

		if(!parameters.containsKey("transaction_id") || StringUtils.isEmpty(parameters.get("transaction_id"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : transaction_id");
		}

		if(!parameters.containsKey("point_value") || StringUtils.isEmpty(parameters.get("point_value")) || Long.valueOf(parameters.get("point_value")) < 1){
			throw new BadRequestException("400", "필수 파라미터 부재! : point_value");
		}
		
		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", parameters.get("requester_code"));
		
		PointInfoVo piv = null;
		
		try {
			piv = this.clipPointService.minusPoint(parameters);
		} catch (Exception e) {
			logger.error("",e);
			piv =null;
		}
		
		if(piv == null){
			throw new CommonException("602", "입력 실패!");
		}
		
        JSONObject jsonObj = new JSONObject();
        if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token")))
        	jsonObj.put("user_ci", parameters.get("user_ci"));
        else
        	jsonObj.put("user_token", parameters.get("user_token"));
        
        jsonObj.put("approve_no", piv.getPointHistIdx());
        jsonObj.put("transaction_id", parameters.get("transaction_id"));
        jsonObj.put("requester_code", parameters.get("requester_code"));
        jsonObj.put("datetime", piv.getRegDate() + piv.getRegTime());
        jsonObj.put("point_value", piv.getPointValue());
        jsonObj.put("balance", piv.getBalance());
        jsonObj.put("description", piv.getDescription());

		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}

	@RequestMapping(
			value="/minusPointCancel.do",
			method = RequestMethod.POST,
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String minusPointCancel(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		// ACL 체크
		if(!parameters.containsKey("requester_code") || StringUtils.isEmpty(parameters.get("requester_code"))){
			throw new UnAuthorizedException("401", "요청자 코드 값이 없음.");
		} else {
			if(!this.clipPointService.checkIP(parameters.get("requester_code"), request.getRemoteAddr())){
				throw new UnAuthorizedException("401", "잘못된 IP 주소.");
			}
		}
		
		if(!parameters.containsKey("user_ci") || StringUtils.isEmpty(parameters.get("user_ci"))){
			if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token"))) {
				throw new BadRequestException("400", "필수 파라미터 부재! : user_ci or user_token");
			} else {
				String userCi = clipPointService.getUserCiByUserToken(parameters.get("user_token"));
				if(userCi == null)
					throw new NotFoundException("404", "사용자 정보 없음!");
				else
					parameters.put("user_ci", userCi);
			}
		}

		if(!parameters.containsKey("transaction_id") || StringUtils.isEmpty(parameters.get("transaction_id"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : transaction_id");
		}
		
		if(!parameters.containsKey("reference_id") || StringUtils.isEmpty(parameters.get("reference_id"))){
			throw new BadRequestException("400", "필수 파라미터 부재! : reference_id");
		}
		
		parameters.put("reference_id", parameters.get("reference_id"));
		
		// 등록 매체를 구분하기 위한 값
		parameters.put("reg_source", parameters.get("requester_code"));
		
		PointInfoVo piv = this.clipPointService.minusPointCancel(parameters);
		
		if(piv == null){
			throw new CommonException("602", "입력 실패!");
		}
		
        JSONObject jsonObj = new JSONObject();
        if(!parameters.containsKey("user_token") || StringUtils.isEmpty(parameters.get("user_token")))
        	jsonObj.put("user_ci", parameters.get("user_ci"));
        else
        	jsonObj.put("user_token", parameters.get("user_token"));
        
        jsonObj.put("approve_no", piv.getPointHistIdx());
        jsonObj.put("transaction_id", parameters.get("transaction_id"));
        jsonObj.put("requester_code", parameters.get("requester_code"));
        jsonObj.put("datetime", piv.getRegDate() + piv.getRegTime());
        jsonObj.put("point_value", piv.getPointValue());
        jsonObj.put("balance", piv.getBalance());
        jsonObj.put("description", piv.getDescription());

		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}
	
	@RequestMapping(
			value="/vipCheck.do",
			produces = {"application/json;charset=UTF-8", "text/json;charset=UTF-8"}
	)
	@ResponseBody
	public String vipCheck(HttpServletRequest request) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");

		Map<String, String> parameters = (Map<String, String>)request.getAttribute("parametersMap");

		if(StringUtils.isEmpty(parameters.get("cust_id"))){
			if(StringUtils.isEmpty(parameters.get("user_ci"))) {
				throw new BadRequestException("400", "필수 파라미터 부재! : cust_id or user_ci");
			} else {
				String cust_id = clipPointService.getCustIdByUserCi(parameters.get("user_ci"));
				if(cust_id == null)
					throw new NotFoundException("404", "사용자 정보 없음!");
				else
					parameters.put("cust_id", cust_id);
			}
		}
		
        JSONObject jsonObj = new JSONObject();
        if(!StringUtils.isEmpty(parameters.get("user_ci")))
        	jsonObj.put("user_ci", parameters.get("user_ci"));
        else
        	jsonObj.put("cust_id", parameters.get("cust_id"));
        
		jsonObj.put("check_result", vipLoungeService.checkVip(parameters));

		request.setAttribute("logSb", log);
		return jsonObj.toString();
	}
	
}
