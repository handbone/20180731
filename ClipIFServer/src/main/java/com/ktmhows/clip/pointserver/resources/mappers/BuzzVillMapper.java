/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.resources.mappers;

import java.util.Map;

import com.ktmhows.clip.pointserver.vo.BuzzAdItem;
import com.ktmhows.clip.pointserver.vo.BuzzAdNewVo;
import com.ktmhows.clip.pointserver.vo.BuzzVillVo;
import com.ktmhows.clip.pointserver.vo.BzscreenEventVo;

public interface BuzzVillMapper {
	int insertBZScreenPost(BuzzVillVo bvv) throws java.lang.Exception;
	/** BSZ  적립여부 체크 */
	int getBZScreenPostCheck(Map<String, String> parameters) throws java.lang.Exception;
	
	int insertBZADPost(BuzzVillVo bvv) throws java.lang.Exception;
	long selectBZScreenPoint(Map<String, String> parameters) throws java.lang.Exception;
	BzscreenEventVo selectBZSEvent(Map<String, String> parameters) throws java.lang.Exception;
	void insertBZSEvent(Map<String, String> parameters) throws java.lang.Exception;
	BzscreenEventVo selectBZSEventNow(Map<String, String> parameters) throws java.lang.Exception;
	int selectJoinCntEvent(Map<String, String> parameters) throws java.lang.Exception;
	void updateBZSEventNow(Map<String, String> parameters) throws java.lang.Exception;
	void insertBZSEventNow(Map<String, String> parameters) throws java.lang.Exception;
	
	/** 고도화 **/
	int insertBZADPostNew(BuzzAdNewVo buzzAdNewVo) throws java.lang.Exception;
	int checkBuzAdTrId(BuzzAdNewVo buzzAdNewVo) throws java.lang.Exception;
	BuzzAdItem selectBuzAdItemValue(BuzzAdNewVo buzzAdNewVo) throws java.lang.Exception;
	int updatePointBombInfo(BuzzAdNewVo buzzAdNewVo) throws java.lang.Exception;
	
}
