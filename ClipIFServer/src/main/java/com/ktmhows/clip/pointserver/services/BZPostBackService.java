/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sound.sampled.Clip;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ktmhows.clip.pointserver.config.exceptions.CommonException;
import com.ktmhows.clip.pointserver.config.exceptions.NotFoundException;
import com.ktmhows.clip.pointserver.resources.mappers.BuzzVillMapper;
import com.ktmhows.clip.pointserver.resources.mappers.PointHistMapper;
import com.ktmhows.clip.pointserver.resources.mappers.UserInfoMapper;
import com.ktmhows.clip.pointserver.resources.repositories.CLiPPointMapperRepository;
import com.ktmhows.clip.pointserver.utils.AES256CipherClip;
import com.ktmhows.clip.pointserver.utils.DateUtil;
import com.ktmhows.clip.pointserver.vo.BuzzAdItem;
import com.ktmhows.clip.pointserver.vo.BuzzAdNewVo;
import com.ktmhows.clip.pointserver.vo.BuzzVillVo;
import com.ktmhows.clip.pointserver.vo.BzscreenEventVo;
import com.ktmhows.clip.pointserver.vo.PointHistVo;
import com.ktmhows.clip.pointserver.vo.UserInfoVo;

import com.ktmhows.clip.pointserver.utils.HttpNetwork;
import com.ktmhows.clip.pointserver.utils.HttpNetworkHTTPS;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


@Service
@PropertySource("classpath:config.properties")
@Transactional
public class BZPostBackService {
	
	//@Autowired
	//private UserInfoMapper userInfoMapper;
	
	//@Autowired
	//private PointInfoMapper pointInfoMapper;

	@Autowired
	private Environment env;
	
	@Autowired
	private PointHistMapper pointHistMapper;
	
	@Autowired
	private BuzzVillMapper buzzVillMapper;

	@Autowired
	private UserInfoMapper userInfoMapper;
	
	@Autowired
	private CLiPPointMapperRepository clipPointMapperRepository;
	
	@Autowired
	private HttpNetworkHTTPS httpNetworkHTTPS;
	
	@Autowired
	private HttpNetwork httpNetwork;
	
	//@Autowired
	//private CLiPPointMapperRepository clipPointMapperRepository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public PointHistVo insertBuzzScreenPoint(Map<String, String> parameters) throws java.lang.Exception {
		StringBuffer sbLog = new StringBuffer();
		sbLog.append("BZPostBackService : insertBuzzScreenPoint Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		UserInfoVo uiv = null;
		parameters.put("cust_id", parameters.get("user_id"));
		
		try{
			uiv = this.userInfoMapper.getUserInfo(parameters);
			if(uiv == null) {
				sbLog.append("BZPostBackService : getUserInfo result : null" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			}else {
				sbLog.append("BZPostBackService : getUserInfo result : not null" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			}
			
			//-------------------------user_info_tbl 조회 결과 없을 경우 로직 추가-------------------------------
			//클립포인트 메인화면 거치지 않고, 바로 클립잠금앱을 통했을 때, USER_INFO_TBL에 user정보 insert 로직 추가
			if(null == uiv && !"".equals(parameters.get("cust_id")) && null != parameters.get("cust_id")){
			 		uiv = new UserInfoVo();
			 		
			 		String cust_id = parameters.get("cust_id");
			 		String strHtmlSource = "";
			 		String user_ci = "";
			 		String ctn = "";
			 		String result_msg = "";
			 		String cert_type = "2";
			 		
			 		Calendar calendar = Calendar.getInstance();
			 		SimpleDateFormat format = new SimpleDateFormat();
			        calendar.add(Calendar.MINUTE, +30);
			        format.applyPattern("yyyyMMddHHmmss");
			        ctn = format.format(calendar.getTime());
			        ctn = AES256CipherClip.AES_EncodeECB(ctn);
			 		
			        JSONObject jObject = new JSONObject();
			        
			        jObject.put("cust_id", cust_id);
					jObject.put("service_id", "clippoint");
					jObject.put("cert_type", cert_type);
					jObject.put("cert_key", ctn);
			        
					String params = jObject.toJSONString();
					
					this.logger.debug("===========================================================" );
					this.logger.debug("GetUserCi API CALL param : "+params );	
			 		if("N".equals(this.env.getProperty("HTTPS_YN"))){
						strHtmlSource = httpNetwork.strGetUserCi(params);
					}else{
						strHtmlSource = httpNetworkHTTPS.strGetUserCi(params);
					}
			 		this.logger.debug("strHtmlSource"+strHtmlSource );
					this.logger.debug("===========================================================" );

					JSONParser jsonParser = new JSONParser();
			    	JSONObject jsonObject = (JSONObject) jsonParser.parse(strHtmlSource);
					
			    	result_msg = jsonObject.get("msg").toString();
			    	
			    	if(result_msg.equals("SUCCESS")) {
			    		//USER_INFO_TBL 에서 index값을 얻어오기 위해 user_ci로 조회
			    		Map<String, String> uMap = new HashMap<String, String>();
			    		user_ci = jsonObject.get("ci").toString();
			    		ctn = jsonObject.get("phone").toString();
			    		
			    		uMap.put("user_ci", user_ci);
			    		uiv= userInfoMapper.getUserInfo(uMap);
			    		if(uiv == null) {// ci로 user_info_tbl를 조회했을때 없을 경우엔 insert
			    			UserInfoVo newUser = new UserInfoVo();
			    			newUser.setUserCi(user_ci);
			    			newUser.setCustId(cust_id);
			    			newUser.setCtn(ctn);
			    			if(this.userInfoMapper.insertUserInfo(newUser) > 0) {
					 			uiv = this.userInfoMapper.getUserInfo(parameters);
					 			uiv.setUpdate_gbn("I");
					 			this.userInfoMapper.insertUserInfoLog(uiv);
					 		}else {
					 			this.logger.error("insertUserInfo fail");
					 			sbLog.append("BZPostBackService : insertUserInfo failed" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
					 		}
			    		}else {// user_info_tbl에 조회결과가 있을 경우 update
			    			uiv.setCustId(cust_id);
			    			uiv.setCtn(ctn);
			    			if(this.userInfoMapper.updateUserInfo(uiv) > 0){
								uiv = this.userInfoMapper.getUserInfo(parameters);
								uiv.setUpdate_gbn("U");
								this.userInfoMapper.insertUserInfoLog(uiv);
							}else {
								sbLog.append("BZPostBackService : updateUserInfo failed" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
							}
			    		}
			    					    		
			    	}else {
			    		uiv=null;
			    		sbLog.append("GetUserCi API CALL error: " +result_msg+"\r\n");
			    		throw new NotFoundException("404", "클립서버 사용자정보 없음!");
			    	}
			
			  }
			//-------------------------------------------------------------------------------------------------
			  
		} catch(Exception ex){
			logger.error("getUserInfo err",ex);
			//ex.printStackTrace();
			uiv = null;
			throw new NotFoundException("404", "클립서버 사용자정보 없음!");	
		}
		
		BuzzVillVo bvv = new BuzzVillVo(parameters);
		BuzzVillVo bvv2 = null;
		
		/******************************************************
		 * 오픈 프로모션을 위한 적용
		 * 
		 *******************************************************/
		String startDate = "20160922"; // 상용
		String endDate = this.env.getProperty("event.doublePoint.enddate");
		//String endDate = "20171212"; // test
		//sbLog.append("BZPostBackService : doublePoint event end date = " + this.env.getProperty("event.doublePoint.enddate") + "\r\n");
		int nowDate = Integer.parseInt(DateUtil.getDate("yyyyMMdd"));
		
		if(Integer.parseInt(startDate) <= nowDate && nowDate <= Integer.parseInt(endDate)){
			sbLog.append("BZPostBackService : Promotion Logic Check Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			bvv2 = new BuzzVillVo(parameters);
			
			parameters.put("start_date", startDate);
			parameters.put("end_date", endDate);
			
			long currentPoint = this.buzzVillMapper.selectBZScreenPoint(parameters);
			
			long reservedPoint = bvv2.getPoint();
			String campaignName = bvv2.getCampaignName();
			String transactionId = bvv2.getTransactionId();
			
			// limit 6000. as 12000
			if((currentPoint+reservedPoint) <= 12000){
				bvv2.setTransactionId(transactionId + "_1");
				bvv2.setCampaignName("★2배★" + campaignName);
				
				sbLog.append("BZPostBackService : Promotion Logic Check true!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
				
			} else {
				bvv2 = null;
			}
			sbLog.append("BZPostBackService : Promotion Logic Check End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			
		}
		
		
		
		
		
		/*********************************************************
		 * 최초접속인지 확인할때는 이벤트 관계없이 체크한다.
		 * 한 아이디는 이벤트명이 없는 ROW를 기본적으로 가지며, 이를 통해 최초 접속만을 기록한다.
		 * 해당 이벤트가 진행중일때는 이벤트명 값을 가지는 신규 ROW를 각각 가진다. 
		 *********************************************************/
		
		/**
		 * 접속기록이 있는지 확인한다.
		 */
		/*BzscreenEventVo bzscreenEventVo = this.buzzVillMapper.selectBZSEvent(parameters);
		// 최초 적립일 경우 신규 ROW 생성
		if( null == bzscreenEventVo){
			this.buzzVillMapper.insertBZSEvent(parameters);
			bzscreenEventVo = this.buzzVillMapper.selectBZSEvent(parameters);
		}
		sbLog.append("BZPostBackService : USER " + bzscreenEventVo.user_id + " First Join Date : " + bzscreenEventVo.getRegdate() + "  \r\n");
		*/
		
		
		
		/**
		 * 2017 2배 이벤트
		 */
		/*BuzzVillVo bvv3 = null;
		
		String EVENT_START_DATE = this.env.getProperty("EVENT_START_DATE");
		String EVENT_END_DATE = this.env.getProperty("EVENT_END_DATE");
		sbLog.append("chk 1 - BZPostBackService : 2017 doublePoint event start date 	= " + EVENT_START_DATE + "\r\n");
		sbLog.append("chk 1 - BZPostBackService : 		nowDate 	= " + nowDate+"000000" + "\r\n");
		sbLog.append("chk 1 - BZPostBackService : 2017 doublePoint event 	end date	= " + EVENT_END_DATE + "\r\n");
		
		// 최초 접속일자는 이벤트의 시작일자와 종료일자 사이에 있어야 하며
		if(Long.parseLong(EVENT_START_DATE) <= Long.parseLong(bzscreenEventVo.getReg_date()+bzscreenEventVo.getReg_time())
			&& Long.parseLong(bzscreenEventVo.getReg_date()+bzscreenEventVo.getReg_time()) <= Long.parseLong(EVENT_END_DATE)){
			
			// 현재 날짜는 최초 접속 후 한달내여야 한다.
			int year = Integer.parseInt(bzscreenEventVo.getReg_date().substring(0, 4)); 
			int month = Integer.parseInt(bzscreenEventVo.getReg_date().substring(4, 6)); 
			int day = Integer.parseInt(bzscreenEventVo.getReg_date().substring(6)); 
			Calendar cal = Calendar.getInstance(); 
			cal.set(year, month - 1, day, 0 ,0, 0);//년도, 월, 일, 시, 분, 초 : 시, 분, 초는 00:00:00으로 처리 
			cal.add(Calendar.DAY_OF_YEAR, +31);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			String endDay = dateFormat.format(cal.getTime()); 
			if(Integer.parseInt(bzscreenEventVo.getReg_date()) <= nowDate && nowDate <= Integer.parseInt(endDay)){
					sbLog.append("chk 2 - BZPostBackService : getReg_date:" +bzscreenEventVo.getReg_date()+"    nowDate:"+nowDate+"   endDay:"+endDay + "\r\n");
					sbLog.append("BZPostBackService : 2017 Promotion Logic Check Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
					
					*//**
					 * 해당 이벤트에 해당하는 접속기록 선택
					 *//*
					parameters.put("eventName",this.env.getProperty("EVENT_NAME"));
					int joinCntEvent = this.buzzVillMapper.selectJoinCntEvent(parameters);
					// 해당 이벤트는 처음이라면 해당 이벤트명으로 신규 ROW 생성
					if( joinCntEvent == 0 ){
						this.buzzVillMapper.insertBZSEventNow(parameters);
					}
					bzscreenEventVo = this.buzzVillMapper.selectBZSEventNow(parameters);
					long currentPoint = Long.parseLong(bzscreenEventVo.getEvent_add_point_sum());
					
					bvv3 = new BuzzVillVo(parameters);
					
					long reservedPoint = bvv3.getPoint();
					String campaignName = bvv3.getCampaignName();
					String transactionId = bvv3.getTransactionId();
					
					// 해당 이벤트에 적립된 포인트와 현재 적립하려는 포인트의 합이 limit을 넘으면 안된다.
					if((currentPoint+reservedPoint) <= Integer.parseInt(this.env.getProperty("EVENT_LIMIT_POINT")) ){
						bvv3.setTransactionId(transactionId + "_2");
						bvv3.setCampaignName("★2배★" + campaignName);
						
						sbLog.append("BZPostBackService : 2017 Promotion Logic Check true!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
						
					} else {
						bvv3 = null;
						sbLog.append("BZPostBackService : 2017 2배적립 이벤트를 적립하려 했으나 적립상한선을 넘었음! " + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
					}
					sbLog.append("BZPostBackService : 2017 Promotion Logic Check End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");


				
			}else{
				sbLog.append("BZPostBackService : 2017 2배적립 이벤트 : 최초 접속일자는 이벤트 기간안에 있지만, 두배적립이 가능한 접속 후 한달이 지났음! \r\n");
			}
			
			
			
		}else{
			sbLog.append("BZPostBackService : 2017 2배적립 이벤트 : 최초 접속일자가 이벤트 기간내에 있지 않음! \r\n");
		}
		*/
		
		
		
		/**
		 *  실제 적립 로직
		 */
		sbLog.append("BZPostBackService : BZ History Insert Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		this.buzzVillMapper.insertBZScreenPost(bvv);
		sbLog.append("BZPostBackService : BZ History Insert End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		
		// 기본적립
		// buzz vill id = cust_id
		PointHistVo phv = new PointHistVo();
//		phv.setCustId(AES256CipherForCLiP.setAesMsg(bvv.getUserId(), AES256CipherForCLiP.CLIP_AES_KEY_V2));
				
		phv.setCustId(bvv.getUserId());
		phv.setTransactionId(bvv.getTransactionId());
		phv.setPointType("I");
		phv.setPointValue(bvv.getPoint());
		phv.setRegSource(parameters.get("reg_source"));
		sbLog.append("bvv.getCampaignName() : "+ bvv.getCampaignName() + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		phv.setDescription(bvv.getCampaignName() + " 적립!");
		// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
		// 실제 사용하지는 않기때문에 주석처리한다.
		//phv.setAcl_limit_yn(bvv.getAcl_limit_yn());
		//phv.setAcl_limit_cnt(bvv.getAcl_limit_cnt());
		sbLog.append("BZPostBackService : Point History Insert Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
		
		int rst = 0;
		try {
			this.pointHistMapper.insertPointHist(phv);
			rst = 1;
		} catch (Exception e) {
			rst = 0;
			logger.error("insertPointHist Error");
		}
		
		if( rst > 0){
			
			// user정보가 존재할 경우 즉시전환
			phv = this.pointHistMapper.getPointHist(parameters);
			
			if(uiv != null && phv != null){
				try{
					sbLog.append("BZPostBackService : changePoint start" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
					this.clipPointMapperRepository.changePoint(phv.getPointHistIdx(), uiv);
					sbLog.append("BZPostBackService : changePoint end" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

				} catch(CommonException ex){
					logger.error("",ex);
					// do noting! - 소스코드 품질검사로 인해 로그를 남기는 로직을 추가함
				} catch (Exception e) {
					logger.error("",e);
				}	
			}
			
			// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
			// 실제 사용하지는 않기때문에 주석처리한다.
			//PointHistVo phvTemp = new PointHistVo();
			//phvTemp = this.pointHistMapper.getPointHist(parameters);
			//this.pointHistMapper.insertPointHistCount(phvTemp);
			
			sbLog.append("BZPostBackService : Point History Insert End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

			// 2배 이벤트에 해당했을 경우 추가 포인트를 적립한다.
			if(bvv2 != null){
				sbLog.append("BZPostBackService : Promotion BZ History Insert Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
				this.buzzVillMapper.insertBZScreenPost(bvv2);
				sbLog.append("BZPostBackService : Promotion BZ History Insert End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
				
				PointHistVo phv2 = new PointHistVo();
				phv2.setCustId(bvv2.getUserId());
				phv2.setTransactionId(bvv2.getTransactionId());
				phv2.setPointType("I");
				phv2.setPointValue(bvv2.getPoint());
				phv2.setRegSource(parameters.get("reg_source"));
				phv2.setDescription(bvv2.getCampaignName() + " 적립!");
				// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
				// 실제 사용하지는 않기때문에 주석처리한다.
				//phv2.setAcl_limit_yn(bvv2.getAcl_limit_yn());
				//phv2.setAcl_limit_cnt(bvv2.getAcl_limit_cnt());
				sbLog.append("BZPostBackService : Promotion Point History Insert Start!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");				
				int rst2 = 0; // 아래 rst2 > 0 조건을 위해 사용, insert 리턴결과가 0으로 오기때문에 임시수정 
				try {
					this.pointHistMapper.insertPointHist(phv2);
					rst2 = 1;
				} catch (Exception e) {
					rst2 = 0;
				}
				
				if(rst2 > 0){
					// user정보가 존재할 경우 즉시전환
					phv2 = this.pointHistMapper.getPointHist(parameters);
					
					if(uiv != null && phv2 != null){
						try{
							sbLog.append("BZPostBackService : changePoint start" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
							this.clipPointMapperRepository.changePoint(phv2.getPointHistIdx(), uiv);
							sbLog.append("BZPostBackService : changePoint end" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");

						} catch(CommonException ex){
							logger.error("",ex);
							// do noting! - 소스코드 품질검사로 인해 로그를 남기는 로직을 추가함
						} catch (Exception e) {
							logger.error("",e);
						}	
					}
				}
				// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
				// 실제 사용하지는 않기때문에 주석처리한다.
				//PointHistVo phvTemp2 = new PointHistVo();
				//HashMap<String, String> parameters2 = new HashMap<String,String>();
				//parameters2.put("transaction_id", bvv2.getTransactionId());
				//phvTemp2 = this.pointHistMapper.getPointHist(parameters2);
				//this.pointHistMapper.insertPointHistCount(phvTemp2);
				sbLog.append("BZPostBackService : Promotion Point History Insert End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
				
			}			
			
			PointHistVo phv4 = this.pointHistMapper.getPointHist(parameters);

			sbLog.append("BZPostBackService : insertBuzzScreenPoint End!" + DateUtil.getCurrentDateString("HH:mm:ss.SSS") + "\r\n");
			phv4.setLog(sbLog);
			
			return phv4;
		} else {
			return null;
		}
		
	}

	public PointHistVo insertBuzzADPoint(Map<String, String> parameters) throws java.lang.Exception {
		BuzzVillVo bvv = new BuzzVillVo(parameters);
		
		UserInfoVo uiv = null;
		parameters.put("cust_id", parameters.get("user_id"));
		try{
			uiv = this.userInfoMapper.getUserInfo(parameters);
		} catch(Exception ex){
			//ex.printStackTrace();
			uiv = null;
		}
		
		this.buzzVillMapper.insertBZADPost(bvv);

		// buzz vill id = cust_id
		PointHistVo phv = new PointHistVo();
//		phv.setCustId(AES256CipherForCLiP.setAesMsg(bvv.getUserId(), AES256CipherForCLiP.CLIP_AES_KEY_V2));
		phv.setCustId(bvv.getUserId());
		phv.setTransactionId(bvv.getTransactionId());
		phv.setPointType("I");
		phv.setPointValue(bvv.getPoint());
		phv.setRegSource(parameters.get("reg_source"));
		phv.setDescription(bvv.getTitle() + " 적립!");
		// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
		// 실제 사용하지는 않기때문에 주석처리한다.
		//phv.setAcl_limit_yn(bvv.getAcl_limit_yn());
		//phv.setAcl_limit_cnt(bvv.getAcl_limit_cnt());
		
		int rst = 0;
		try {
			this.pointHistMapper.insertPointHist(phv);
			rst = 1;
		} catch (Exception e) {
			rst = 0;
		}
		
		if(rst > 0){
			
			// user정보가 존재할 경우 즉시전환
			phv = this.pointHistMapper.getPointHist(parameters);
	
				if(uiv != null && phv != null){
					try{
						this.clipPointMapperRepository.changePoint(phv.getPointHistIdx(), uiv);
					} catch(CommonException ex){
						logger.error("",ex);
						// do noting! - 소스코드 품질검사로 인해 로그를 남기는 로직을 추가함
					} catch (Exception e) {
						logger.error("",e);
					}
				}
				
			// insertPointHist 를 쓰는 모든곳에 횟수 제한을 걸다보니 BZS까지 횟수제한 로직을 걸었다.
			// 실제 사용하지는 않기때문에 주석처리한다.
			//PointHistVo phv2 = new PointHistVo();
			//phv2 = this.pointHistMapper.getPointHist(parameters);
			//this.pointHistMapper.insertPointHistCount(phv2);
			//return phv2;
				
			PointHistVo returndata =this.pointHistMapper.getPointHist(parameters);
			
			return returndata;
		} else {
			return null;
		}
	}
	
	public BuzzAdNewVo insertBuzzADPoint(BuzzAdNewVo param) throws java.lang.Exception {
		
		param.setUser_id( param.getCustom());
		
		int cnt = this.buzzVillMapper.checkBuzAdTrId(param);
		
		if(cnt > 0){
			throw new CommonException("999","이미 처리된 요청입니다.");
		} else {
			this.buzzVillMapper.insertBZADPostNew(param);
		}
		
		UserInfoVo uiv = null;
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("cust_id", param.getCustom());
		try{
			uiv = this.userInfoMapper.getUserInfo(parameters);
		} catch(Exception ex){
			throw new NotFoundException("404", "사용자 확인 불가!");
		}
		
		param.setCust_id(uiv.getCustId());
		BuzzAdItem itemInfo = this.buzzVillMapper.selectBuzAdItemValue(param);
		
		if(itemInfo == null){
			throw new CommonException("999","상품 정보가 없습니다.");
		} else {
			param.setBase_point(itemInfo.getRevenue());
			this.buzzVillMapper.updatePointBombInfo(param);
			
			param.setResult("S");
			param.setResultMsg("정상적으로 처리되었습니다.");
		}

		return param;
	}
	
	public int  getBZScreenPostCheck(Map<String, String> parameters)  throws java.lang.Exception {
		return buzzVillMapper.getBZScreenPostCheck(parameters);
		
	}
}
