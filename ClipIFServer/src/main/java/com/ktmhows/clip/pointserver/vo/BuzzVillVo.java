/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.vo;

import java.util.Map;

public class BuzzVillVo {
	private String appKey;
	private String transactionId;
	private String userId;
	private int campaignId;
	private String campaignName;
	private String title;
	private long eventAt;
	private int isMedia;
	private String extra;
	private String actionType;
	private long point;
	private long basePoint;
	private String data;
	private String acl_limit_yn;
	private int acl_limit_cnt;
	
	
	public BuzzVillVo(Map<String, String> parameters){
		if(parameters.containsKey("app_key")){
			this.appKey = parameters.get("app_key");
		}

		if(parameters.containsKey("transaction_id")){
			this.transactionId = parameters.get("transaction_id");
		}

		if(parameters.containsKey("user_id")){
			this.userId = parameters.get("user_id");
		}

		if(parameters.containsKey("campaign_id")){
			this.campaignId = Integer.parseInt(parameters.get("campaign_id"));
		}

		if(parameters.containsKey("campaign_name")){
			this.campaignName = parameters.get("campaign_name");
		}

		if(parameters.containsKey("title")){
			this.title = parameters.get("title");
		}

		if(parameters.containsKey("event_at")){
			this.eventAt = Long.parseLong(parameters.get("event_at"));
		}

		if(parameters.containsKey("is_media")){
			this.isMedia = Integer.parseInt(parameters.get("is_media"));
		}

		if(parameters.containsKey("extra")){
			this.extra = parameters.get("extra");
		}

		if(parameters.containsKey("action_type")){
			this.actionType = parameters.get("action_type");
		}

		if(parameters.containsKey("point")){
			this.point = Long.parseLong(parameters.get("point"));
		}

		if(parameters.containsKey("base_point")){
			this.basePoint = Long.parseLong(parameters.get("base_point"));
		}

		if(parameters.containsKey("data")){
			this.data = parameters.get("data");
		}

		if(parameters.containsKey("acl_limit_yn")){
			this.acl_limit_yn = parameters.get("acl_limit_yn");
		}
		if(parameters.containsKey("acl_limit_cnt")){
			this.acl_limit_cnt = Integer.parseInt(parameters.get("acl_limit_cnt"));
		}
		
		
	}
	
	public String getAppKey() {
		return appKey;
	}
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getEventAt() {
		return eventAt;
	}
	public void setEventAt(long eventAt) {
		this.eventAt = eventAt;
	}
	public int getIsMedia() {
		return isMedia;
	}
	public void setIsMedia(int isMedia) {
		this.isMedia = isMedia;
	}
	public String getExtra() {
		return extra;
	}
	public void setExtra(String extra) {
		this.extra = extra;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public long getPoint() {
		return point;
	}
	public void setPoint(long point) {
		this.point = point;
	}
	public long getBasePoint() {
		return basePoint;
	}
	public void setBasePoint(long basePoint) {
		this.basePoint = basePoint;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}

	public String getAcl_limit_yn() {
		return acl_limit_yn;
	}

	public void setAcl_limit_yn(String acl_limit_yn) {
		this.acl_limit_yn = acl_limit_yn;
	}

	public int getAcl_limit_cnt() {
		return acl_limit_cnt;
	}

	public void setAcl_limit_cnt(int acl_limit_cnt) {
		this.acl_limit_cnt = acl_limit_cnt;
	}
	
}
