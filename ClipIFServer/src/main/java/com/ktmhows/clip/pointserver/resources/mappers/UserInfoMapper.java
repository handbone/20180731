/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.resources.mappers;

import java.util.Map;

import javax.annotation.Resource;

import com.ktmhows.clip.pointserver.vo.UserInfoVo;

@Resource(name="sqlSessionFactory")
public interface UserInfoMapper {
	UserInfoVo getUserInfo(Map<String, String> parameters) throws java.lang.Exception;
	int insertUserInfo(UserInfoVo uiv) throws java.lang.Exception;
	int insertUserInfoLog(UserInfoVo uiv) throws java.lang.Exception;
	int updateUserInfo(UserInfoVo uiv) throws java.lang.Exception;
	int exitUserInfo(UserInfoVo uiv) throws java.lang.Exception;
	
	//user_unique_id 로 user_ci 조회
	String selectUserCiByUserToken(String userToken) throws java.lang.Exception;
	
	//잠금화면 한달 이상 사용자 체크
	String checkVipByLockScreen(String cust_id) throws Exception;
	
}
