/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.config.database;

import java.util.Objects;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import net.sf.log4jdbc.Log4jdbcProxyDataSource;
import net.sf.log4jdbc.tools.Log4JdbcCustomFormatter;
import net.sf.log4jdbc.tools.LoggingType;

@Configuration
@PropertySource("classpath:server.properties")
//@PropertySource("classpath:server-${SERVER_CONF_MODE}.properties")
//@PropertySource("file:${CONF_PATH}/server-${SERVER_CONF_MODE}.properties")
@EnableTransactionManagement
@MapperScan(basePackages="com.ktmhows.clip.pointserver.resources.mappers", sqlSessionFactoryRef="sqlSessionFactory")
public class MybatisConfig {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Environment env;
	
	@Bean(name="dataSourceReal",destroyMethod="close")
	public DataSource dataSourceReal() {
		
//		String id = System.getProperty("db.id");
//		String pass = System.getProperty("db.password");
		
		/*BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(this.env.getProperty("jdbc.drvierClassName"));
		dataSource.setUrl(this.env.getProperty("jdbc.url"));
		dataSource.setUsername(this.env.getProperty("jdbc.userName"));
		dataSource.setPassword(this.env.getProperty("jdbc.password"));*/
		/*Context ctx = new InitialContext();
		DataSource dataSource = (DataSource) ctx.lookup("java:/comp/env/jdbc/ClipPointDS");*/
		
		
		String envInitialSize= Objects.toString(this.env.getProperty("jdbc.initialSize"), "N") ;
		String envMaxActive= Objects.toString(this.env.getProperty("jdbc.maxActive"), "N");
		String envMaxWait=Objects.toString(this.env.getProperty("jdbc.maxWait"), "N");
		String envMinIdle=Objects.toString(this.env.getProperty("jdbc.minIdle"), "N");
		String evnMaxIdle=Objects.toString(this.env.getProperty("jdbc.maxIdle"), "N");
		
		BasicDataSource dataSourceReal = new BasicDataSource();
		dataSourceReal.setDriverClassName(this.env.getProperty("jdbc.drvierClassName"));
		dataSourceReal.setUrl(this.env.getProperty("jdbc.url"));
		dataSourceReal.setUsername(this.env.getProperty("jdbc.userName"));
		dataSourceReal.setPassword(this.env.getProperty("jdbc.password"));
		
		if ( envInitialSize.matches("^[0-9]+$") ) {
			dataSourceReal.setInitialSize(Integer.parseInt(envInitialSize));
		}
		if ( envMaxActive.matches("^[0-9]+$") ) {
			dataSourceReal.setMaxActive(Integer.parseInt(envMaxActive));
		}
		if ( envMaxWait.matches("^[0-9]+$") ) {
			dataSourceReal.setMaxWait(Integer.parseInt(envMaxWait));
		}
		if ( envMinIdle.matches("^[0-9]+$") ) {
			dataSourceReal.setMinIdle(Integer.parseInt(envMinIdle));
		}
		if ( evnMaxIdle.matches("^[0-9]+$") ) {
			dataSourceReal.setMaxIdle(Integer.parseInt(evnMaxIdle));
		}
	
		this.logger.info("Make dataSource !");
		this.logger.info("drvierClassName : {}" , this.env.getProperty("jdbc.drvierClassName"));
		this.logger.info("initialSize : {}" , envInitialSize);
		this.logger.info("maxActive : {}" , envMaxActive);
		this.logger.info("maxWait : {}" , envMaxWait);
		this.logger.info("minIdle : {}" ,envMinIdle );
		this.logger.info("maxIdle : {}" ,evnMaxIdle) ;
		
		
		
		return dataSourceReal;
	}
	
	@Bean(name="dataSource")
	public Log4jdbcProxyDataSource dataSource(DataSource dataSourceReal) {
		Log4jdbcProxyDataSource log4DataSource = new Log4jdbcProxyDataSource(dataSourceReal);
		Log4JdbcCustomFormatter fomatter = new Log4JdbcCustomFormatter();
		fomatter.setLoggingType(LoggingType.MULTI_LINE);
		fomatter.setSqlPrefix("SQL:::");
		log4DataSource.setLogFormatter(fomatter);
		
		return log4DataSource;		
	}

    @Bean(name="transactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("dataSource") Log4jdbcProxyDataSource dataSource) {

		this.logger.debug("Make transactionManager !");
		
        return new DataSourceTransactionManager(dataSource);
    }
        
	@Bean(name="sqlSessionFactory")
	public SqlSessionFactory sqlSessionFactory(@Qualifier("dataSource") Log4jdbcProxyDataSource dataSource,  ApplicationContext applicationContext) throws java.lang.Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dataSource);
		sqlSessionFactoryBean.setTypeAliasesPackage("com.ktmhows.clip.pointserver.vo");
		sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:com/ktmhows/clip/pointserver/resources/mappers/*.xml"));
//		sqlSessionFactoryBean.setTransactionFactory(new ManagedTransactionFactory());
		this.logger.debug("Make sqlSessionFactoryBean !");
		
		return sqlSessionFactoryBean.getObject();
	}
	
	@Bean(name="sqlSession")
	public SqlSessionTemplate sqlSession(@Qualifier("sqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws java.lang.Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
}
