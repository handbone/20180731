package com.ktmhows.clip.pointserver.vo;

import java.util.Map;

public class BuzzAdNewVo {
	
	private String app_key;
	private String transaction_id;
	private String unit_id;
	private int campaign_id;
	private String ifa;
	private String custom;
	
	private String cust_id;
	private String user_id;
	private long base_point;
	
	
	
	public BuzzAdNewVo(Map<String, String> parameters){
		if(parameters.containsKey("app_key")){
			this.app_key = parameters.get("app_key");
		}

		if(parameters.containsKey("transaction_id")){
			this.transaction_id = parameters.get("transaction_id");
		}

		if(parameters.containsKey("custom")){
			this.custom = parameters.get("custom");
			this.user_id = parameters.get("custom");
			this.cust_id = parameters.get("custom");
		}

		if(parameters.containsKey("campaign_id")){
			this.campaign_id = Integer.parseInt(parameters.get("campaign_id"));
		}

		
		if(parameters.containsKey("base_point")){
			this.base_point = Long.parseLong(parameters.get("base_point"));
		}
		
		
	}
	
	public String getUser_id() {
		return user_id;
	}
	public String getApp_key() {
		return app_key;
	}
	public void setApp_key(String app_key) {
		this.app_key = app_key;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	private String result;
	private String resultMsg;
	
	public String getUnit_id() {
		return unit_id;
	}
	public void setUnit_id(String unit_id) {
		this.unit_id = unit_id;
	}
	public int getCampaign_id() {
		return campaign_id;
	}
	public void setCampaign_id(String campaign_id) {
		this.campaign_id = Integer.parseInt(campaign_id);
	}
	public String getIfa() {
		return ifa;
	}
	public void setIfa(String ifa) {
		this.ifa = ifa;
	}
	public String getCustom() {
		return custom;
	}
	public void setCustom(String custom) {
		this.custom = custom;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public long getBase_point() {
		return base_point;
	}
	public void setBase_point(int base_point) {
		this.base_point = base_point;
	}
}
