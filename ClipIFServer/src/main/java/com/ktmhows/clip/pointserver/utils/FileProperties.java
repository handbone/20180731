/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Properties;

public class FileProperties {
	private Properties properties;

	public FileProperties(File f) throws IOException {
		this.properties = new Properties();
		FileInputStream fis = new FileInputStream(f);
		try {
			this.properties.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try { fis.close(); } catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public FileProperties(String fileName) throws IOException {
		this(new File(fileName));
	}
	
	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	public String getProperty(String key, String defaultValue) {
		return properties.getProperty(key, defaultValue);
	}
	
	public Enumeration<?> propertyNames() {
		return properties.propertyNames();
	}
	
	public Object setProperty(String key, String value) {
		return properties.setProperty(key, value);
	}
	
	public void store(OutputStream out, String header) throws IOException {
		properties.store(out, header);
	}
	
	public void print() {
		Enumeration<?> e = properties.propertyNames();
		while(e.hasMoreElements()) {
			String key = (String)e.nextElement();
			//System.out.println(key + " : " + properties.getProperty(key));
		}		
	}
}