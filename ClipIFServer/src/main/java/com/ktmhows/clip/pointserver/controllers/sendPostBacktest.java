package com.ktmhows.clip.pointserver.controllers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class sendPostBacktest {
	
	@Autowired
	private SqlSession postgreSession;

	@RequestMapping(value = "/sendPostBacktest.do")
	@ResponseBody
	public String sendPostBacktest(HttpServletRequest request, @RequestParam Map map) throws java.lang.Exception {
	
		String smsDomain = "http://localhost:8080/postBackForBZScreen.do";
		String parameter = "user_id=testudfsesrdf2s122%30%D30&click_type=u&extra=%7B%7D&is_media=0&point=2&unit_id=288093757943634&campaign_id=100054&event_at=1479141459&action_type=u&campaign_name=%5B%ED%95%9C%EA%B5%AD%EA%B2%BD%EC%A0%9C%EC%8B%A0%EB%AC%B8%5D+%ED%8D%BC%EC%84%BC%ED%8A%B8+%ED%85%8C%EC%8A%A4%ED%8A%B8+%255+%256+13+&base_point=2&transaction_id=914";
		
		BufferedReader oBufReader = null;
		HttpURLConnection httpConn = null;
		String strBuffer = "";
		String strRslt = "";
		
			String strEncodeUrl = smsDomain ;

			URL oOpenURL;
				oOpenURL = new URL(strEncodeUrl);
			
				httpConn = (HttpURLConnection) oOpenURL.openConnection();
				httpConn.setDoOutput(true);
				httpConn.setRequestMethod("POST");
				httpConn.setConnectTimeout(5000);
				httpConn.setReadTimeout(5000);
				httpConn.setRequestProperty("Content-Length", String.valueOf(parameter.length()));
				OutputStream os = httpConn.getOutputStream();
				os.write(parameter.getBytes());
				os.flush();
				os.close();
	
				oBufReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(),"UTF-8"));
	
				StringBuffer sb = new StringBuffer(strRslt);
				
				while ((strBuffer = oBufReader.readLine()) != null) {
					if (strBuffer.length() > 1) {
						sb.append(strBuffer);
					}
				}
	
				oBufReader.close();
			
		return sb.toString();
	}

	
	
}
