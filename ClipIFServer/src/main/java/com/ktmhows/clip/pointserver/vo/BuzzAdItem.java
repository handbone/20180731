package com.ktmhows.clip.pointserver.vo;

public class BuzzAdItem {
	
	private int id;
	private String title;
	private String description;
	private String action_description;
	private String icon;
	private int revenue;
	private String revenue_type;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAction_description() {
		return action_description;
	}
	public void setAction_description(String action_description) {
		this.action_description = action_description;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getRevenue_type() {
		return revenue_type;
	}
	public void setRevenue_type(String revenue_type) {
		this.revenue_type = revenue_type;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRevenue() {
		return revenue;
	}
	public void setRevenue(int revenue) {
		this.revenue = revenue;
	}
	
	private static final int[][] pre = {{1,65},{2,40},{3,25},{4, 15},{5, 8},{6,7},{7,6},{8,6},{9,5},{10,5},{11,4},{12,4},{13,3},{14,3},{15,3},{16,2},{17,2},{18,2},{19,1},{20,1}};
	public static void main(String[] args) {
		Double cnt = 0.0;
		Double sum = 0.0;
		for (int i =0; i<40000 ;i++) {
			cnt ++;
			sum += getRandomBombPoint() ;
		}
		
		System.out.println(sum+" : "+cnt +" : "+(sum/cnt));
	}
	
	private static void getRandomBombPoint2() {
		int t = 2;
		
		int cnt = 0;
		
		
		int r_cnt= 0;
		int sum = 0;
		int d = 1;
		for (int i = 20; i>0; i--) {
			cnt ++;
			r_cnt =+ d;
			System.out.println(i+" : "+d +" : "+(i*(d)));

			sum += (i*d)-6;
			
			d += t;
		}
		
		Double x = Double.parseDouble(""+sum);
		Double y = Double.parseDouble(""+r_cnt);
		System.out.println(sum+"//"+r_cnt+"//"+(x/y));
	}
	
	private static int getRandomBombPoint() {
		
		//인덱스 범위
		int range = 0;
		for(int i = 0; i < 10; i++) {
			int[] temp = pre[i];
			range += temp[1];
		}

		//조회할 인덱스를 랜덤 생성
		int target_idx = (int) (Math.random() * range);

		int sum = 0;
		int val = 0;
		for(int i = 0; i < 10; i++) {
			int[] temp = pre[i];
			sum += temp[1];
			
			if(sum > target_idx) {
				val = temp[0];
				break;
			}
		}
		
		return val * 10;
	}
}
