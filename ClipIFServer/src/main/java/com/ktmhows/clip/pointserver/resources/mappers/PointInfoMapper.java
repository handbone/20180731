/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.resources.mappers;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ktmhows.clip.pointserver.vo.PointInfoVo;

@Resource(name="sqlSessionFactory")
public interface PointInfoMapper {
	PointInfoVo getPointInfo(String userCi) throws java.lang.Exception;
	List<PointInfoVo> getPointInfoList(Map<String, String> parameters) throws java.lang.Exception;
	List<PointInfoVo> getNewPointInfoList(PointInfoVo historyParamVo) throws java.lang.Exception;
	int getPlusCount(Map<String, String> parameters) throws java.lang.Exception;
	
}
