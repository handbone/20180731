/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.handlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.ktmhows.clip.pointserver.config.exceptions.BadRequestException;
import com.ktmhows.clip.pointserver.config.exceptions.CommonException;
import com.ktmhows.clip.pointserver.config.exceptions.MethodNotAllowedException;
import com.ktmhows.clip.pointserver.config.exceptions.NotFoundException;
import com.ktmhows.clip.pointserver.config.exceptions.UnAuthorizedException;

@ControllerAdvice
public class GlobalControllerExceptionHandler {
	
	private static Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);
	
    @ExceptionHandler(BadRequestException.class)
    public void handleBadRequestException(HttpServletRequest request, HttpServletResponse response, BadRequestException exception) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");
		
		log.append("ERROR : [" + exception.getErrorCode() + "][" + exception.getErrorMsg() + "]\r\n");
		if("401".equals(exception.getErrorCode())){
			logger.error("CLIPPOINTERROR 401");
		}else{
			logger.error("CLIPPOINTERROR 001:handleBadRequestException");
		}
		// 400
		response.sendError(HttpStatus.BAD_REQUEST.value(), exception.getErrorMsg());
    }

    @ExceptionHandler(UnAuthorizedException.class)
    public void handleUnAuthorizedException(HttpServletRequest request, HttpServletResponse response, UnAuthorizedException exception) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");
		log.append("ERROR : [" + exception.getErrorCode() + "][" + exception.getErrorMsg() + "]\r\n");
		logger.error("CLIPPOINTERROR 002:handleUnAuthorizedException");
		// 401
		response.sendError(HttpStatus.UNAUTHORIZED.value(), exception.getErrorMsg());
    }
    
    @ExceptionHandler(NotFoundException.class)
    public void handleNotFoundException(HttpServletRequest request, HttpServletResponse response, NotFoundException exception) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");
		log.append("ERROR : [" + exception.getErrorCode() + "][" + exception.getErrorMsg() + "]\r\n");
		if("402".equals(exception.getErrorCode())){
			logger.error("CLIPPOINTERROR 402");
		}else{
			logger.error("CLIPPOINTERROR 003:handleNotFoundException");
		}
		//404
		response.sendError(HttpStatus.NOT_FOUND.value(), exception.getErrorMsg());
    }
    
    @ExceptionHandler(MethodNotAllowedException.class)
    public void handleMethodNotAllowedException(HttpServletRequest request, HttpServletResponse response, MethodNotAllowedException exception) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");
		log.append("ERROR : [" + exception.getErrorCode() + "][" + exception.getErrorMsg() + "]\r\n");
		logger.error("CLIPPOINTERROR 004:handleMethodNotAllowedException");
		// 405
		response.sendError(HttpStatus.METHOD_NOT_ALLOWED.value(), exception.getErrorMsg());
    }

    @ExceptionHandler(CommonException.class)
    public void handleCommonException(HttpServletRequest request, HttpServletResponse response, CommonException exception) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");
		log.append("ERROR : [" + exception.getErrorCode() + "][" + exception.getErrorMsg() + "]\r\n");
		if("403".equals(exception.getErrorCode())){
			logger.error("CLIPPOINTERROR 403");
		}else{
			logger.error("CLIPPOINTERROR "+exception.getErrorCode()+":"+exception.getErrorMsg());
		}
		
		
		
		if("600".equals(exception.getErrorCode())){
			response.sendError(600, exception.getErrorMsg());
		}else{
			// 500
			response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getErrorMsg());
		}
    }

    @ExceptionHandler(Exception.class)
    public void handleException(HttpServletRequest request, HttpServletResponse response, Exception exception) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");
		if(log != null){
			log.append("ERROR : [" + exception.getLocalizedMessage() + "]\r\n");
			exception.printStackTrace();
			logger.error("CLIPPOINTERROR 006:handleException");
		} else {
			exception.printStackTrace();
		}
		// 500
		response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "관리자에게 문의 요망.");
    }
}
