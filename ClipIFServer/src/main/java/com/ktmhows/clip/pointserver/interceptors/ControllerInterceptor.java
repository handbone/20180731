/*
 * [CLiP Point] version [v1.0]
 * 
 * Copyright © 2016 kt corp. All rights reserved.
 * 
 * This is a proprietary software of kt corp, and you may not use this file except in
 * compliance with license agreement with kt corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of kt corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 * 
 */

package com.ktmhows.clip.pointserver.interceptors;

import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.ktmhows.clip.pointserver.utils.DateUtil;

//@PropertySource("classpath:server-${SERVER_CONF_MODE}.properties")
@PropertySource("classpath:server.properties")
public class ControllerInterceptor extends HandlerInterceptorAdapter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws java.lang.Exception {
        String startTime = DateUtil.getCurrentDateString("HH:mm:ss.SSS");
    	StringBuffer log = new StringBuffer();
    	
    	log.append("\r\n");
    	log.append("===================================================================\r\n");
    	log.append("Request Time=" + startTime + "\r\n");
    	log.append("Request URL::" + request.getRequestURL().toString() + "\r\n");
    	log.append("Request IP=" + request.getRemoteAddr() + "\r\n");
//        logger.info("Start Time=" + startTime);
        request.setAttribute("startTime", startTime.replaceAll("[:]", "").replaceAll("[.]", ""));
        
		try{
			Map<String, String> parameterMap = new HashMap<String, String>();
			
			Enumeration<String> paramNames = request.getParameterNames();
			
			while(paramNames.hasMoreElements()){
				String key = paramNames.nextElement();
				
				if(StringUtils.equals(key.toLowerCase(), "cust_id")
						|| StringUtils.equals(key.toLowerCase(), "ctn")
						|| StringUtils.equals(key.toLowerCase(), "ga_id")
						|| StringUtils.equals(key.toLowerCase(), "user_ci")
						|| StringUtils.equals(key.toLowerCase(), "user_token")){
					log.append("parameter:" + key.toLowerCase() + "=[" + URLDecoder.decode(request.getParameter(key), "UTF-8").replaceAll(" ", "+")+"]\r\n");
					parameterMap.put(key.toLowerCase(), URLDecoder.decode(request.getParameter(key), "UTF-8").replaceAll(" ", "+"));
				} else {
					if("campaign_name".equals(key.toLowerCase())){
						log.append("parameter::" + key.toLowerCase() + "=[" + request.getParameter(key)+"]\r\n");
						parameterMap.put(key.toLowerCase(), request.getParameter(key));
					}else if("user_id".equals(key.toLowerCase())){
						log.append("parameter:::" + key.toLowerCase() + "=[" + URLDecoder.decode(request.getParameter(key), "UTF-8").replaceAll(" ", "+")+"]\r\n");
						parameterMap.put(key.toLowerCase(), URLDecoder.decode(request.getParameter(key), "UTF-8").replaceAll(" ", "+"));
					}else{
						log.append("parameter:::" + key.toLowerCase() + "=[" + URLDecoder.decode(request.getParameter(key), "UTF-8")+"]\r\n");
						parameterMap.put(key.toLowerCase(), URLDecoder.decode(request.getParameter(key), "UTF-8"));
					}
				}
			}
			
			request.setAttribute("parametersMap", parameterMap);
			request.setAttribute("logSb", log);
		} catch(Exception ex){
			ex.printStackTrace();
			logger.error(ex.getMessage());
			logger.error("URLDecoder.decode Error :: request IP:"+request.getRemoteAddr());
			request.setAttribute("logSb", log);
			
		}
        
        return true;
    }
 
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws java.lang.Exception {
		StringBuffer log = (StringBuffer)request.getAttribute("logSb");
        String startTime = (String)request.getAttribute("startTime");
        String endTime = DateUtil.getCurrentDateString("HH:mm:ss.SSS");
        
        log.append("Response Time=" + endTime + "\r\n");
        log.append("Elapsed Time=" + (Long.valueOf(endTime.replaceAll("[:]", "").replaceAll("[.]", ""))-Long.valueOf(startTime)) + "ms\r\n");
        log.append("===================================================================\r\n");

        logger.info(log.toString());
    }

}
