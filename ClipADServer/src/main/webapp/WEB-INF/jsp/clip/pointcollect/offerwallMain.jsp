<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>CLIP</title>
	
	<!-- 이전 CLIP JS 및 CSS 파일
	<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="../js/common/common.js"></script>
	<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
	<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
	bx slider lib
	<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> 
	-->
	
	<!-- to-be  -->
	<link rel="stylesheet" href="../css/lib/swiper.min.css">
	<link rel="stylesheet" href="../css/common.css?20170905">
</head>
<body>
<form id="form" name="form" action="">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden" id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden" id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden" id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden" id="mid" name="mid" value="${mid}"/>
	<input type="hidden" id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden" id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden" id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden" id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	<input type="hidden" id="user_token" name="user_token" value="${user_token}"/>
</form>
<!-- wrap -->
<div class="wrap noheader">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">랜덤 포인트 폭탄</h1>
		<script type="text/javascript">
			function goBack(){
				var back = document.referrer;
				var numberOfEntries = window.history.length;
				if (back == null || back == '' || numberOfEntries == 1) {
					goSubmit0();
				}else{
					window.history.back();
				}
			}
			function goSubmit0() {
				$('#pageType').val('0');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
		</script>
		<a href="javascript:goBack();" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents contents_h">
		<div class="bomb_wrap nobg">
			<div class="bomb_top">
				<img src="../images/temp/bomb_top_ban.jpg" class="img_bomb_ban" alt="">
				<div class="blind">
					<span>3일에 한번 터지는 폭탄! 기다리기 힘드시다면?</span>
					<p>이벤트 참여하면 즉시 폭탄이! 추가 포인트는 더더더!!! </p>
					<p>이미 참여하신 이벤트는 대상에서 제외됩니다.</p>
				</div>
			</div>
			<ul class="bomb_list">
			</ul>
			<div class="btn_wrap"><!-- 170911 . btn_fixed 클래스 삭제 -->
				<a href="javascript:;" class="btn_bomb_popout">폭탄 터트리기</a>
			</div>
		</div>
	</div><!-- // contents -->
	<!-- 170728 추가 -->
	<!-- 로딩 -->
	<div class="layer_pop_wrap loading_clip">
		<div class="deem"></div>
		<div class="loading">
			<img src="../images/common/loading.gif" alt="">
		</div>
	</div>
	<!-- // 로딩 -->
	<!-- //170728 추가 -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
<script>
	$(function() {
		alert('제휴사 사정으로 현재 이용이 불가합니다. 클립포인트 메인페이지로 이동합니다.');
		goSubmit0();
// 		var idx;
// 		fn.buzzAdList();
// 		$('.btn_bomb_popout').click(fn.page.submit);
	});
	var fn = {
			cust_id : $('#cust_id').val(),
			ga_id : $('#gaid').val(),
			buzzAdList : function() {
				$.ajax({
					type : 'POST',
					url : '<c:url value="/pointbomb/getOfferwallAdList.do" />',
					dataType : 'Json',
					success : function(resData) {
						var offerAdList = resData.offerwallAdList;
						var result = resData.result;
						var strHTML = '';
						var layerHTML = '';
						var offerAdItem, revDescription;
						
						//console.log('================== RESPONSE_DATA ==================');
						//console.log('RESPONSE_RESULT : ' + result);
						//console.log('OFFERWALL_ADVERTISING_LIST_LENGTH : ' + offerAdList.length);
						//console.log('===================================================');
						
						for(var i=0; i<offerAdList.length; i++) {
							layerHTML = '';
							offerAdItem = offerAdList[i];
							if(offerAdItem.revenue_type == 'cpi') revDescription = '앱 설치형';
							else if(offerAdItem.revenue_type == 'cpe') revDescription = '앱 실행형';
							else if(offerAdItem.revenue_type == 'cpa') revDescription = '액션형';
							else if(offerAdItem.revenue_type == 'cpl') revDescription = '페이스북 좋아요';
							else if(offerAdItem.revenue_type == 'cpb') revDescription = '페이스북 공유';
							else if(offerAdItem.revenue_type == 'cpy') revDescription = '동영상 시청';
							else if(offerAdItem.revenue_type == 'cpinsta') revDescription = '인스타그램 팔로우';
							strHTML += '<li>';
							strHTML += '<a href="javascript:void(0);" onclick="fn.buzzAdDetail(\'pop_bomb\', \'' + i + '\')">';
							strHTML += '<i><img src="' + offerAdItem.icon + '" alt="' + offerAdItem.title + '"></i>';
							strHTML += '<strong>' + offerAdItem.title + '</strong>';
							strHTML += '<p>' + offerAdItem.description + '</p>';
							strHTML += '<span>' + revDescription + '</span>';
							strHTML += '</a>';
							strHTML += '</li>';
							
							layerHTML += '<div class="layer_pop_wrap pop_bomb">';
							layerHTML += '<div class="deem"></div>';
							layerHTML += '<div class="layer_pop">';
							layerHTML += '<div class="lp_hd">';
							layerHTML += '<strong class="title"><img src="' + offerAdItem.icon + '" alt="현대아울렛"></strong>';
							layerHTML += '<span>' + offerAdItem.title + '</span>';
							layerHTML += '</div>';
							layerHTML += '<div class="lp_ct">';
							layerHTML += '<div class="point_tf_cont">';
							layerHTML += '<div class="inr ex_txt">';
							layerHTML += '<strong>[참여방법]</strong>';
							layerHTML += '본 이벤트 참여를 완료하시면 포인트 폭탄을 바로 터트릴 수 있으며, 랜덤 포인트 선물이 제공됩니다.';
							layerHTML += '<br>';
							layerHTML += '<br>';
							layerHTML += '<strong>[주의사항]</strong>';
							layerHTML += '<p>';
							layerHTML += '<span>-</span> 이미 참여한 이벤트의 경우 대상에서 제외됩니다. 다른 이벤트에 참여해주세요. 3G/4G에서는 데이터 이용료가 발생할 수 있습니다.';
							layerHTML += '</p>';
							layerHTML += '<p>';
							layerHTML += '<span>-</span> 페이스북 좋아요 또는 인스타그램 팔로우 이벤트의 경우, 30일 이내 취소할 경우 이후 이벤트 참여시 불이익을 받을 수 있습니다.';
							layerHTML += '</p>';
							layerHTML += '<p>';
							layerHTML += '<span>-</span> 앱 이벤트의 경우, 본 이벤트와 관련 없는 단순 홍보, 비방 등의 내용을 해당 앱 리뷰에 등록 시, 현재 이용중인 서비스에 제한을 받을 수 있습니다.';
							layerHTML += '</p>';
							layerHTML += '</div>';
							layerHTML += '</div>';
							layerHTML += '<div class="btn_multi_box">';
							layerHTML += '<button type="button" class="btn_lp_cancel" onclick="javascript:fn.buzzAdClose(\'pop_bomb\', \'' + i + '\')">취소</button>';
							layerHTML += '<button type="submit" class="btn_lp_confirm" onclick="javascript:fn.buzzAdMove(\'pop_bomb\', \'' + offerAdItem.id + '\', ' + i + ')">확인</button>';
							layerHTML += '</div>';
							layerHTML += '</div>';
							layerHTML += '</div>';
							layerHTML += '</div>';
							$('.wrap').append(layerHTML);
						}
						$('.bomb_list').html(strHTML);
					},
					error : function(errData) {
						alert('네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.');
						console.log(errData.status + ' ERROR');
					},
					beforeSend : function() {
						loadingClip.openClip();
					},
					complete : function() {
						loadingClip.closeClip();
					}
				});
			},
			buzzAdDetail : function(str, idx) {
				$('.'+str).eq(idx).show();
				$('.'+str).eq(idx).on('scroll touchmove mousewheel', function(event) {
					event.preventDefault();
					event.stopPropagation();
					return false;
				});
				$('.'+str).eq(idx).find('.deem').on('scroll touchmove mousewheel', function(event) {
					event.preventDefault();
					event.stopPropagation();
					return false;
				});
			},
			buzzAdMove : function(str, id, idx) {
				var param = {
					'cust_id' : fn.cust_id,
					'banner_id' : id,
					'ga_id' : fn.ga_id
				};
				$.ajax({
					type : 'post',
					url : '<c:url value="/pointbomb/offerwallAdJoin.do" />',
					dataType : 'Json',
					data : param,
					success : function(resData) {
						var result = resData.result;
						var resultMsg = resData.resultMsg;
						var url = '';
						if(resData.landing_url != null) {
							url = resData.landing_url;
						}
						
						$('.'+str).eq(idx).hide();
		        		$('.'+str).eq(idx).find('.deem').off('scroll touchmove mousewheel');
		        		
						//if(result == 'S') location.href = url;
						if(result == 'S') window.location = 'KTolleh00114://gobrowser?'+url;
						else alert(resultMsg);
					},
					error : function(errData) {
						alert('네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.');
						console.log(errData.status + ' ERROR');
					}
				});
			},
			buzzAdClose : function(str, idx){
		        $('.'+str).eq(idx).hide();
		        $('.'+str).eq(idx).find('.deem').off('scroll touchmove mousewheel');
		    },
			page : {
				submit : function() {
					var param = {
							'cust_id' : $('#cust_id').val()
					};
					$.ajax({
						type : 'POST',
						url : '<c:url value="/pointbomb/checkPointBombAuth.do" />',
						dataType : 'Json',
						data : param,
						beforeSend : function() {
							loadingClip.openClip();
						},
						success : function(resData) {
							var result = resData.result;
							var resultMsg = resData.resultMsg;
							
							if(result == 'F') {
								alert(resultMsg);
							} else {
								$('#form').attr('action', '<c:url value="/pointbomb/pointBombMain.do" />');
								$('#form').submit();
							}
						},
						complete : function(resData) {
							loadingClip.closeClip();
						},
						error : function(errData) {
							alert('네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.');
							console.log(errData.status + ' ERROR');
						}
					});
					//$('#form').attr('action', '<c:url value="/pointbomb/pointBombMain.do" />');
					//$('#form').submit();
				}
			}
	}
</script>
</body>
</html>