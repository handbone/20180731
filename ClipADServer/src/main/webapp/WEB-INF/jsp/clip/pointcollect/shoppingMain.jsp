<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- as-is -->
<!-- 
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
<link rel="stylesheet" href="../css/jquery.bxslider.css"/>
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script>
-->

<!-- to-be -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20171103_">
</head>
<body>
<form id="form" name="form" action="">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="uId" name="uId" value="${user_token}" />
	<input type="hidden"  id="user_token" name="user_token" value="${user_token}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	
	<input type="hidden"  id="pageType" name="pageType" value="" />
</form>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">쇼핑 적립</h1>
		<script type="text/javascript">
			function goBack(){
				var back = document.referrer;
				var numberOfEntries = window.history.length;
				if (back == null || back == '' || numberOfEntries == 1) {
					goSubmit0();
				}else{
					window.history.back();
				}
			}
			function goSubmit0() {
				$('#pageType').val('0');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
		</script>
		<a href="javascript:goBack();" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<h2 class="clippoint_top_title">
				구매하면 포인트가 덤!<br>
				구매하고 클립포인트도 모으세요!
			</h2>
			<!-- CLIP 포인트 -->
			<a href="javascript:;" class="clippoint_box" onclick="javascript:fn.btnAct.redirect('2')">
				<!-- 170719 수정 -->
				<span class="title">클립포인트</span>
				<!-- // 170719 수정 -->
				<strong class="point"><span id="mypoint">0</span><span>P</span></strong>
				<c:if test="${order_sum_show == 'Y' }">
					<c:choose>
						<c:when test="${order_sum eq '' || order_sum eq null}">
							<span class="sub_point">당월 적립 예정포인트 : 0P</span>	
						</c:when>
						<c:otherwise>
							<span class="sub_point">당월 적립 예정포인트 : <c:out value="${order_sum }"/>P</span>	
						</c:otherwise>
					</c:choose>
				</c:if>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<!--  적립 예정 포인트  -->
		<div class="section">
			<div class="section_inner">
				<div class="point_two_wrap">
					<h2 class="section_title">적립 예정 포인트</h2>
					<div class="point_two_box type_color">
						<div class="inr">
							<span class="tit">익월 적립</span>
							<strong class="cash"><span>P</span></strong>
						</div>
						<div class="inr">
							<span class="tit">익익월 적립</span>
							<strong class="cash"><span>P</span></strong>
						</div>
					</div>
					<a href="javascript:;" class="btn_view" onclick="javascript:movePage('<c:url value="/pointcollect/shoppingHistory.do" />', 'form')">내역보기</a>
				</div>
			</div>
		</div>
		<!--  //적립 예정 포인트  -->
		<div class="section">
			<div class="section_inner2">
				<!-- 170726 수정 -->
				<div class="point_list_wrap">
					<a href="#bot" class="btn_view">쇼핑 적립 유의사항 보기</a>
					<!-- 170719 수정 -->
					<ul class="point_list"><!-- 170726 클래스 point_list_wrap 를  point_list 변경-->
						<c:if test="${itemList ne null}">
							<c:forEach var="item" items="${itemList}" varStatus="status" >
								<li onclick="javascript:fn.layer.open('pop_point', '${status.index}');">
									<a class="${item.mall_id} ${item.item_id}" href="javascript:void(0);">
										<img src="../images/temp/${item.mall_image_url}" alt="${item.mall_name}">
										<span><strong>${item.reward}%</strong> 적립</span>
										<%-- <c:if test="${item.mall_id eq '11st' }">
											<span style="color:red;font-size: 10px;margin-bottom:-10px;">(11/1~12/31, 2.1% 적립)</span>
										</c:if> --%>
									</a>
									</a>
								</li>
							</c:forEach>
						</c:if>
					</ul>
					<!-- // 170719 수정 -->
				</div>
				<!-- // 170726 수정 -->
				<!-- 170726 추가 -->
				<dl id="bot" class="note_list">
					<dt>유의사항</dt>
					<dd><span>-</span> 적립 후 익익월 7일이 지나야 클립포인트를 사용하실 수 있습니다.</dd>
					<dd><span>-</span> 배송비, 포인트, 적립금, 쿠폰 등의 사용금액을 제외한 실결제 금액 
					   기준으로 클립포인트가 적립됩니다.</dd>
					<dd><span>-</span> 주문 후 주문취소, 반품 등이 발생하면 클립포인트 적립이 취소됩니
					   다.</dd>
					<dd><span>-</span> 본페이지 내의 쇼핑몰 아이콘을 누르고 경유하여 결제한 상품에만
					 클립포인트가 적립되며 바로 접속 또는 해당 몰 자체 앱을 통해 결제한 상품에 대해서는 적립되지 않습니다.</dd>
				</dl>
				<!-- // 170726 추가 -->
			</div>
		</div>
	</div><!-- // contents -->
	
	<!--  popup --><!-- [D]2017.07.14 수정-->
	<c:forEach var="detailItem" items="${itemList}">
		<div class="layer_pop_wrap pop_point">
			<div class="deem"></div>
			<div class="layer_pop" style="overflow: scroll">
				<div class="lp_hd">
					<!-- 170726 수정 -->
					<strong class="title"><img src="../images/temp/${detailItem.item_image_url}" alt=""></strong>
					<!-- // 170726 수정 -->
					<span>${detailItem.reward}% 적립</span>
				</div>
				<div class="lp_ct">
					<!-- point_tf_cont -->
					<div class="point_tf_cont">
						<div class="inr ex_txt">
							KT CLiP을 통해 쇼핑몰을 방문해서 상품을
							구매하면 추가로 포인트를 적립해 드립니다.<br><br>
			
							<c:if test="${detailItem.description ne ''}">
								<strong>[적립 제외 상품 및 유의사항]</strong>
								<c:out value="${detailItem.description}" escapeXml="false" />
							</c:if>
						</div>
					</div>
					<!-- // point_tf_cont -->
					<div class="btn_multi_box">
						<button type="button" class="btn_lp_cancel" onclick="javascript:fn.layer.close('pop_point');">취소</button>
						<button type="submit" class="btn_lp_confirm" onclick="fn.btnAct.goBrowser('${detailItem.mall_id}', '${user_token}')">쇼핑하러가기</button>
					</div>
				</div>
			</div>
		</div>
	</c:forEach>
	<!--  //popup -->
	<!-- 170728 추가 -->
	<!-- 로딩 -->
	<div class="layer_pop_wrap loading_clip">
		<div class="deem"></div>
		<div class="loading">
			<img src="../images/common/loading.gif" alt="">
		</div>
	</div>
	<!-- // 로딩 -->
	<!-- //170728 추가 -->
	<!--  180119 popup 추가-->
	<%@include file="/WEB-INF/jsp/clip/alert_popup.jsp"%>
	<!--  //180119 popup 추가 -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?ver=1.0.7"></script>
<script src="../js/common/jquery.json-2.4.js"></script>
<script src="../js/common/common.js"></script>
<script src="../js/my_point/pointCommon.js"></script>
<script>
//180119 jyi 추가
function popup_confirm_Act(){
	var type = $('#popup_type').val();
	if(type == ''){
		$('#alert_popup').hide();
	}else{
		$('#alert_popup').hide();
	}
	
}
// //180119 jyi 추가

	$(function(){
		//2018.04.11 jyi
		var userAgent = navigator.userAgent.toLowerCase();
		var osType = null;
	  	if ((userAgent.search("iphone") > -1) || (userAgent.search("ipod") > -1)	|| (userAgent.search("ipad") > -1)){
	    		osType = "IOS";
	    	}else{
	    		osType = "ANDROID";
	    	}
		var obj = new Object();
		obj.custid = $("#cust_id").val();
		obj.os = osType;
		obj.cid = "PNT004";
		fn.sendMidas(obj);
		//\\2018.04.11 jyi
		
		common.invoker.invoke("myPoint");
		fn.savingShow();
	});
	
	
	var fn = {
			savingShow : function() {
				var uId = $('#uId').val();
				var param = {'u_id':uId};
				
				$.ajax({
					type : 'POST',
					url : '<c:url value="/pointcollect/nextSaving.do" />',
					data : param,
					dataType : 'JSON',
					beforeSend : function() {
						loadingClip.openClip();
					},
					success : function(resData) {
						var nextSaving = resData.nextSaving.order_sum == null ? '0' : common.string.setComma(resData.nextSaving.order_sum);
						var next2Saving = resData.next2Saving.order_sum == null ? '0' : common.string.setComma(resData.next2Saving.order_sum);
						
						$('.cash').eq(0).prepend(nextSaving);
						$('.cash').eq(1).prepend(next2Saving);
					},
					error : function(errData) {
						/* alert('네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.'); */
						// 180119 jyi 추가
				    		var _text = '네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.';
						$('#notice_word').empty();
						$('#notice_word').append(_text);
						$('#popup_type').val('default');
						$('#alert_type').hide();
						$('#alert_popup').show();
						// //180119 jyi 추가
						console.log(errData);
					},
					complete : function() {
						loadingClip.closeClip();
					}
					
				});
			},
			layer : {
			    open : function(obj, idx) {
					$('.'+obj).eq(idx).show();
					$(document).css('overflow', 'auto');
					
    		        $('.'+obj).eq(idx).on('scroll touchmove mousewheel', function(event) {
    		           event.preventDefault();
    		           event.stopPropagation();
    		           return false;
    		        });
    		        $('.deem').eq(idx).on('scroll touchmove mousewheel', function(event) {
    		           event.preventDefault();
    		           event.stopPropagation();
    		           return false;
    		        });
			    },
				/* 링크프라이스 상세 레이어팝업 비동기 방식
				open : function(obj, mallId, itemId) {
			    	
			    	var param = {
			    			'mall_id' : mallId,
			    			'item_id' : itemId
			    	};
			    	
			    	$.ajax({
			    		type : 'POST',
			    		url : '<c:url value="/pointcollect/shoppingDetail.do" />',
			    		dataType : 'text',
			    		data : param,
			    		beforeSend : function() {
			    			loadingClip.openClip();
			    		},
			    		success : function(resData) {
			    			
			    			$('.'+obj).html(resData);
			    			$('.'+obj).show();
			    	        $('.'+obj).on('scroll touchmove mousewheel', function(event) {
			    	           event.preventDefault();
			    	           event.stopPropagation();
			    	           return false;
			    	        });
			    	        $('.deem').on('scroll touchmove mousewheel', function(event) {
			    	           event.preventDefault();
			    	           event.stopPropagation();
			    	           return false;
			    	        });
			    		},
			    		error : function(errData) {
			    			alert('네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.');
							console.log(errData);
			    		},
			    		complete : function() {
			    			loadingClip.closeClip();
			    		}
			    	});
			    }, 
			    */
			    close : function(obj) {
			        $('.'+obj).hide();
			        $('.deem').off('scroll touchmove mousewheel');
			    }
			},
			btnAct : {
				redirect : function(pageType) {
					$('#pageType').val(pageType);
					$('#form').attr('action', '<c:url value="/pointmain/main.do" />');
					$('#form').submit();
				},
				goBrowser : function(m, u_id) {
					var mallId = encodeURIComponent(m);
					var uId = encodeURIComponent(u_id);

					window.location = 'KTolleh00114://gobrowser?http://click.linkprice.com/click.php?m=' + mallId + '&a=A100558370&l=0000&u_id=' + uId;
				}
			},
			sendMidas : function(_data){
				console.log(_data);
				$.ajax({
					type: "POST",
				    url: "${pageContext.request.contextPath}/pointmain/sendMidas.do",
				    data: _data,
				    type:"post", 
				    dataType	: "json",
				    success: function (resCode) {
				    		console.log(resCode);
				    },
				    error: function(e){
				    		console.log(e);
				    }
				});
			}
	};
</script>
</body>
</html>