<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- as-is -->
<!-- 
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
<link rel="stylesheet" href="../css/jquery.bxslider.css"/>
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script>
-->

<!-- to-be -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">
</head>
<body>
<form id="form" name="form" action="">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	<input type="hidden"  id="user_token" name="user_token" value="${user_token}"/>
	
	<input type="hidden"  id="pageType" name="pageType" value="" />
	<input type="hidden"  id="coupon_no" name="coupon_no" value=""/>
	<input type="hidden"  id="point_value" name="point_value" value=""/>
	<input type="hidden"  id="coupon_value" name="coupon_value" value=""/>
</form>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">포인트 쿠폰 등록</h1>
		<script type="text/javascript">
			function goBack(){
				var back = document.referrer;
				var numberOfEntries = window.history.length;
				if (back == null || back == '' || numberOfEntries == 1) {
					goSubmit0();
				}else{
					window.history.back();
				}
			}
			function goSubmit0() {
				$('#pageType').val('0');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
		</script>
		<a href="javascript:goBack();" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<!-- 170718 추가 -->
			<h2 class="clippoint_top_title">
				클립 포인트 쿠폰을 등록하세요.<br>
				등록하는 순간, 포인트가 쌓이는 즐거움!
			</h2>
			<!-- // 170718 추가 -->
			<!-- CLIP 포인트 -->
			<a href="javascript:;" class="clippoint_box" onclick="javascript:btnAct.submit()">
				<!-- 170719 수정 -->
				<span class="title">클립포인트</span>
				<!-- // 170719 수정 -->
				<strong class="point"><span id="mypoint">0</span><span>P</span></strong>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<div class="section">
			<div class="section_inner">
				<!-- 170718 수정 -->
				<h2 class="section_title">포인트 쿠폰 코드 등록</h2>
				<!-- // 170718 수정 -->
				<!-- pattern="[0-9]*" inputmode="numeric" min="0" oninput="maxLengthCheck(this)"-->
				<input type="text" name="coupon_number" class="coupon_number" value="<c:out value="${ param.no }" />" placeholder="쿠폰 코드를 입력해 주세요.">
				<div class="btn_multi_box">
					<button type="button" class="btn_cancel" onclick="javascript:window.history.back();">취소</button>
					<button type="submit" class="btn_confirm" onclick="javascript:btnAct.confirm()">확인</button>
				</div>
			</div>
		</div>
	</div><!-- // contents -->
	<!-- 170728 추가 -->
	<!-- 로딩 -->
	<div class="layer_pop_wrap loading_clip">
		<div class="deem"></div>
		<div class="loading">
			<img src="../images/common/loading.gif" alt="">
		</div>
	</div>
	<!-- // 로딩 -->
	<!-- //170728 추가 -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
<script src="../js/common/jquery.json-2.4.js"></script>
<script src="../js/common/common.js"></script>
<script src="../js/my_point/pointCommon.js"></script>
<script>
var overlap = false;
	$(function(){
		alert('현재 포인트 쿠폰 시스템 개선작업으로 쿠폰등록이 불가합니다. 이용에 불편을 드려 죄송합니다.');
		goSubmit0();
// 		common.invoker.invoke("myPoint");
	});
	
	var btnAct = {
			confirm : function() {
				if(overlap){
					return;
				}else{
					overlap = true;
				}
				$('input[name=coupon_no]').val($('input[name=coupon_number]').val());
				var formData = $('form[name=form]').serialize();
				
				var couponLength = $('input[name=coupon_no]').val().length;
				
				$.ajax({
					type : 'POST',
					url : '<c:url value="/pointcollect/pointCouponInsert.do" />',
					dataType : 'Json',
					data : formData,
					success : function(resData) {
						console.log(resData.result);
						var data = resData.result;
						
						if(data.result == 'F') {
							alert(data.resultMsg);
						} else {
							if(data.point_value != null) {
								$('#point_value').val(data.point_value);
								$('#coupon_value').val(data.coupon_value);								
								$('#form').attr('action', '<c:url value="/pointcollect/pointCouponComplete.do" />').submit();
							} 
							else {
								alert(data.resultMsg);
							}
						}
						overlap = false;
					},
					error : function(errData) {
						alert('네트워크 오류가 발생하였습니다. 관리자에게 문의하세요.');
						console.log(errData);
						overlap = false;
					},
					beforeSend : function() {
						loadingClip.openClip();
					},
					complete : function() {
						loadingClip.closeClip();
					}
				});
			},			
			cancle : function() {
				location.href = '<c:url value="/pointmain/main.do" />';
			},
			submit : function() {
				$('#pageType').val('2');
				$('#form').attr('action', '<c:url value="/pointmain/main.do" />').submit();
			}
	};
	function maxLengthCheck(object){
		if (object.value.length > object.maxLength){
			object.value = object.value.slice(0, object.maxLength);
		}    
	}
</script>
</body>
</html>