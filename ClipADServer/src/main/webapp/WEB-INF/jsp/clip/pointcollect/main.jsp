<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />

<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>

<link rel="stylesheet" type="text/css" href="."/>
<!-- bx slider lib -->
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script>


<!-- to-be -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>

<!-- <link rel="stylesheet" href="../css/lib/swiper.min.css"> -->
<link rel="stylesheet" href="../css/common.css?20170905">
<link rel="stylesheet" type="text/css" href="../css/jquery.bxslider.css"/>

<script type="text/javascript">
	$(function() {
		common.invoker.invoke("myPoint");
	});
</script>
</head>
<body>
<form id="mainForm" name="mainForm" action="">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
</form>
<!-- wrap -->
<div class="wrap noheader">
	<!-- contents1 -->
	<div class="contents">
		<div style="height: 170px;line-height: 170px;text-align: center;background-color: #eee;">IPGROUP UI 최종 작업 후 반영</div>
		<div class="section brbn">
			<ul class="info_list">
				<li>
					<a href="/clip/pointcollect/pointSwapMain.do">
						<strong>카드 포인트 가져오기</strong>
						<em>신용카드 포인트를 모아 클립포인트로!</em>
					</a>
				</li>
				<li>
					<a href="">
						<strong>참여 적립 <i>최대 1000포인트 적립!</i></strong>
						<em>참여만 해도 클립 포인트가 팍팍!</em>
					</a>
				</li>
				<li>
					<a href="">
						<strong>쇼핑 적립 <i>최대 0.1% 적립!</i></strong>
						<em>쇼핑도 하고 클립포인트도 받자!</em>
					</a>
				</li>
				<li>
					<a href="">
						<strong>금융 혜택 <i>적립 최대 7만 포인트 적립</i></strong>
						<em>금융상품을 이용하면 클립포인트가 덤으로!</em>
					</a>
				</li>
				<li>
					<a href="">
						<strong>포인트 쿠폰 등록</strong>
						<em>이벤트로 받은 쿠폰을 등록하세요!</em>
					</a>
				</li>
			</ul>
		</div>
		<div class="ad_area">
			<a href=""><img src="../images/temp/@ad_img1.png" alt="클립포인트 당첨의 행운을 잡으세요! CLIP 매일매일 출석 체크 이벤트"></a>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->

</body>
</html>