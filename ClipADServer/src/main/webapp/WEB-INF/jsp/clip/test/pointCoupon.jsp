<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- 이전 CLIP JS 및 CSS 파일
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
bx slider lib
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> 
-->


<!-- to-be -->
<!-- <link rel="stylesheet" href="../css/jquery.bxslider.css"/> -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">

<script type="text/javascript">
	
</script>
</head>
<body>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">포인트 쿠폰 등록</h1>
		<a href="#" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<!-- 170718 추가 -->
			<h2 class="clippoint_top_title">
				클립 포인트 쿠폰을 등록하세요.<br>
				등록하는 순간, 포인트가 쌓이는 즐거움!
			</h2>
			<!-- // 170718 추가 -->
			<!-- CLIP 포인트 -->
			<a href="#" class="clippoint_box">
				<span class="title">CLIP 포인트</span>
				<strong class="point">37,200<span>P</span></strong>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<div class="section">
			<div class="section_inner">
				<!-- 170718 수정 -->
				<h2 class="section_title">포인트 쿠폰 번호 등록</h2>	
				<!-- // 170718 수정 -->
				<input type="text" class="coupon_number" placeholder="숫자, 특수문자로 이루어진 8자 쿠폰번호">
				<div class="btn_multi_box">
					<button type="button" class="btn_cancel">취소</button>
					<button type="submit" class="btn_confirm">확인</button>
				</div>
			</div>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
</body>
</html>