<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- 이전 CLIP JS 및 CSS 파일
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
bx slider lib
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> 
-->


<!-- to-be -->
<!-- <link rel="stylesheet" href="../css/jquery.bxslider.css"/> -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">

<script type="text/javascript">
	
</script>
</head>
<body>
<!-- wrap -->
<div class="wrap noheader">
	<!-- 170718 추가 -->
	<div class="header main_header">
		<h1 class="page_title">클립포인트</h1>
		<a href="#" class="btn_back"><span>뒤로가기</span></a>
		<button type="button" class="btn_f5"><span>새로고침</span></button>
	</div>
	<!-- // 170718 추가 -->
	<!-- contents -->
	<div class="contents">
		<!-- 170718 추가 -->
		<div class="section">
			<div class="point_maintop">
				<div class="inr_box">
					<!-- 170719 수정 -->
					<span class="title">사용가능 포인트</span>
					<!-- // 170719 수정 -->
					<strong class="point">22,500P</strong>
					<div class="bot">
						<span>잠금화면에서 추가 모으기</span>
						<button type="button" class="btn_onoff on">ON</button>

						<!-- [D] OFF 버튼 -->
						<!-- <button type="button" class="btn_onoff">OFF</button> -->
						<!-- // [D] OFF 버튼 -->

					</div>
				</div>
				<img src="../images/common/main_bg.jpg" class="main_bg" alt="">
			</div>
		</div>
		<!-- //170718 추가 -->
		<!-- 170718 추가 -->
		<ul class="earn_tab tab_act">
			<li>모으기</li>
			<li>사용하기</li>
			<li class="active">이용내역</li>
		</ul>	
		<!-- //170718 추가 -->
		<div class="earn_area">
			<div class="earn_search">
				<select>
					<option>전체</option>
				</select>
				<span>
					<button type="button" class="on">7일</button>
					<button type="button">30일</button>
					<button type="button">90일</button>
				</span>
			</div>
			<ul class="earn_list">
				<li>
					<div class="tit_point">
						<strong>KB 국민카드국민카드국민카드국민카드 발급하면 50,000 클립 포인트 제공</strong>
						<em class="plus">+50,000<i>P</i></em>
					</div>
					<div class="date">
						<span>2017.07.02</span>
						<em>모으기(광고적립)</em>
					</div>
				</li>
				<li>
					<div class="tit_point">
						<strong>신한카드 포인트</strong>
						<em class="plus">+786<i>P</i></em>
					</div>
					<div class="date">
						<span>2017.07.02</span>
						<em>모으기(포인트 가져오기)</em>
					</div>
				</li>
				<li>
					<div class="tit_point">
						<strong>G마켓 상품 구매</strong>
						<em class="plus">+1,688<i>P</i></em>
					</div>
					<div class="date">
						<span>2017.07.02</span>
						<em>모으기(쇼핑적립)</em>
					</div>
				</li>
				<li>
					<div class="tit_point">
						<strong>상그리아 레드 티 Tall</strong>
						<em class="minus">-7,688<i>P</i></em>
					</div>
					<div class="date">
						<span>2017.07.02</span>
						<em>사용하기(기프티쇼 구매)</em>
					</div>
				</li>
			</ul>
		</div>
		<div class="ad_area">
			<a href=""><img src="../images/temp/@ad_img1.png" alt="클립포인트 당첨의 행운을 잡으세요! CLIP 매일매일 출석 체크 이벤트"></a>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
</body>
</html>