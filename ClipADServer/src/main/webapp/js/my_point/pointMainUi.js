//170804 수정 스크롤 막음 제거 원복
/* 레이어 동작 */
var layerAct ={
    open:function(obj){
        $('.'+obj).show();
        $('.'+obj).on('scroll touchmove mousewheel', function(event) {
           event.preventDefault();
           event.stopPropagation();
           return false;
        });
        $('.deem').on('scroll touchmove mousewheel', function(event) {
           event.preventDefault();
           event.stopPropagation();
           return false;
        });
    },
    close:function(obj){
        $('.'+obj).hide();
        $('.deem').off('scroll touchmove mousewheel');
    }
}
 

//1700808 변경
/* 클립포인트 메인 하단 배너 슬라이더 */
var adArea = {
        adAreaSliderEv:function(){
            if($('.ad_area_slider li').length  > 1) {
                var adAreaSlider = new Swiper('.ad_area_slider', {
                    pagination: '.ad_area_slider .pagination_n',
                    paginationClickable: true,
                    loop: true,
                    autoplayDisableOnInteraction: false,
                    autoplay: 4000
//                    autoplay: 3000
                });
            };
        }
    };
	//$('.ad_area_slider').length && adArea.adAreaSliderEv();
    

//170719 추가
/* 클립포인트 메인 tab fixed */
function TopFixed(obj){
    var TopFixed = jQuery('.'+obj);

    var h = 155;

    jQuery(window).scroll(function () {
        if(parseInt(jQuery(window).scrollTop()) > h){
            TopFixed.addClass('jbFixed');
        }else{
        TopFixed.removeClass('jbFixed');
        }
    });

}
$('.earn_tab').length && TopFixed('earn_tab');

//170728 추가
//170803 수정 스크롤 막음
/* 로딩*/
var loadingClip ={
    openClip:function(){
        $('.loading_clip').show();
        $('.loading_clip').on('scroll touchmove mousewheel', function(event) {
           event.preventDefault();
           event.stopPropagation();
           return false;
        });
    },
    closeClip:function(){
        $('.loading_clip').hide();
        $('.loading_clip').off('scroll touchmove mousewheel');
    }
}