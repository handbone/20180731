package clip.integration.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.coopnc.cardpoint.CardPointSwapManager;
import com.coopnc.cardpoint.domain.FtpFileInfo;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.handler.PointHandler_HANA;
import com.coopnc.cardpoint.util.MessageUtil;

import clip.integration.bean.HanaCardMessage;
import clip.integration.bean.PointSwapCancelVo;
import clip.pointswap.dao.PointSwapDao;

@Service("hanaCardClientService")
public class HanaCardClientServiceImpl implements HanaCardClientService {

	Logger logger = Logger.getLogger(this.getClass());
	
	@Value("#{props['API_DOMAIN']}") private String API_DOMAIN;
	
	@Value("#{props['POINT_SWAP_INI_FILE']}") private String pointSwapIniFile;

	private Boolean fakeUserCi;

	@PostConstruct
	public void init() {
		if( API_DOMAIN !=null && API_DOMAIN.contains("127.0.0.1") ) {
			fakeUserCi =true;
		}else {
			fakeUserCi =false;
		}
		logger.info("hana Card fakecheck " + String.valueOf(fakeUserCi));
	}
	
	private String getSwapConfigFile() {
		try {
			String result = new ClassPathResource(pointSwapIniFile).getURI().getPath();
			logger.debug(result);
			return result;
		} catch (IOException e) {
			return "";
		}
	}

	@Autowired
	private PointSwapDao pointSwapDao;
	
	/**
	 * 하나 멤버스 조회 userCi
	 */
	@Override
	@Async
	public Future<HanaCardMessage> getPoint(HanaCardMessage param) {
		try {
			if(fakeUserCi) {
				param.setUser_ci("4+G9UIWSKk3LLecZY33DOFZLCiCrdiyfuBZAMZKU4RzSxPM+7PjtQi0nBMvq/SMTYmVQWOXG63RhMmB7TPqDqA==");
			}
			
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			int ret = pointSwapDao.selectDailySerialNo("hanamembers");
			String trId = manager.makeDefaultTrId("hanamembers", "", ""+ ret, null);
			
			logger.debug(" generated trId : "+trId);
			
			param.setReg_source("hanamembers");
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_HANA(param)).getPoint();
			
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			if(!"S".equals(param.getResult())) {
				if(StringUtils.isEmpty(param.getRes_code())) {
					
					// 통신 오류나 연동 실패 시 03으로 리턴
					param.setResult("F");
					param.setRes_code("03");
				}
			}
			return new AsyncResult<HanaCardMessage>(param);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * user_ci, req_point
	 */
	@Override
	@Async
	public Future<HanaCardMessage> minusPoint(HanaCardMessage param) {
		try {
			if(fakeUserCi) {
				param.setUser_ci("4+G9UIWSKk3LLecZY33DOFZLCiCrdiyfuBZAMZKU4RzSxPM+7PjtQi0nBMvq/SMTYmVQWOXG63RhMmB7TPqDqA==");
			}
			
			if(param.isPointOver() || param.getReq_point() == 0 || param.isCountOver()){
				param.setResult("F");
				return new AsyncResult<HanaCardMessage>(param);
			}
			
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());

			int ret = pointSwapDao.selectDailySerialNo("hanamembers");
			String trId = manager.makeDefaultTrId("hanamembers", "", ""+ ret, null);

			param.setReg_source("hanamembers");
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_HANA(param)).usePoint();
			
			//하나카드의 경우 포인트 초과 실패시 오류를 리턴해준다.
			if("02123".equals(param.getRes_code())){
				param.setPointOver(true);
			}
			
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			pointSwapDao.insertPointSwapHist(param);

			if(!"S".equals(param.getResult())) {
				//응답 코드가 없다면 내부 오류나 응답 지연으로 판단해 취소를 보낸다.
				if(StringUtils.isEmpty(param.getRes_code())) {
					// 망취소를 직접 바로 날리지 않게 하기 위해 배치로 돌아가는 테이블에 insert
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(param);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert hana old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
							
					// 통신 오류나 연동 실패 시 03으로 리턴
					param.setResult("F");
					param.setRes_code("03");
//					HanaCardMessage cancelMsg = new HanaCardMessage();
//					
//					ret = pointSwapDao.selectDailySerialNo("hanamembers");
//					trId = manager.makeDefaultTrId("hanamembers", "", ""+ ret, "");
//
//					cancelMsg.setReg_source("hanamembers");
//					cancelMsg.setUser_ci(param.getUser_ci());
//					cancelMsg.setTrans_id(trId);
//					cancelMsg.setRef_trans_id(param.getTrans_id());
//					cancelMsg.setReq_point(param.getReq_point());
//
//					cancelMsg.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
//					manager.setHandler(new PointHandler_HANA(cancelMsg)).netCancelUse();
//					cancelMsg.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
//					
//					logger.info("PointSwap Cancel Response hana result["+cancelMsg.getResult()+"],resultCode["+cancelMsg.getRes_code()+"], avail point["+cancelMsg.getAvail_point() +"], use point["+cancelMsg.getRes_point()+"]");
//
//					pointSwapDao.insertPointSwapHist(cancelMsg);
				}
			}
			
			return new AsyncResult<HanaCardMessage>(param);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return null;
	}

	@Override
	@Async
	public Future<HanaCardMessage> cancelMinusPoint(HanaCardMessage param) {
		try {
			if(fakeUserCi) {
				param.setUser_ci("4+G9UIWSKk3LLecZY33DOFZLCiCrdiyfuBZAMZKU4RzSxPM+7PjtQi0nBMvq/SMTYmVQWOXG63RhMmB7TPqDqA==");
			}
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			int ret = pointSwapDao.selectDailySerialNo("hanamembers");
			String trId = manager.makeDefaultTrId("hanamembers", "", ""+ ret, null);

			param.setReg_source("hanamembers");
			param.setRef_trans_id(param.getTrans_id());
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_HANA(param)).netCancelUse();
			
			logger.info("PointSwap Cancel Response hana result["+param.getResult()+"],resultCode["+param.getRes_code()+"], avail point["+param.getAvail_point() +"], use point["+param.getRes_point()+"]");
			
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			pointSwapDao.insertPointSwapHist(param);
			
			return new AsyncResult<HanaCardMessage>(param);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return null;
	}
	

	@Override
	public int calculateHanaPointSwap() throws Exception {
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        format.applyPattern("yyyyMMdd");
	    String preDate = format.format(calendar.getTime());
		return calculateHanaPointSwap(preDate);
	}
	
	@Override
	public int calculateHanaPointSwap(String preDate) throws Exception {
		
		try {
			PointSwapInfo param = new PointSwapInfo();
			
			param.setReg_source("hanamembers");
			param.setTrans_req_date(preDate);
			param.setResult("S");
			//전일 거래 내역 조회
			List<PointSwapInfo> pointSwapInfoList = pointSwapDao.selectSwapList(param);
			
			//-------------------------통신에러건 사용거래 추가를 위한 코드------------------------------
			
			PointSwapInfo paramInfo = new PointSwapInfo();
			List<PointSwapInfo> failSwapInfoList= new ArrayList<>();
			paramInfo.setReg_source("hanamembers");
			paramInfo.setTrans_req_date(preDate);
			paramInfo.setTrans_div("U");
			paramInfo.setResult("F");
			int isExist = 0;
			
			for(int i=0; i<pointSwapInfoList.size(); i++) {
				PointSwapInfo data = pointSwapInfoList.get(i);
				
				if("N".equals(data.getTrans_div()) || "C".equals(data.getTrans_div())) {
					for(int j=0; j<pointSwapInfoList.size(); j++) {
						if(data.getRef_trans_id().equals(pointSwapInfoList.get(j).getTrans_id())) {
							isExist++;
							break;
						}
					}
					if(isExist == 0) {
						PointSwapInfo res;
						paramInfo.setTrans_id(data.getRef_trans_id());
						res = pointSwapDao.selectSwapInfo(paramInfo);
						if(res != null) {
							res.setRes_code("00000");
							failSwapInfoList.add(res);
						}						
						
					}
					isExist= 0;
				}
			}
			pointSwapInfoList.addAll(failSwapInfoList);
			
			//---------------------------------------------------------------------------------			
			
			
			logger.debug("HANA pointSwapInfoList : "+ pointSwapInfoList.size());
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			//하나멤버스 핸들러 셋팅
			manager.setHandler(new PointHandler_HANA(param));
			
			int result = 0;
			
			//KT측 대사파일 생성
			FtpFileInfo upFileInfo = new FtpFileInfo();
			upFileInfo.setStandDate(preDate);
			manager.makeLocalRecordFile(upFileInfo, pointSwapInfoList);
			
			logger.debug("Hana local file : "+upFileInfo.getLocalFilePath() +"/"+ upFileInfo.getLocalFileName());
			result = manager.putRecordFile(upFileInfo);
			logger.debug("Hana upResult : "+ result);
			
			//하나멤버스 대사파일 다운로드
			FtpFileInfo downFileInfo = new FtpFileInfo();
			downFileInfo.setStandDate(preDate);
			result = manager.getRecordFile(downFileInfo);
			
			logger.debug("Hana downResult : "+ result);
			
			@SuppressWarnings("unused")
			int hanaUseCnt = 0;
			@SuppressWarnings("unused")
			int hanaCancelCnt = 0;
			int cancelReqCnt = 0;
			
			if(result == 1) {
				
				Map<String, PointSwapInfo> remoteUseSuccess = new HashMap<String, PointSwapInfo>();
				Map<String, PointSwapInfo> remoteCancelSuccess = new HashMap<String, PointSwapInfo>();

				List<PointSwapInfo> remoteSwapInfoList = manager.parseRemoteRecordFile(downFileInfo);
				
				if(remoteSwapInfoList == null || remoteSwapInfoList.size() == 0){
					logger.debug("Hana file has no result");
				} else {
					
					//쓰기 편하게 맵으로 정리
					for(int i = 0; i< remoteSwapInfoList.size() ; i++){
						PointSwapInfo data = remoteSwapInfoList.get(i);
						logger.debug("Hana result ["+ i + "] div["+ data.getTrans_div()+ "] tr_id["+data.getTrans_id() + "] ref_tr_id["+data.getRef_trans_id()+"]");
						
						if("U".equals(data.getTrans_div())){
							remoteUseSuccess.put(data.getTrans_id(), data);
							hanaUseCnt ++;
						}
						
						if("N".equals(data.getTrans_div())){
							remoteCancelSuccess.put(data.getRef_trans_id(), data);
							hanaCancelCnt ++;
						}
					}
					
					for(PointSwapInfo localData : pointSwapInfoList){
						//1. 우리쪽 데이터에서 차감 실패로 남겨진 리스트를 찾는다.
						
						if("U".equals(localData.getTrans_div()) && !"S".equals(localData.getResult())) {
							
							//2. 우리쪽에서 실패인데 하나쪽에서 성공이고 취소 정보도 없는 경우 취소를 날린다. 
							if(remoteUseSuccess.get(localData.getTrans_id()) != null
									&& remoteCancelSuccess.get(localData.getTrans_id()) == null) {
								
								//3.마지막으로 이미 처리 요청된 건인지 한번더 확인한다.
								PointSwapCancelVo predata = pointSwapDao.selectSwapCancelInfo(localData.getIdx());
								if(predata != null && (predata.getStatus() == 0 || predata.getStatus() == 1)) {
									continue;
								}
								
								pointSwapDao.insertPointSwapCancelInfo(localData);
								cancelReqCnt++;
								logger.debug("New Hana point cancel request idx["+ localData.getIdx() + "],user_ci["+ localData.getUser_ci()+ "],"
										+ "tr_id["+localData.getTrans_id() + "],ref_tr_id["+localData.getRef_trans_id()+"]");
							}
						}
					}
					
				}

			} else {
				logger.debug("Hana file download Fail, result : "+ result);
			}
			
			logger.info("Calculate Hana Point result : "+cancelReqCnt);
			
			return 1;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
}
