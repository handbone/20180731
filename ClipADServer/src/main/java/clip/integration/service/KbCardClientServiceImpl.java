package clip.integration.service;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.coopnc.cardpoint.CardPointSwapManager;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.handler.PointHandler_KB;
import com.coopnc.cardpoint.util.MessageUtil;

import clip.integration.bean.KbCardMessage;
import clip.pointswap.dao.PointSwapDao;

@Service("kbCardClientService")
public class KbCardClientServiceImpl implements KbCardClientService {

	Logger logger = Logger.getLogger(this.getClass());
	
	@Value("#{props['POINT_SWAP_INI_FILE']}") private String pointSwapIniFile;
	
	@Value("#{props['API_DOMAIN']}") private String API_DOMAIN;
	
	@Autowired
	private PointSwapDao pointSwapDao;
	
	private Boolean fakeUserCi;
	
	@PostConstruct
	public void init() {
		if( API_DOMAIN !=null && API_DOMAIN.contains("127.0.0.1") ) {
			fakeUserCi =true;
		}else {
			fakeUserCi =false;
		}
		logger.info("shinhan Card fakecheck " + String.valueOf(fakeUserCi));
	}
	
	@Override
	@Async
	public Future<KbCardMessage> getPoint(KbCardMessage param) {
		try {
			if(fakeUserCi) {
				param.setUser_ci("TEST000012345678901234567890123456789012345678901234567890123456789012345678901234567890");
			}
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			int ret = pointSwapDao.selectDailySerialNo("kbpointree");
			String trId = manager.makeDefaultTrId("kbpointree", "", ""+ ret, null);
			
			logger.debug(" generated trId : "+trId);

			param.setReg_source("kbpointree");
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_KB(param)).getPoint();
			
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			if(!"S".equals(param.getResult())) {
				if(StringUtils.isEmpty(param.getRes_code())) {
					
					// 통신 오류나 연동 실패 시 03으로 리턴
					param.setResult("F");
					param.setRes_code("03");
				}
			}
			
			return new AsyncResult<KbCardMessage>(param);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Async
	public Future<KbCardMessage> minusPoint(KbCardMessage param) {
		try {
			logger.debug("kb Point Over :: " + param.isPointOver());
			if(fakeUserCi) {
				param.setUser_ci("TEST000012345678901234567890123456789012345678901234567890123456789012345678901234567890");
			}
			if(param.isPointOver() || param.getReq_point() == 0){
				param.setResult("F");
				return new AsyncResult<KbCardMessage>(param);
			}
			
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());

			int ret = pointSwapDao.selectDailySerialNo("kbpointree");
			String trId = manager.makeDefaultTrId("kbpointree", "", ""+ ret, null);
			
			param.setReg_source("kbpointree");
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_KB(param)).usePoint();
			
			if("02123".equals(param.getRes_code())){
				param.setPointOver(true);
			}
			
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			pointSwapDao.insertPointSwapHist(param);
			
			if(!"S".equals(param.getResult())) {
				//응답 코드가 없다면 내부 오류나 응답 지연으로 판단해 취소를 보낸다.
				if(StringUtils.isEmpty(param.getRes_code())) {
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(param);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert kb old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
//					KbCardMessage cancelMsg = new KbCardMessage();
//					
//					ret = pointSwapDao.selectDailySerialNo("kbpointree");
//					trId = manager.makeDefaultTrId("kbpointree", "", ""+ ret, null);
//
//					cancelMsg.setReg_source("kbpointree");
//					cancelMsg.setUser_ci(param.getUser_ci());
//					cancelMsg.setTrans_id(trId);
//					cancelMsg.setRef_trans_id(param.getTrans_id());
//
//					cancelMsg.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
//					manager.setHandler(new PointHandler_KB(cancelMsg)).netCancelUse();
//					cancelMsg.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
//					
//					logger.info("PointSwap Cancel Response kb result["+cancelMsg.getResult()+"],resultCode["+cancelMsg.getRes_code()+"], avail point["+cancelMsg.getAvail_point() +"], use point["+cancelMsg.getRes_point()+"]");
//					pointSwapDao.insertPointSwapHist(cancelMsg);
					
					// 통신 오류나 연동 실패 시 03으로 리턴
					param.setResult("F");
					param.setRes_code("03");
				}
			}
			
			return new AsyncResult<KbCardMessage>(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Async
	public Future<KbCardMessage> cancelMinusPoint(KbCardMessage param) {
		try {
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			int ret = pointSwapDao.selectDailySerialNo("kbpointree");
			String trId = manager.makeDefaultTrId("kbpointree", "", ""+ ret, null);

			param.setReg_source("kbpointree");
			param.setRef_trans_id(param.getTrans_id());
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_KB(param)).netCancelUse();
			logger.info("PointSwap Cancel Response kb result["+param.getResult()+"],resultCode["+param.getRes_code()+"], avail point["+param.getAvail_point() +"], use point["+param.getRes_point()+"]");
			
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			pointSwapDao.insertPointSwapHist(param);
			
			return new AsyncResult<KbCardMessage>(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Future<KbCardMessage> uploadBalanceAccounts(KbCardMessage param) {
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	private String getSwapConfigFile() {
		try {
			String result = new ClassPathResource(pointSwapIniFile).getURI().getPath();
			logger.debug(result);
			return result;
		} catch (IOException e) {
			return "";
		}
	}
}
