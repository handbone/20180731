package clip.integration.service;

import clip.integration.bean.ClipContract2Message;
import clip.mypoint.bean.MyPointBean;

public interface ClipClientService {

	//user ci 조회 (기존)
	public MyPointBean getUserCi(MyPointBean myPointBean) throws Exception;

	//약정 조회 - 포인트전환용
	public ClipContract2Message getContract2(ClipContract2Message param);

	/**
	 * Midas 통계 로그 API
	 * @param param
	 * @param request
	 * @return
	 */
//	public int sendMidasLog(MidasBean param, HttpServletRequest request) throws Exception;

}
