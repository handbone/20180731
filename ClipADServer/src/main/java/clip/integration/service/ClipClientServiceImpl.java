package clip.integration.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import clip.common.crypto.AES256CipherClip;
import clip.common.util.DateUtil;
import clip.common.util.HmacCrypto;
import clip.common.util.HttpClientUtil;
import clip.integration.bean.ClipContract2Message;
import clip.integration.bean.ClipContract2Message.ClipContractItem;
import clip.mypoint.bean.MyPointBean;
import clip.mypoint.service.MyPointService;

@Service("clipClientService")
public class ClipClientServiceImpl implements ClipClientService {
	
	public static final String MY_SERVICE_ID = "clippoint";
	public static final String GROUP_CODE_SWAP = "GRTRM00001";
	public static final String GROUP_CODE_SHOPPING = "GRTRM00002";
	
	public static final String ITEM_CODE_SHINHAN = "shinhancard";
	public static final String ITEM_CODE_HANA = "hanamembers";
	
	@Value("#{props['CLIP_CONTRACT_URL']}")
	private String CLIP_CONTRACT_URL;
	
//	@Value("#{props['MIDAS_URL']}")
//	private String MIDAS_URL;
	
	Logger logger = Logger.getLogger(this.getClass());

	@SuppressWarnings("unused")
	@Autowired
	private MyPointService myPointService;

	@Override
	public MyPointBean getUserCi(MyPointBean myPointBean) throws Exception {
		//기존 로직
		//return myPointService.getUserCi(myPointBean);

		//더미데이타
		//성공
		myPointBean.setUser_ci("XXXXXXXXXXXX");
		myPointBean.setApp_ver("v1.0.0");

		return myPointBean;
	}
	
	@Override
	public ClipContract2Message getContract2(ClipContract2Message param) {

		String groupCode = "";
		logger.debug(param.getServiceName());
		
		if("pointswap".equals(param.getServiceName())) {
			groupCode = GROUP_CODE_SWAP;
		} else if("shopbuy".equals(param.getServiceName())) {
			groupCode = GROUP_CODE_SHOPPING;
		} else {
			param.setResult("F");
		}
				
		HttpClientUtil conn = null;
		try {
			conn = new HttpClientUtil("POST", CLIP_CONTRACT_URL);
			conn.addHeader("Content-Type", "application/json");
			
			ObjectMapper mapper = new ObjectMapper();
			ClipContractRequestVo req = new ClipContractRequestVo();
			req.setGroupCode(AES256CipherClip.AES_Encode(groupCode));
			req.setCustId(param.getCustId());
			req.setLinkServiceId(AES256CipherClip.AES_Encode(MY_SERVICE_ID));
			
			String _expire = DateUtil.getDate2String(DateUtil.addMinute(30), "yyyyMMddHHmmss");
			req.setExpired(_expire);		//30동안만 요청이 유효

			String plain_custId = AES256CipherClip.AES_Decode(param.getCustId());
			String hmacKey = plain_custId + groupCode + "0000000000000000000000000000000";
			req.setSign(HmacCrypto.getHmacVal(hmacKey.substring(0, 32)
					,_expire + plain_custId + groupCode + "clippoint" + "terms" + "agreelist" + "get"));

			conn.addBody("request_data", mapper.writeValueAsString(req));
			
			logger.info("ContractCheck request_data : " + mapper.writeValueAsString(req));
			
			int ret = conn.sendJson();
			//int ret = HttpStatus.SC_OK;
			logger.info("ContractCheck response status : " + ret);

			if(ret == HttpStatus.SC_OK) {
				String str = conn.getResponseString();
			//	String str = "{\"retcode\":\"OK\",\"retmsg\":\"SUCCESS\",\"data\":{\"termList\":[{\"groupCode\":\"2jG/ibOEUt+MXUh/8IUEpw==\",\"itemCode\":\"hanamembers\",\"version\":\"2.0\",\"itemName\":\"하나멤버스 - 클립포인트 3자 제공 동의\",\"mandatoryYN\":\"Y\",\"userAgreeYN\":\"Y\",\"userAgreeDateTime\":\"20171205173745\"},{\"groupCode\":\"2jG/ibOEUt+MXUh/8IUEpw==\",\"itemCode\":\"shinhancard\",\"version\":\"1.0\",\"itemName\":\"신한카드 - 클립포인트 3자 제공 동의\",\"mandatoryYN\":\"Y\",\"userAgreeYN\":\"Y\",\"userAgreeDateTime\":\"20171205173745\"},{\"groupCode\":\"2jG/ibOEUt+MXUh/8IUEpw==\",\"itemCode\":\"pointswap_bc\",\"version\":\"1.0\",\"itemName\":\"BC카드\",\"mandatoryYN\":\"Y\",\"userAgreeYN\":\"N\",\"userAgreeDateTime\":\"20171205173745\"}]}}";
				logger.debug("res message str = "+str);
				
				Map<String, Object> resData = mapper.readValue(str, HashMap.class);
				logger.debug("res message = "+(String)resData.get("retcode"));
				
				String retcode = (String)resData.get("retcode");

				if(retcode != null && "OK".equals(retcode)) {
					Map<String, Object> obj = (HashMap)resData.get("data");
					
					List<Map<String, Object>> list = (ArrayList)obj.get("termList");
					
					List<ClipContractItem> temp = new ArrayList<ClipContractItem>();
					
					param.setContractShowYn("N");
					
					if("pointswap".equals(param.getServiceName())) {
						for (Map<String, Object> data : list){
							ClipContractItem vo = new ClipContractItem();
							vo.setGroupCode(AES256CipherClip.AES_Decode((String)data.get("groupCode")));
							vo.setUserAgreeYN((String)data.get("userAgreeYN"));
							vo.setItemCode((String)data.get("itemCode"));
							
							if( ( ITEM_CODE_SHINHAN.equals(vo.getItemCode()) || ITEM_CODE_HANA.equals(vo.getItemCode()) ) && !"Y".equals(vo.getUserAgreeYN())  ){
								param.setContractShowYn("Y");
							}
							
							logger.info("ContractCheck result : service["+param.getServiceName()+"], itemName["+vo.getItemCode()+"], retcode["+retcode+"],userAgreeYN["+vo.getUserAgreeYN()+"]");

							temp.add(vo);
						}
						param.setResult("S");
					} else if("shopbuy".equals(param.getServiceName())) {
						for (Map<String, Object> data : list){
							ClipContractItem vo = new ClipContractItem();
							vo.setGroupCode(AES256CipherClip.AES_Decode((String)data.get("groupCode")));
							vo.setUserAgreeYN((String)data.get("userAgreeYN"));
							vo.setItemCode((String)data.get("itemCode"));
							
							if("GRTRM00002".equals(vo.getGroupCode()) && "Y".equals(vo.getUserAgreeYN())){
								param.setContractShowYn("N");
							}

							temp.add(vo);
						}
						
						param.setResult("S");
						logger.info("ContractCheck Success : service["+param.getServiceName()+"],retcode["+retcode+"],contractShowYn["+param.getContractShowYn()+"]");
						
					} else {
						param.setResult("F");
					}

					param.setTermList(temp);

				} else {
					logger.info("ContractCheck Error Fail Response : retcode["+retcode+"] ");
					param.setResult("F");
				}

			} else {
				logger.info("ContractCheck Error HTTP : code["+ret+"] ");
				param.setResult("F");
			}
			
		} catch (Exception e) {
			param.setResult("F");
			e.printStackTrace();
			conn.closeConnection();
			logger.info("ContractCheck Error System : message["+e.getMessage()+"] ");
			logger.error(e.getMessage());
		}

		return param;
		
		
		/*//파라미터 체크
		//* groupCode = AES256(groupCode)
		//* custId = AES256(custId)
		//linkServiceId = AES256("clippoint")
		//expired = timestamp + 30분 (yyyyMMddHHmmss)
		//sign = HexEncode-lowcase(HmacSHA256(expired + custId + groupCode + linkServiceId + "terms" + "agreelist" + "get", 
		//			custId + groupCode + '0000..." (32자))
		//단, 이때 요소값은 원문으로 조홥 (암호화가 안된 평문)

		//더미데이타
		//성공
		param.setRetcode("OK");
		param.setRetmsg("SUCCESS");
		
		List<ClipContractItem> itemList = new ArrayList<ClipContractItem>();
		param.setTermList(itemList);
		
		//포인트전환
		if(ClipContract2Message.GROUP_POINT_SWAP.equals(param.getGroupCode())) {
			//카드전환 - 하나멤버스
			ClipContractItem item1 = new ClipContractItem();
			item1.setGroupCode(ClipContract2Message.GROUP_POINT_SWAP);
			item1.setItemCode(ClipContract2Message.ITEM_HANAMEMBERS);
			item1.setVersion("v1.0.0");
			item1.setItemName("하나멤버스");
			item1.setMandatoryYN("Y");
			item1.setUserAgreeYN("Y");
			item1.setUserAgreeDateTime("20000101000000");
			itemList.add(item1);
			
			//카드전환 - 신한카드
			ClipContractItem item2 = new ClipContractItem();
			item2.setGroupCode(ClipContract2Message.GROUP_POINT_SWAP);
			item2.setItemCode(ClipContract2Message.ITEM_SHINHANCARD);
			item2.setVersion("v1.0.0");
			item2.setItemName("신한카드");
			item2.setMandatoryYN("Y");
			item2.setUserAgreeYN("Y");
			item2.setUserAgreeDateTime("20000101000000");
			itemList.add(item2);
			
			//카드전환 - KB국민카드
			ClipContractItem item3 = new ClipContractItem();
			item3.setGroupCode(ClipContract2Message.GROUP_POINT_SWAP);
			item3.setItemCode(ClipContract2Message.ITEM_KBCARD);
			item3.setVersion("v1.0.0");
			item3.setItemName("국민카드");
			item3.setMandatoryYN("Y");
			item3.setUserAgreeYN("Y");
			item3.setUserAgreeDateTime("20000101000000");
			itemList.add(item2);
		}

		param.setTermList(itemList);
		param.setTxId("XXXXXXXXXXXXXX");*/
	}

	/*
	 sign : 보안 강화에 따른 정보 변조 유무 체크를 위한 전송 정보 문자열 hash 값
	- key 기반 hash 알고리즘 적용 : HmacSHA256
	- Charset : UTF-8
	- key : {cust_id} + {groupCode} + "0000000000000000000000000000000"(32자) 조합에서 left 32byte를 취함
	- sign value = HexEncode(HmacSHA256( {expired}+ {cust_id} + {groupCode} + {linkServiceId} + "terms" +"agreelist" + "get" ))
	- hash 결과값 : hex encoded String
	- 주의 : hash를 위한 문자열 조합시 조합되는 문자열은 암호화가 안된 평문의 문자열
	- hash 절차
	- step1. 전송 문자열 조합 : {expired}+ {cust_id} + {groupCode} + {linkServiceId} + "terms" +"agreelist" + "get"
	- step2. step1의 문자열을 위의 룰로 생성된 key를 이용하여 HmacSHA256 sign 수행
	- step3. step2의 결과 byte를 hex로 encode(lowercase)*/
	
	
	/*@Override
	public int sendMidasLog(MidasBean param, HttpServletRequest request) throws Exception {
		
	    	Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
	    	String date = format.format(calendar.getTime());
	    	param.setDate(date);
		
		boolean valueCheckYn = valueCheck(param);
		
		HttpClientUtil conn = null;
		ObjectMapper mapper = new ObjectMapper();
		int result = 500;
		
		try {
			conn = new HttpClientUtil("POST", MIDAS_URL);
			conn.addHeader("Content-Type", "application/json");
			conn.addHeader("Accept-Encoding", "gzip,deflate");
			conn.addHeader("Content-Length", "237");
			conn.addHeader("Connection", "Keep-Alive");
			conn.addHeader("User-Agent", "Apache-HttpClient/4.1.1");
			
			
			conn.addParameter("custid", param.getCustid());
			conn.addParameter("cid", param.getCid());
			conn.addParameter("pid", param.getPid());
			conn.addParameter("channelid", param.getChannelid());
			conn.addParameter("date", param.getDate());
			conn.addParameter("action", param.getAction());
			conn.addParameter("appver", param.getAppver());
			conn.addParameter("os", param.getOs());
			
			if(valueCheckYn) {
				logger.debug("MIDAS_REQ_MESSAGE : " + conn.toString());
				int ret = conn.sendJson();
				logger.debug("MIDAS_RES_HTTP_STATUS : " + ret);
	
				if(HttpStatus.SC_OK == ret) {
					String str = conn.getResponseString();
					//	String str = "{\"retcode\":\"OK\",\"retmsg\":\"SUCCESS\",\"data\":{\"termList\":[{\"groupCode\":\"2jG/ibOEUt+MXUh/8IUEpw==\",\"itemCode\":\"hanamembers\",\"version\":\"2.0\",\"itemName\":\"하나멤버스 - 클립포인트 3자 제공 동의\",\"mandatoryYN\":\"Y\",\"userAgreeYN\":\"Y\",\"userAgreeDateTime\":\"20171205173745\"},{\"groupCode\":\"2jG/ibOEUt+MXUh/8IUEpw==\",\"itemCode\":\"shinhancard\",\"version\":\"1.0\",\"itemName\":\"신한카드 - 클립포인트 3자 제공 동의\",\"mandatoryYN\":\"Y\",\"userAgreeYN\":\"Y\",\"userAgreeDateTime\":\"20171205173745\"},{\"groupCode\":\"2jG/ibOEUt+MXUh/8IUEpw==\",\"itemCode\":\"pointswap_bc\",\"version\":\"1.0\",\"itemName\":\"BC카드\",\"mandatoryYN\":\"Y\",\"userAgreeYN\":\"N\",\"userAgreeDateTime\":\"20171205173745\"}]}}";
					logger.debug("MIDAS_RES_MESSAGE_STR = "+str);
					
					@SuppressWarnings("unchecked")
					Map<String, Object> resData = mapper.readValue(str, HashMap.class);
					logger.debug("res message = "+(String)resData.get("retcode"));
					result = 200;
				} else {
					String resMsg = conn.getResponseString();
					logger.debug("MIDAS_RES_MESSAGE_STR ERROR = "+resMsg);
					result = 500;
				}
			}
			
		} catch (Exception e) {
			conn.closeConnection();
			logger.error(e.getMessage(), e);
			logger.debug("Plus Point API URL Response : "+e.getMessage());
			result=400;
		} finally {
			conn.closeConnection();
		}
		
		return result;
	}*/


//	private boolean valueCheck(MidasBean param) {
//		
//		if(param.getCid() == null || param.getCid().isEmpty())
//			return false;
//		if(param.getDate() == null || param.getDate().isEmpty())
//			return false;
//		if(param.getOs() == null || param.getOs().isEmpty())
//			return false;
//		if(param.getChannelid() == null || param.getChannelid().isEmpty())
//			return false;
//		if(param.getAppver() == null || param.getAppver().isEmpty())
//			return false;
//		if(param.getAction() == null || param.getAction().isEmpty())
//			return false;
//		
//		return true;
//	}


	public static class ClipContractRequestVo {
		private String groupCode;
		private String custId;
		private String linkServiceId;
		private String expired;		
		private String sign;
		
		public String getGroupCode() {
			return groupCode;
		}
		public void setGroupCode(String groupCode) {
			this.groupCode = groupCode;
		}
		public String getCustId() {
			return custId;
		}
		public void setCustId(String custId) {
			this.custId = custId;
		}
		public String getLinkServiceId() {
			return linkServiceId;
		}
		public void setLinkServiceId(String linkServiceId) {
			this.linkServiceId = linkServiceId;
		}
		public String getExpired() {
			return expired;
		}
		public void setExpired(String expired) {
			this.expired = expired;
		}
		public String getSign() {
			return sign;
		}
		public void setSign(String sign) {
			this.sign = sign;
		}
	}
}
