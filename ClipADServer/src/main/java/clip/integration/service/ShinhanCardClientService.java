package clip.integration.service;

import java.util.concurrent.Future;

import clip.integration.bean.ShinhanCardMessage;

public interface ShinhanCardClientService {

	//포인트 조회
	public Future<ShinhanCardMessage> getPoint(ShinhanCardMessage param);
	
	//포인트 차감
	public Future<ShinhanCardMessage> minusPoint(ShinhanCardMessage param);
	
	//포인트 차감 취소
	public Future<ShinhanCardMessage> cancelMinusPoint(ShinhanCardMessage param);
	
	//일대사
	public Future<ShinhanCardMessage> uploadBalanceAccounts(ShinhanCardMessage param);

	//FAN 체크
	public Future<ShinhanCardMessage> shinhanFanCheck(ShinhanCardMessage param);
	
}
