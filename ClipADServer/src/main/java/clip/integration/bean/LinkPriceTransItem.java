package clip.integration.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LinkPriceTransItem {
	//주문 고유번호
	private String trLogId;
	//주문 일자
	private String yyyymmdd;
	//주문 시간
	private String hhmiss;
	//머천트 아이디
	private String mId;
	//주문 번호
	private String oCd;
	//상품 코드
	private String pCd;
	//상품명
	private String pNm;
	//카테고리 코드
	private String cCd;
	//주문수량
	private String itCnt;
	//판매 합계금액
	private String sales;
	//어필 커미션
	private String commission;
	//매체 사용자 정보
	private String userId;
	//머천트의 주문자 정보
	private String membershipId;
	//사용자 PC 주소
	private String remoteAddr;
	//상태 100-일반, 300-취소신청, 310-취소완료, 200-정산대기, 210-정산완료 MER, 220-정산완료 AFF
	private String status;
	//실적 취소 사유
	private String transComment;

	public String getTrLogId() {
		return trLogId;
	}
	public void setTrLogId(String trLogId) {
		this.trLogId = trLogId;
	}
	public String getYyyymmdd() {
		return yyyymmdd;
	}
	public void setYyyymmdd(String yyyymmdd) {
		this.yyyymmdd = yyyymmdd;
	}
	public String getHhmiss() {
		return hhmiss;
	}
	public void setHhmiss(String hhmiss) {
		this.hhmiss = hhmiss;
	}
	public String getmId() {
		return mId;
	}
	public void setmId(String mId) {
		this.mId = mId;
	}
	public String getoCd() {
		return oCd;
	}
	public void setoCd(String oCd) {
		this.oCd = oCd;
	}
	public String getpCd() {
		return pCd;
	}
	public void setpCd(String pCd) {
		this.pCd = pCd;
	}
	public String getpNm() {
		return pNm;
	}
	public void setpNm(String pNm) {
		this.pNm = pNm;
	}
	public String getcCd() {
		return cCd;
	}
	public void setcCd(String cCd) {
		this.cCd = cCd;
	}
	public String getItCnt() {
		return itCnt;
	}
	public void setItCnt(String itCnt) {
		this.itCnt = itCnt;
	}
	public String getSales() {
		return sales;
	}
	public void setSales(String sales) {
		this.sales = sales;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMembershipId() {
		return membershipId;
	}
	public void setMembershipId(String membershipId) {
		this.membershipId = membershipId;
	}
	public String getRemoteAddr() {
		return remoteAddr;
	}
	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTransComment() {
		return transComment;
	}
	public void setTransComment(String transComment) {
		this.transComment = transComment;
	}		
}
