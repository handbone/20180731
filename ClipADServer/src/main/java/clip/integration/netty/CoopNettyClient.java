package clip.integration.netty;

import javax.annotation.PostConstruct;

import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import clip.integration.bean.CoopMessage;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import io.netty.handler.codec.bytes.ByteArrayEncoder;
import io.netty.util.concurrent.GenericFutureListener;

@Component
public class CoopNettyClient {
	private static Logger logger = LoggerFactory.getLogger(CoopNettyClient.class);
	
	private static final int COOP_MSG_LENGTH = 300;
	private static final String _SPACE = " ";
	
	//쿠프 쿠폰 서버
	//@Value("#{props['COOP_COUPON_SERVER_IP']}")
	private String SERVER_IP;
	//@Value("#{props['COOP_COUPON_SERVER_PORT']}")
	private int SERVER_PORT;
	
	//쿠프 쿠폰 서버 - clip 서버 코드 , 패스워드
	//@Value("#{props['COOP_COUPON_COMP_CODE']}")
	private String COMP_CODE;
	//@Value("#{props['COOP_COUPON_COMP_PASS']}")
	private String COMP_PASS;
	
	private static ChannelFuture channelFuture = null;
	
	/*@PostConstruct
	public void init() {
		reconnectIfNot(SERVER_IP, SERVER_PORT);
	}*/

	public ResponseFuture send(final CoopMessage reqMsg) throws Exception {
		final ResponseFuture responseFuture = new ResponseFuture();

		final byte[] msg = makeCoopReqMsg(reqMsg);
		
		channelFuture.addListener(new GenericFutureListener<ChannelFuture>() {
			@Override 
			public void operationComplete(ChannelFuture future) throws Exception {
				channelFuture.channel().pipeline().get(CoopChannelInboundHandler.class).setResponseFuture(responseFuture);
				channelFuture.channel().writeAndFlush(msg);
				logger.debug(" writeAndFlush done !!!!! "+reqMsg.toString());
			}
		});

		return responseFuture;
	}

	public void close() {
		channelFuture.channel().close();
	}
	 
	//isActive,isOpen
	public void reconnectIfNot(String host, int port) {
		if(channelFuture == null || !channelFuture.channel().isActive()) {
			logger.debug("Initializing client and connecting to server..");
			EventLoopGroup workerGroup = new NioEventLoopGroup();

			Bootstrap b = new Bootstrap();
			b.group(workerGroup).channel(NioSocketChannel.class)
			 		.option(ChannelOption.SO_KEEPALIVE, true)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						protected void initChannel(SocketChannel channel) throws Exception {
							channel.pipeline().addLast("framerDecoder", new FixedLengthFrameDecoder(COOP_MSG_LENGTH));
							channel.pipeline().addLast("decode", new ByteArrayDecoder());
							channel.pipeline().addLast("encoder", new ByteArrayEncoder());
							channel.pipeline().addLast("handler", new CoopChannelInboundHandler());	
						}
					});

			channelFuture = b.connect(host, port);
		}
	}
	
	class CoopChannelInboundHandler extends SimpleChannelInboundHandler<Object> {

		private ResponseFuture responseFuture;

		public void setResponseFuture(ResponseFuture future) {
			this.responseFuture = future;		
		}

		@Override
		protected void channelRead0(ChannelHandlerContext ctx, Object msg)
				throws Exception {
			
			byte[] bmsg = (byte[])msg;
			
			logger.debug("[client][in] " + new String(bmsg));
			
			CoopMessage resMsg = new CoopMessage();
			parseCoopResMsg(bmsg, resMsg);
			
			responseFuture.set(resMsg);
		}
	}

	private byte[] makeCoopReqMsg(CoopMessage coopMessage){

		//메세지 초기화 - 모두 스페이스로 채움
		byte[] msg = new byte[COOP_MSG_LENGTH];
		
		byte[] _space = _SPACE.getBytes();
		for (int i = 0 ; i < msg.length ; i++){
			msg[i] = _space[0];
		}
		
		System.arraycopy("0300".getBytes(), 0, msg, 0, 4);	//전문길이
		System.arraycopy("14".getBytes(), 0, msg, 4, 2);	//요청코드
		System.arraycopy("I".getBytes(), 0, msg, 6, 1);		//입력구분
		System.arraycopy(StringUtils.rightPad(COMP_CODE, 4, _SPACE).getBytes(), 0, msg, 7, 4);		//TODO 요청처코드
		System.arraycopy(StringUtils.rightPad(COMP_PASS, 20, _SPACE).getBytes(), 0, msg, 11, 20);	//요청처패스워드
		System.arraycopy(StringUtils.rightPad(coopMessage.getGoodId(), 13, _SPACE).getBytes(), 0, msg, 31, 13);	//상품발급코드
		System.arraycopy(StringUtils.rightPad(coopMessage.getTrId(), 20, _SPACE).getBytes(), 0, msg, 44, 20);	//tr_id
		System.arraycopy(StringUtils.rightPad(coopMessage.getFiller01(), 100, _SPACE).getBytes(), 0, msg, 64, 100);	//FILLER01 : user_token
		System.arraycopy(StringUtils.rightPad("", 136, _SPACE).getBytes(), 0, msg, 164, 50+50+36);	//FILLER02+FILLER03+FILLER
		
		return msg;
	}
	
	private void parseCoopResMsg(byte[] msg, CoopMessage coopMessage){
		
		String resCode = getStringFromByteArray(msg, 64, 2);
		
		if("00".equals(resCode) || "71".equals(resCode)) {
			coopMessage.setResult("S");
			coopMessage.setPinId(getStringFromByteArray(msg, 64, 2));
			coopMessage.setCouponId(getStringFromByteArray(msg, 166, 50));
		} else  {
			coopMessage.setResult("F");
		}
	}
	
	private String getStringFromByteArray(byte[] org, int index, int length) {
		byte[] tar = new byte[length];
		System.arraycopy(org, index, tar, 0, length);
		return new String(tar);
	}
	
	/*
	----------- 요청	Length : 300 Byte			
	항목명	필드유형	길이	필수여부	비고	
	전문길이	char	4	Y	'0300' 고정	4
	요청코드	char	2	Y	코드 - 14	6
	입력구분	char	1	Y	I - 요청, O - 회신	7
	요청처코드	char	4	Y		11
	요청처패스워드	char	20	Y		31
	상품발급코드	char	13	Y		44
	거래번호	char	20	Y		64
	FILLER01	char	100	N	user_token	164
	FILLER02	char	50	N	공백	214
	FILLER03	char	50	N	공백	264
	FILLER	char	36	N	공백	300
						
	----------- 회신	Length : 300 Byte				
	항목명	필드유형	길이	필수여부	비고	
	전문길이	char	4	Y	'0300' 고정	4
	요청코드	char	2	Y	코드 - 14	6
	입력구분	char	1	Y	I - 요청, O - 회신	7
	요청처코드	char	4	Y		11
	요청처패스워드	char	20	Y		31
	상품발급코드	char	13	Y		44
	거래번호	char	20	Y		64
	결과코드	char	2	Y	00 - 성공 , 그외 오류	66
	결과메세지	char	100	Y		166
	쿠폰번호	char	50	N	결과 코드 00 , 71 시 회신	216
	핀번호	char	20	N	결과 코드 00 , 71 시 회신	236
	FILLER	char	64	N	공백	300
	*/
	
	/*public static void main(String[] args) {
		
	}*/
}
