package clip.mypoint.bean;

public class CouponState {
	
	private String user_block;
	private String last_try_date;
	private int error_cnt;
	private String user_ci;
	
	public CouponState() {
		// TODO Auto-generated constructor stub
	}
	
	public CouponState(String user_block, int error_cnt, String user_ci) {
		this.user_block = user_block;
		this.error_cnt = error_cnt;
		this.user_ci = user_ci;
	}
	
	public String getUser_block() {
		return user_block;
	}
	public void setUser_block(String user_block) {
		this.user_block = user_block;
	}
	public String getLast_try_date() {
		return last_try_date;
	}
	public void setLast_try_date(String last_try_date) {
		this.last_try_date = last_try_date;
	}
	public int getError_cnt() {
		return error_cnt;
	}
	public void setError_cnt(int error_cnt) {
		this.error_cnt = error_cnt;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}

}
