package clip.mypoint.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import clip.common.util.TelegramUtil;
import clip.integration.bean.CoopMessage;
import clip.integration.service.CoopClientService;
import clip.mypoint.bean.OfflinePayBean;
import clip.mypoint.dao.OfflinePayDao;

@Service("offlinePayService")
public class OfflinePayServiceImpl implements OfflinePayService {
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private OfflinePayDao offlinePayDao;
	
	@Autowired
	private CoopClientService coopClientService;	
	
	@Override
	public List<OfflinePayBean> getItemList(OfflinePayBean param) throws Exception {
		return offlinePayDao.selectOfflinePayItemList();
	}
	
	@Override
	public OfflinePayBean getItemInfo(OfflinePayBean param) throws Exception {
		OfflinePayBean itemInfo = offlinePayDao.selectOfflinePayItem(param);
		
		return itemInfo;
	}
	
	public OfflinePayBean getPinId(OfflinePayBean param) throws Exception {
		
		OfflinePayBean itemInfo = offlinePayDao.selectOfflinePayItem(param);

			CoopMessage msg = new CoopMessage();
			msg.setFiller01(param.getUser_token());
			msg.setGoodId(itemInfo.getItem_id());
			
			String trId = TelegramUtil.createSimpleTrId();
			msg.setTrId(trId);

			CoopMessage resMsg = coopClientService.issueCoupon(msg);

			if("S".equals(resMsg.getResult())){
				itemInfo.setPin_id(resMsg.getPinId());
				itemInfo.setCouponId(resMsg.getCouponId());
			}
		
		return itemInfo;
	}
	
	
	public OfflinePayBean getBarcodeInfo(OfflinePayBean param) throws Exception {
		
		OfflinePayBean itemInfo = offlinePayDao.selectOfflinePayItem(param);

		CoopMessage msg = new CoopMessage();
		msg.setFiller01(param.getUser_token());
		msg.setGoodId(itemInfo.getItem_id());
		msg.setPinId(param.getPin_id());
		
		String trId = TelegramUtil.createSimpleTrId();
		msg.setTrId(trId);
		
		CoopMessage resMsg = coopClientService.getBarcodeUrl(msg);

		itemInfo.setBarcode_image_url(resMsg.getBarcodeImgUrl());

		return itemInfo;
	}

}
