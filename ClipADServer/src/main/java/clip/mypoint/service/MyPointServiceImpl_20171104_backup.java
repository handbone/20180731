///*[CLiP Point] version [v1.0]
//* Copyright © 2016 kt corp. All rights reserved.
//* This is a proprietary software of kt corp, and you may not use this file except in
//* compliance with license agreement with kt corp. Any redistribution or use of this
//* software, with or without modification shall be strictly prohibited without prior written
//* approval of kt corp, and the copyright notice above does not evidence any actual or
//* intended publication of such software.
//*/
//package clip.mypoint.service;
//
//import java.io.UnsupportedEncodingException;
//import java.security.InvalidAlgorithmParameterException;
//import java.security.InvalidKeyException;
//import java.security.NoSuchAlgorithmException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//import java.util.Map;
//import java.util.UUID;
//
//import javax.annotation.Resource;
//import javax.crypto.BadPaddingException;
//import javax.crypto.IllegalBlockSizeException;
//import javax.crypto.NoSuchPaddingException;
//
//import org.apache.log4j.Logger;
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
//import clip.common.crypto.AES256CipherClip;
//import clip.common.util.ClipRequesterCode;
//import clip.framework.HttpNetwork;
//import clip.framework.HttpNetworkHTTPS;
//import clip.mypoint.bean.BannerInfoBean;
//import clip.mypoint.bean.MyPointBean;
//import clip.mypoint.dao.MyPointDao_20171104_backup;
//
//import org.apache.commons.codec.binary.Base64;
//
//@Service("myPointService")
//public class MyPointServiceImpl_20171104_backup implements MyPointService_20171104_backup{
//	Logger log = Logger.getLogger(this.getClass());
//	
//	@Autowired
//	private MyPointDao_20171104_backup myPointDao;
//	
//	@Autowired
//	private HttpNetwork httpNetwork;
//	
//	@Autowired
//	private HttpNetworkHTTPS httpNetworkHTTPS;
//	
//	@Autowired
//	private ClipRequesterCode clipRequesterCode;
//	
//	@Value("#{props['HTTPS_YN']}") private String HTTPS_YN;
//	
//	@SuppressWarnings("unchecked")
//	@Override
//	public MyPointBean getUserCi(MyPointBean myPointBean) throws java.lang.Exception {
//		// TODO Auto-generated method stub
//		log.debug("myPointService/getUserCi");
//		
//		String cust_id = myPointBean.getCust_id();
//		String ctn = myPointBean.getCtn();
//		//String gaid = myPointBean.getGaid();
//		//String mid = myPointBean.getMid();
//		
//		String cert_type ="1";
//		
//		if("".equals(ctn)){
//			Calendar calendar = Calendar.getInstance();
//	        SimpleDateFormat format = new SimpleDateFormat();
//	        calendar.add(Calendar.MINUTE, +30);
//	        format.applyPattern("yyyyMMddHHmmss");
//	        ctn = format.format(calendar.getTime());
//	        ctn = AES256CipherClip.AES_Encode(ctn);
//	        cert_type ="2";
//	    }
//		
//		//String decCtn = AES256CipherClip.AES_Decode(ctn);
//		
//		String user_ci = "";
//		String result_msg = "";
//		String strHtmlSource ="";
//		
//		JSONObject jObject = new JSONObject();
//		
//		jObject.put("cust_id", cust_id);
//		jObject.put("service_id", "clippoint");
//		jObject.put("cert_type", cert_type);
//		jObject.put("cert_key", ctn);
//		
//		String params = jObject.toJSONString();
//		
//		try {
//			//=================================== 테스트 용으로 json 타입으로 변형 =======================================
//				JSONObject jo = new JSONObject();
//				jo.put("ci", "PaqJUszllT4+2DLrPt8ZrWCtPhIj5cnOKzzFhc919VXLhs5m8DTBWk7EtA1XEHvXjofxWfdrYVd11I5m7+xz8w==");
//				jo.put("msg", "SUCCESS");
//				jo.put("result", "OK");
//				jo.put("phone", "AFAPlCjqAerUwBBb4PmdVg==");
//				jo.put("app_ver", "06.00.30");
//				jo.put("os_type", "apple");
//			//============================================================================================================
//			//테스트용
//			strHtmlSource = jo.toString();
//			
////			if("Y".equals(HTTPS_YN)){
////				strHtmlSource = httpNetworkHTTPS.strGetUserCi(params);
////			}else{
////				strHtmlSource = httpNetwork.strGetUserCi(params);
////			}
//			
//			log.debug("strHtmlSource"+strHtmlSource);
//	        JSONParser jsonParser = new JSONParser();
//
//	    	JSONObject jsonObject = (JSONObject) jsonParser.parse(strHtmlSource);
//	    	
//	    	result_msg = jsonObject.get("msg").toString();
////	    	String result = jsonObject.get("result").toString();
//	    	
//	    	if(result_msg.equals("SUCCESS")){
//	    		user_ci = jsonObject.get("ci").toString();
//	    		ctn = jsonObject.get("phone").toString();
//	    		myPointBean.setApp_ver(jsonObject.get("app_ver").toString());
//	    	}
//	    	
//		} catch (Exception e ){
//			log.debug("CLIPPOINTERROR 302:get user_ci http api error.");
//			e.getMessage();
//			log.debug(":::::::::::::::::::::: getUserCi ErrMessage =="+e.getMessage());
//		}	
//		
//		myPointBean.setUser_ci(user_ci);
//		myPointBean.setCtn(ctn);
//		
//		return myPointBean;
//	}
//	
//	@Override
//	public MyPointBean getPoint(MyPointBean myPointBean) throws java.lang.Exception {
//		// TODO Auto-generated method stub
//		String user_ci = myPointBean.getUser_ci();
//		String cust_id = myPointBean.getCust_id();
//		String ctn = myPointBean.getCtn();
//		String gaid = myPointBean.getGaid();
//		String mid = myPointBean.getMid();
//		
//		String strHtmlSource ="";
//		String balance = "";
//		String description = "";
//		
//		String strDo = "getPointRefresh.do";
//		String endUrl = "user_ci="+user_ci+"&cust_id="+cust_id+"&ctn="+ctn+"&ga_id="+gaid+"&mid="+mid;        
//		
//		try {
//			strHtmlSource = httpNetwork.strGetData(strDo,endUrl);
//			
//	        JSONParser jsonParser = new JSONParser();
//
//	    	JSONObject jsonObject = (JSONObject) jsonParser.parse(strHtmlSource);
//	    	
//	    	balance = jsonObject.get("balance").toString();
//	    	if(null != jsonObject.get("description")){
//	    		description = jsonObject.get("description").toString();
//	    	}else{
//	    		description = "";
//	    	}
//	    	
//	    	
//		} catch (Exception e ){
//			balance = "-1";
//			e.getMessage();
//			log.debug("CLIPPOINTERROR 303:getPointRefresh http api error.");
//			log.debug(":::::::::::::::::::::: getPoint ErrMessage =="+e.getMessage());
//		}
//		myPointBean.setBalance(balance);
//		myPointBean.setDescription(description);
//        
//		return myPointBean;
//	}
//	
//	
//	@Override
//	public MyPointBean getPointHistory(MyPointBean myPointBean) throws java.lang.Exception {
//		// TODO Auto-generated method stub
//		String user_ci = myPointBean.getUser_ci();
//		String start_date = myPointBean.getStart_date();
//		String end_date = myPointBean.getEnd_date();
//		
//		String endUrl = "user_ci="+user_ci+"&start_date="+start_date+"&end_date="+end_date;        
//		
//		String strDo = "getPointHistory.do";
//		
//		String strHtmlSource ="";
//		List<JSONObject> saveList = new ArrayList<JSONObject>();
//		List <JSONObject> useList = new ArrayList<JSONObject>();
//		
//		try{
//			strHtmlSource = httpNetwork.strGetData(strDo,endUrl);
//			
//	        JSONParser jsonParser = new JSONParser();
//	        JSONObject jsonObject = (JSONObject) jsonParser.parse(strHtmlSource);
//	        
//	        JSONArray historyArray = (JSONArray)jsonObject.get("history");
//	        
//	        for (int i = 0; i < historyArray.size(); i++) {
//	        	JSONObject tempJSONObject = (JSONObject) historyArray.get(i);
//	        	String point_type = tempJSONObject.get("point_type").toString();
//	        	if(point_type.equals("I")){
//	        		saveList.add(tempJSONObject);
//	        	}else{
//	        		useList.add(tempJSONObject);
//	        	}
//	        }
//	        myPointBean.setResResult("success");
//	        myPointBean.setSaveList(saveList);
//	        myPointBean.setUseList(useList);
//	        
//		}catch(Exception e){
//			e.getMessage();
//			myPointBean.setResResult("failure");
//			log.debug("CLIPPOINTERROR 304:getPointHistory http api error.");
//			log.debug(":::::::::::::::::::::: getPointHistory ErrMessage =="+e.getMessage());
//		}
//
//		return myPointBean;
//	}
//	
//	@Override
//	public BannerInfoBean getBannerInfo(BannerInfoBean bannerInfoBean) throws java.lang.Exception{
//		List<BannerInfoBean> list = myPointDao.getBannerInfo(bannerInfoBean);
//		bannerInfoBean.setBannerInfoList(list);
//		return bannerInfoBean;
//	}
//
//	@Override
//	public String getUserToken(String userCi) throws java.lang.Exception {
//
//		String userToken = null;
//		try {
//			userToken = myPointDao.getUserToken(userCi);
//			if(userToken == null) {
//				userToken = AES256CipherClip.AES_Encode(UUID.randomUUID().toString().replaceAll("-", "")).replaceAll("\\+", "P").replaceAll("\\/", "S");
//				myPointDao.insertUserToken(userCi, userToken);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			
//			throw e;
//		}
//		
//		return userToken;
//		
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public MyPointBean getPointHistoryNew(MyPointBean myPointBean) throws Exception {
//		// TODO Auto-generated method stub
//		String user_ci = myPointBean.getUser_ci();
//		String start_date = myPointBean.getStart_date();
//		String end_date = myPointBean.getEnd_date();
//		
//		String listType = myPointBean.getListType();
//		
//		String endUrl = "user_ci="+user_ci+"&start_date="+start_date+"&end_date="+end_date;        
//		
//		String strDo = "getPointHistoryNew.do";
//		
//		String strHtmlSource ="";
//
//		List <JSONObject> histList = new ArrayList<JSONObject>();
//		
//		try{
//			strHtmlSource = httpNetwork.strGetData(strDo,endUrl);
//			
//	        JSONParser jsonParser = new JSONParser();
//	        JSONObject jsonObject = (JSONObject) jsonParser.parse(strHtmlSource);
//	        
//	        JSONArray historyArray = (JSONArray)jsonObject.get("history");
//	        
//	        String sourceObjName = "reg_source_name";
//	        if("I".equals(listType) || "O".equals(listType)) {
//		        for (int i = 0; i < historyArray.size(); i++) {
//		        	JSONObject tempJSONObject = (JSONObject) historyArray.get(i);
//		        	
//		        	String point_type = tempJSONObject.get("point_type").toString();
//		        	if(point_type.equals(listType)){
//		        		
//			        	String requester_code = tempJSONObject.get("reg_source").toString();
//			        	
//			        	if("I".equals(point_type)) {
//			        		if(requester_code.indexOf("pointswap") != -1) {
//			        			tempJSONObject.put(sourceObjName, "[적립] 포인트 가져오기");
//			        		} else {
//			        			tempJSONObject.put(sourceObjName, "[적립] " + clipRequesterCode.getName(requester_code));
//			        		}
//			        	}
//			        	else {
//			        		tempJSONObject.put(sourceObjName, "[차감] " + clipRequesterCode.getName(requester_code));
//			        	}
//		        		
//		        		histList.add(tempJSONObject);
//		        	}
//		        }
//	        } else {
//		        for (int i = 0; i < historyArray.size(); i++) {
//		        	JSONObject tempJSONObject = (JSONObject) historyArray.get(i);
//		        	String point_type = tempJSONObject.get("point_type").toString();
//		        	String requester_code = tempJSONObject.get("reg_source").toString();
//		        	
//		        	if("I".equals(point_type))
//		        		if(requester_code.indexOf("pointswap") != -1) {
//		        			tempJSONObject.put(sourceObjName, "[적립] 포인트 가져오기");
//		        		} else {		        			
//		        			tempJSONObject.put(sourceObjName, "[적립] " + clipRequesterCode.getName(requester_code));
//		        		}
//		        	else
//		        		tempJSONObject.put(sourceObjName, "[차감] " + clipRequesterCode.getName(requester_code));
//
//		        	histList.add(tempJSONObject);
//		        }
//	        }
//	        
//	        myPointBean.setResResult("success");
//	        myPointBean.setHistList(histList);
//	        
//		}catch(Exception e){
//			e.getMessage();
//			myPointBean.setResResult("failure");
//			log.debug("CLIPPOINTERROR 304:getPointHistory http api error.");
//			log.debug(":::::::::::::::::::::: getPointHistory ErrMessage =="+e.getMessage());
//		}
//
//		return myPointBean;
//	}
//
//}
