/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.mypoint.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import clip.mypoint.bean.BannerInfoBean;
import clip.mypoint.bean.MainbannerInfoBean;
import clip.mypoint.bean.MyPointBean;

@Repository("myPointDao")
public class MyPointDaoImpl implements MyPointDao {
	
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public List<BannerInfoBean> getBannerInfo(BannerInfoBean bannerInfoBean) throws java.lang.Exception{
		return sqlSession.selectList("MyPoint.selectBannerInfo", bannerInfoBean);
	}

	@Override
	public String getUserToken(String user_ci) throws Exception {
		return sqlSession.selectOne("MyPoint.selectUserToken", user_ci);
	}

	@Override
	public int insertUserToken(String userCi, String userToken) throws Exception {
		MyPointBean param = new MyPointBean();
		param.setUser_ci(userCi);
		param.setUser_token(userToken);
		return sqlSession.insert("MyPoint.insertUserToken", param);
	}

	@Override
	public List<MainbannerInfoBean> getMainbannerInfo() throws Exception {
		return sqlSession.selectList("MyPoint.selectMainbannerInfo");
	}

	@Override
	public List<MainbannerInfoBean> getMainbannerInfoIOS() throws Exception {
		return sqlSession.selectList("MyPoint.selectMainbannerInfoIOS");
	}

	@Override
	public List<MainbannerInfoBean> getMainbannerInfoAndroid() throws Exception {
		return sqlSession.selectList("MyPoint.selectMainbannerInfoAndroid");
	}

	@Override
	public String selectDebugUser(String user_ci) throws Exception {
		return sqlSession.selectOne("MyPoint.selectDebugUser", user_ci);
	}

	
}
