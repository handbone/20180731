///*[CLiP Point] version [v1.0]
//* Copyright © 2016 kt corp. All rights reserved.
//* This is a proprietary software of kt corp, and you may not use this file except in
//* compliance with license agreement with kt corp. Any redistribution or use of this
//* software, with or without modification shall be strictly prohibited without prior written
//* approval of kt corp, and the copyright notice above does not evidence any actual or
//* intended publication of such software.
//*/
//package clip.mypoint.dao;
//
//import java.util.List;
//
//import org.apache.ibatis.session.SqlSession;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//import clip.mypoint.bean.BannerInfoBean;
//import clip.mypoint.bean.MyPointBean;
//
//@Repository("myPointDao")
//public class MyPointDaoImpl_20171104_backup implements MyPointDao_20171104_backup {
//	
//	Logger log = Logger.getLogger(this.getClass());
//	
//	@Autowired
//	private SqlSession sqlSession;
//	
//	@Override
//	public List<BannerInfoBean> getBannerInfo(BannerInfoBean bannerInfoBean) throws java.lang.Exception{
//		return sqlSession.selectList("MyPoint.selectBannerInfo", bannerInfoBean);
//	}
//
//	@Override
//	public String getUserToken(String userCi) throws Exception {
//		return sqlSession.selectOne("MyPoint.selectUserToken", userCi);
//	}
//
//	@Override
//	public int insertUserToken(String userCi, String userToken) throws Exception {
//		MyPointBean param = new MyPointBean();
//		param.setUser_ci(userCi);
//		param.setUser_token(userToken);
//		return sqlSession.insert("MyPoint.insertUserToken", param);
//	}
//
//}
