/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.mypoint.dao;

import java.util.List;

import clip.mypoint.bean.BannerInfoBean;
import clip.mypoint.bean.MainbannerInfoBean;

public interface MyPointDao {

	List<BannerInfoBean> getBannerInfo(BannerInfoBean bannerInfoBean) throws java.lang.Exception;

	String getUserToken(String userCi) throws java.lang.Exception;
	
	int insertUserToken(String userCi, String userToken) throws java.lang.Exception;

	List<MainbannerInfoBean> getMainbannerInfo() throws java.lang.Exception;

	List<MainbannerInfoBean> getMainbannerInfoIOS() throws java.lang.Exception;

	List<MainbannerInfoBean> getMainbannerInfoAndroid() throws java.lang.Exception;

	String selectDebugUser(String user_ci) throws java.lang.Exception;
	
}
