//package clip.mypoint.controller;
//
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.lang3.StringUtils;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.view.json.MappingJacksonJsonView;
//
//import clip.framework.BaseController;
//import clip.integration.bean.LinkPriceItem;
//import clip.integration.bean.LinkPriceReportItem;
//import clip.integration.bean.LinkPriceReportMessage;
//import clip.integration.bean.ext.CoopPointCouponMessage;
//import clip.integration.service.LinkPriceClientService;
//import clip.mypoint.bean.CouponState;
//import clip.mypoint.bean.PointCouponBean;
//import clip.mypoint.service.PointCouponService;
//import clip.pointswap.service.PointSwapService;
//import clip.roulette.bean.CardPointInfo;
//import clip.roulette.bean.PointSwap;
//
//@Controller
//@RequestMapping("/pointcollect")
//public class PointCollectController_20171104_backup extends BaseController {
//	
//	Logger log = Logger.getLogger(this.getClass());
//	
//	@Resource(name="pointSwapService")
//	private PointSwapService pointSwapService;
//
//	@Autowired
//	private LinkPriceClientService linkPriceClientService;
//	
//	@Autowired
//	private PointCouponService pointCouponService;
//	
//	/** 포인트 모으기 텝 화면(사용하지않음) **/
//	@RequestMapping(value="/main.do")
//	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView("/clip/pointcollect/main");	
//		
//		handOverUserKey(request, mv);
//		
//		return mv;
//	}
//	
//	/** 포인트 스왑 화면 **/
//	@RequestMapping(value="/pointSwapMain.do")
//	public ModelAndView pointSwapMain( HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView("/clip/pointcollect/pointSwapMain");	
//		handOverUserKey(request, mv);
//		
//		//TODO 포인트 조회 등등
//
//		return mv;
//	}
//	
//	/** 포인트 스왑 처리/완료 화면 **/
//	@RequestMapping(value="/pointSwapComplete.do")
//	public ModelAndView pointSwapComplete(@ModelAttribute PointSwap param,
//			HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView("/clip/pointcollect/pointSwapComplete");	
//		handOverUserKey(request, mv);
//		
//		parseSwapReqData(param);
//		
//		mv.addObject("result", "success");
//		mv.addObject("pointInfo", param);
//
//		return mv;
//	}
//	
//	@RequestMapping(value = "/checkShinhanFan.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
//	@ResponseBody
//	public ModelAndView checkShinhanFan(@RequestBody PointSwap param,
//			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("jsonView");
//		
//		PointSwap result = pointSwapService.isShinhanFan(request, param);
//		mav.addObject("result", result.getResult());	
//		mav.addObject("resultMsg", result.getResultMsg());
//
//		return mav;
//	}
//	
//	private static void parseSwapReqData(PointSwap param){
//		String[] cardNames = param.getCardNames();
//		String[] points = param.getPoints();
//		String[] results = param.getResults();
//		//boolean pointOver = Boolean.valueOf(param.getPointOver()).booleanValue();
//		boolean[] pointOvers = param.getPointOvers();
//
//		for (int i =0; i < cardNames.length ;i++) {
//			String cardName = cardNames[i];
//			switch(cardName) {
//				case "hanamembers" : 
//					CardPointInfo hanaInfo = new CardPointInfo();
//					hanaInfo.setCardName(cardName);
//					hanaInfo.setPoint(""+parseInt(points[i]));
//					hanaInfo.setResult(""+results[i]);
//					hanaInfo.setPointOver(pointOvers[i]);
//					param.setHanaPoint(hanaInfo);
//					break;
//				case "kbpointree" : 
//					CardPointInfo kbInfo = new CardPointInfo();
//					kbInfo.setCardName(cardName);
//					kbInfo.setPoint(""+parseInt(points[i]));
//					kbInfo.setResult(""+results[i]);
//					kbInfo.setPointOver(pointOvers[i]);
//					param.setKbPoint(kbInfo);
//					break;
//				case "shinhan" : 
//					CardPointInfo shinhanInfo = new CardPointInfo();
//					shinhanInfo.setCardName(cardName);
//					shinhanInfo.setPoint(""+parseInt(points[i]));
//					shinhanInfo.setResult(""+results[i]);
//					shinhanInfo.setPointOver(pointOvers[i]);
//					param.setShinhanPoint(shinhanInfo);
//					break;
//				default : 
//					break;
//				
//			}
//		}	
//	}
//	
//	private static int parseInt(String str) {
//		int val = 0;
//		
//		if(StringUtils.isEmpty(str))
//			return 0;
//		
//		try {
//			val = Integer.parseInt(str);
//		} catch (Exception e) {}
//		
//		return val;
//	}
//	
//	@RequestMapping(value = "/getCardPoints.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
//	@ResponseBody
//	public ModelAndView getCardPoint(@RequestBody PointSwap param,
//			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("jsonView");
//		
//		param.setRemoteIp(getRemoteAddr(request));
//		
//		PointSwap result = pointSwapService.getCardPoints(request, param);
//
//		mav.addObject("result", "success");
//	    mav.addObject("pointInfo", result);
//
//		return mav;
//	}
//	
//	@RequestMapping(value = "/swapCardPoints.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
//	@ResponseBody
//	public ModelAndView swapCardPoints(@RequestBody PointSwap param,
//			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("jsonView");
//		
//		param.setRemoteIp(getRemoteAddr(request));
//		
//		PointSwap result = pointSwapService.swapCardPoints(request, param);
//		
//		mav.addObject("result", "success");
//	    mav.addObject("pointInfo", result);
//		
//		return mav;
//	}
//
//	/** 쇼핑 적립 메인 **/
//	@RequestMapping(value="/shoppingMain.do")
//	public ModelAndView shoppingMain(HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView("/clip/pointcollect/shoppingMain");
//		
//		String cust_id = (String) request.getParameter("cust_id");
//		String ctn = (String) request.getParameter("ctn");
//		String gaid = (String) request.getParameter("gaid");
//		String offerwall = (String) request.getParameter("offerwall");
//		
//		String user_ci = (String) request.getParameter("user_ci");	
//		//user token 추가
//		String user_token = (String) request.getParameter("user_token");
//		
//		handOverUserKey(request, mv);
//
//		LinkPriceReportMessage param = new LinkPriceReportMessage();
//		param.setuId(user_token);
//		log.debug("user_token ==== "+user_token);
//
//		// [jaeheung] 2017.09.01 해당 페이지 로딩 후 페이지에서 비동기통신으로 가져올 예정
//		// 하기 메서드 참조
////		mv.addObject("nextSaving", linkPriceClientService.getNextSaving(param));
////		mv.addObject("next2Saving", linkPriceClientService.getNext2Saving(param));
//		
//		/**
//		 * 2017.11.1 Lee 적립예정포인조회		
//		 */
//		
//		int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
//		if (day >= 1 && day <= 6) {
//			
//			Calendar cal = Calendar.getInstance();
//			cal.setTime(new Date());
//			cal.add(Calendar.MONTH, -2);
//			String past = new SimpleDateFormat("yyyyMM").format(cal.getTime());
//			
//			LinkPriceReportMessage params = new LinkPriceReportMessage();
//			params.setuId(user_token);
//			params.setYyyymm(past);
//			LinkPriceReportMessage result = linkPriceClientService.getNextSaving(params);
//			String order_sum = result.getOrder_sum();
//			mv.addObject("order_sum", order_sum);
//			mv.addObject("order_sum_show", "Y");
//			
//		}else {
//			mv.addObject("order_sum_show", "N");
//		}
//		
//		/**
//		 * 2017.11.1 Lee 적립예정포인조회		
//		 */
//		
//		LinkPriceItem itemParam = new LinkPriceItem();
//		mv.addObject("itemList", linkPriceClientService.getItemList(itemParam));
//		
//		return mv;
//	}
//	
//	@RequestMapping(value="/nextSaving.do")
//	public @ResponseBody Map<String, Object> savingAjax(HttpServletRequest request) {		
//		Map<String, Object> map = new HashMap<String, Object>();		
//		LinkPriceReportMessage param = new LinkPriceReportMessage();
//		
//		String user_token = (String) request.getParameter("u_id");
//		param.setuId(user_token);
//		log.debug("##################################################################");
//		log.debug("u_id : " + param.getuId());
//		log.debug("##################################################################");
//		
//		map.put("nextSaving", linkPriceClientService.getNextSaving(param));
//		map.put("next2Saving", linkPriceClientService.getNext2Saving(param));
//		
//		return map;
//		
//	}
//	
//	/** 쇼핑 적립 상품 상세 **/
//	@RequestMapping(value="/shoppingDetail.do")
//	public ModelAndView shoppingDetail(@ModelAttribute LinkPriceItem itemParam, HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView("/clip/pointcollect/shoppingDetail");
//		
//		String cust_id = (String) request.getParameter("cust_id");
//		String ctn = (String) request.getParameter("ctn");
//		String gaid = (String) request.getParameter("gaid");
//		String offerwall = (String) request.getParameter("offerwall");
//		
//		String user_ci = (String) request.getParameter("user_ci");	
//		//user token 추가
//		String user_token = (String) request.getParameter("user_token");
//		
//		handOverUserKey(request, mv);
//
//		mv.addObject("itemInfo", linkPriceClientService.getItemInfo(itemParam));
//		
//		return mv;
//	}
//
//	/** 쇼핑 적립 내역 화면 **/
//	@RequestMapping(value="/shoppingHistory.do")
//	public ModelAndView shoppingHistory(HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView("/clip/pointcollect/shoppingHistory");
//		
//		String cust_id = (String) request.getParameter("cust_id");
//		String ctn = (String) request.getParameter("ctn");
//		String gaid = (String) request.getParameter("gaid");
//		String offerwall = (String) request.getParameter("offerwall");
//		
//		String user_ci = (String) request.getParameter("user_ci");	
//		//user token 추가
//		String user_token = (String) request.getParameter("user_token");
//		
//		String listGb = request.getParameter("listGb") == null?"1":request.getParameter("listGb");
//		
//		handOverUserKey(request, mv);
//		
//		//TODO
//		LinkPriceReportMessage param = new LinkPriceReportMessage();
//
//		return mv;
//	}
//	
//	/** 쇼핑 적립 내역 조회 **/
//	@RequestMapping(value="/shoppingHistoryAjax.do")
//	public ModelAndView shoppingHistoryAjax(HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView();
//		mv.setViewName("jsonView");
//		
//		String cust_id = (String) request.getParameter("cust_id");
//		String ctn = (String) request.getParameter("ctn");
//		String gaid = (String) request.getParameter("gaid");
//		String offerwall = (String) request.getParameter("offerwall");
//		
//		String user_ci = (String) request.getParameter("user_ci");	
//		//user token 추가
//		String user_token = (String) request.getParameter("user_token");
//		
//		String listGb = request.getParameter("list_gb") == null?"1":request.getParameter("list_gb");
//		
//		//TODO
//		LinkPriceReportMessage param = new LinkPriceReportMessage();
//		param.setuId(user_token);
//		
//		System.out.println("user_token ==== "+user_token);
//
//		LinkPriceReportMessage linkpriceMsg = null;
//		if("1".equals(listGb)) {
//			linkpriceMsg = linkPriceClientService.getNextSavingHistory(param);
//		} else {
//			linkpriceMsg = linkPriceClientService.getNext2SavingHistory(param);
//		}
//		
//		DescendingObj descending = new DescendingObj();
//		Collections.sort(linkpriceMsg.getOrder_list(), descending);
//		
//		mv.addObject("history", linkpriceMsg);
//		return mv;
//	}
//	
//	/** 쿠폰번호 입력 화면 **/
//	@RequestMapping(value="/pointCouponMain.do")
//	public ModelAndView pointCouponMain(HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView("/clip/pointcollect/pointCouponMain");
//		
//		String cust_id = (String) request.getParameter("cust_id");
//		String ctn = (String) request.getParameter("ctn");
//		String gaid = (String) request.getParameter("gaid");
//		String offerwall = (String) request.getParameter("offerwall");
//		
//		String user_ci = (String) request.getParameter("user_ci");	
//		//user token 추가
//		String user_token = (String) request.getParameter("user_token");
//		
//		handOverUserKey(request, mv);
//
//		return mv;
//	}
//	
//	/** 쿠폰번호 등록 **/
//	@SuppressWarnings("unused")
//	@RequestMapping(value="/pointCouponInsert.do")
//	public ModelAndView pointCouponInsert(PointCouponBean pointCouponBean, HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView();
//		mv.setViewName("jsonView");
//		
//		String cust_id = (String) request.getParameter("cust_id");
//		String ctn = (String) request.getParameter("ctn");
//		String gaid = (String) request.getParameter("gaid");
//		String offerwall = (String) request.getParameter("offerwall");
//		String user_ci = (String) request.getParameter("user_ci");
//		//user token 추가
//		String user_token = (String) request.getParameter("user_token");
//		String coupon_no = request.getParameter("coupon_no");
//
////		if(StringUtils.isEmpty(coupon_no)) {
////			mv.addObject("result", "F");
////			mv.addObject("resultMsg", "쿠폰 번호가 입력되지 않았습니다.");
////		}
////		PointCouponBean result = pointCouponService.checkCoupon(pointCouponBean);
////		mv.addObject("result", result.getResult()); 
////		
////		if("S".equals(result.getResult())) {
////			mv.addObject("coupon_value",result.getCoupon_value());
////			mv.addObject("point_value",result.getPoint_value());
////		} else {
////			mv.addObject("resultMsg",result.getResultMsg());
////		}
//		
//		// [jaeheung] 2017.08.07 실행 로직(주석처리) 수정
//		PointCouponBean result = new PointCouponBean();
//		if(StringUtils.isEmpty(coupon_no)) {
//			result.setResult("F");  // 해당 값으로 내려올 경우 무조건 메시지 팝업
//			result.setResultMsg("쿠폰 번호가 입력되지 않았습니다.");
//		} else {
//			
//			// 2017.9.8일 
//			// Lee : 쿠폰 5회 오류시 10분동안 진행 할 수 없도록 적용하는 로직
//			
//			boolean gotry = false;
//			
//			CouponState check_result = pointCouponService.checkFailCouponCheck(user_ci);
//			if(check_result == null || check_result.getUser_ci() == null || check_result.getUser_ci().isEmpty()) {
//				/**
//				 * user_ci가 없는 경우 디폴트로 값을 삽입
//				 */
//				check_result = new CouponState();
//				check_result.setError_cnt(0);
//				pointCouponService.putCouponUserInfo(user_ci);
//				gotry = true;
//				
//			}else if (check_result.getUser_ci() != null && !check_result.getUser_ci().isEmpty() && check_result.getUser_block().equals("Y")) {
//				
//				/**
//				 * 시간계산 시작 
//				 */
//				SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
//				String today = format.format(new Date(System.currentTimeMillis()));
//				System.out.println("today   : "+today);
//				Date todate_date = format.parse(today);
//				String testday = check_result.getLast_try_date();
//				System.out.println("testday : "+testday);
//				Date test_date = format.parse(testday);
//				
//				
//				long diff = todate_date.getTime() - test_date.getTime();
//				System.out.println("todate_date : "+todate_date.getTime());
//				System.out.println("test_date : "+test_date.getTime());
//				long diffDays = (diff / 1000) / 60;  // 몇분 남았는지 계산
//				diffDays = diffDays + 1;
//				System.out.println("diffDays : "+diffDays);
//				/**
//				 * 시간계산 종료  
//				 */
//				
//				if (diffDays > 10) {  // 10분이 지난 상황
//					pointCouponService.changeCouponBlock(new CouponState("N", 0, user_ci));
//					check_result.setError_cnt(0);
//					gotry = true;
//				}else { // 10분이 지나지 않은 상황
//					result.setResult("F");  // 해당 값으로 내려올 경우 무조건 메시지 팝업
//					result.setResultMsg("포인트 쿠폰 코드를 오류가 5회 잘못 입력하였습니다. 10분간 사용이 제한됩니다.");
//				}
//				
//			}else {
//				gotry = true;
//			}
//			
//			if (gotry) {
//				result = pointCouponService.checkCoupon(pointCouponBean, request);  // 쿠프 서버에 쿠폰값 확인하는 API
//				final String errorcode = result.getErrorcode();
//				switch(errorcode) {
//					case CoopPointCouponMessage.RET_SUCCESS :
//						
//						if("S".equals(result.getResult())) {
//							mv.addObject("coupon_value",result.getCoupon_value());
//							mv.addObject("point_value",result.getPoint_value());
//						}
//						pointCouponService.changeCouponBlock(new CouponState("N", 0, user_ci));
//						
//						break;		//정상적으로 처리되었습니다.
//					case CoopPointCouponMessage.RET_PERIOD_END_ERR : 		// 기간이 만료된 쿠폰입니다.
//					case CoopPointCouponMessage.RET_PAYED_ERR : 			// 이미 사용된 쿠폰입니다.
//					break;	
//					case CoopPointCouponMessage.RET_NOT_ESXIST_ERR : 			// 없는 쿠폰 번호입니다.
//					case CoopPointCouponMessage.RET_IVALID_COUPON_ERR : 		// 비정상 쿠폰 번호입니다.
////						case CoopPointCouponMessage.RET_PERIOD_END_ERR : 		// 기간이 만료된 쿠폰입니다.
////						case CoopPointCouponMessage.RET_PAYED_ERR : 			// 이미 사용된 쿠폰입니다.
//					case CoopPointCouponMessage.RET_CANCEL_ERR : 			// 결제 취소된 쿠폰입니다.
//					default :
//						result.setResult("F");  // 해당 값으로 내려올 경우 무조건 메시지 팝업
//						int count = check_result.getError_cnt();
//						switch (count) {
//							case 0:case 1:case 2:case 3:{
//								pointCouponService.changeCouponBlock(new CouponState("N", count+1, user_ci));
//								result.setResultMsg("포인트 쿠폰 코드를 오류가 "+(count+1)+"회 입니다.");
//							}break;
//							case 4:default:{
//								pointCouponService.changeCouponBlock(new CouponState("Y", count+1, user_ci));
//								result.setResultMsg("포인트 쿠폰 코드를 오류가 "+(count+1)+"회 잘못 입력하였습니다. 10분간 사용이 제한됩니다.");
//							}break;
//						}
//						
//					break;
//				}
//			}
//			
//		}
//		
//		/*
//		포인트 쿠폰 등록 결과 이력 DB 저장 시 결과 코드 추가 됨.
//		RESULT_CODE : 91 - 쿠프마케팅과 연동 실패 시 91 삽입
//		RESULT_CODE : 92 - 포인트 적립 도중 실패 시 92 삽입
//		*/
//		
//		mv.addObject("result", result);
//		return mv;
//	}
//	
//	/** 쿠폰 처리 완료 화면 **/
//	@RequestMapping(value="/pointCouponComplete.do")
//	public ModelAndView pointCouponComplete(HttpServletRequest request, Model model) throws java.lang.Exception {
//		
//		ModelAndView mv = new ModelAndView("/clip/pointcollect/pointCouponComplete");
//		
//		String cust_id = (String) request.getParameter("cust_id");
//		String ctn = (String) request.getParameter("ctn");
//		String gaid = (String) request.getParameter("gaid");
//		String offerwall = (String) request.getParameter("offerwall");
//		
//		String user_ci = (String) request.getParameter("user_ci");	
//		//user token 추가
//		String user_token = (String) request.getParameter("user_token");
//		
//		handOverUserKey(request, mv);
//		
//		mv.addObject("point_value", request.getParameter("point_value").toString());
//		mv.addObject("coupon_value", request.getParameter("coupon_value").toString());
//
//		return mv;
//	}
//	
//	@RequestMapping(value = "/swapTest.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
//	@ResponseBody
//	public ModelAndView  swapTest(@ModelAttribute PointSwap param,
//			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
//		
//		ModelAndView modelAndView = new ModelAndView(new MappingJacksonJsonView());
//		pointSwapService.testSwap();
//		modelAndView.addObject("result", "success");
//		
//		return modelAndView;
//		
//	}
//	
//}
//
//class DescendingObj implements Comparator<LinkPriceReportItem> {
//	 
//    @Override
//    public int compare(LinkPriceReportItem o1, LinkPriceReportItem o2) {
//        return o2.getYyyymmdd().compareTo(o1.getYyyymmdd());
//    }
// 
//}
