/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.mypoint.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import clip.framework.BaseController;
import clip.mypoint.service.MyPointService;

@Controller
@RequestMapping("/test")
public class TesterMainController extends BaseController {
	Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name="myPointService")
	private MyPointService myPointService;
	
	/**테스트 화면 호출 **/
	@RequestMapping(value="/pointSwap.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/test/point_swap");	
		
		/*String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");
		
		if(cust_id != null && gaid != null){
			
			mv.addObject("cust_id", cust_id.replaceAll("[\r\n]", ""));
			if(null != ctn && !"".equals(ctn)){
				mv.addObject("ctn", ctn.replaceAll("[\r\n]", ""));	
			}
			mv.addObject("gaid", gaid.replaceAll("[\r\n]", ""));
			
			if(user_ci != null){
				mv.addObject("user_ci", user_ci.replaceAll("[\r\n]", ""));
			}
			
			if(offerwall != null){
				mv.addObject("offerwall", offerwall.replaceAll("[\r\n]", ""));
			}
			
		}*/
		
		return mv;
	}

}
