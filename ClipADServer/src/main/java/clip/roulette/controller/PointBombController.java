/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import clip.framework.BaseController;
import clip.roulette.bean.BuzzAdItem;
import clip.roulette.bean.PointBombInfo;
import clip.roulette.service.PointBombService;

@Controller
public class PointBombController extends BaseController {
	Logger logger = Logger.getLogger(PointBombController.class.getName());

	@Autowired
	private PointBombService pointBombService;

	/**
	 * 버즈빌 용 - 잠금화면 설정 알림
	 * @param param
	 * @param request
	 * @param response
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(value = "/pointbomb/lockScreenNoti.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public Map<String, Object> lockScreenNoti(PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("lockScreenNoti start!!!");
//		ModelAndView modelAndView = new ModelAndView(new MappingJacksonJsonView());
		Map<String, Object> map = new HashMap<String, Object>();

		String _switch = request.getParameter("use_yn");
		
		if(StringUtils.isEmpty(_switch)){
			// 400
			response.sendError(HttpStatus.BAD_REQUEST.value(), "필수 파라미터 부재! : switch");
			return null;
		} else {
			if("Y".equals(_switch))
				param.setUse_yn("Y");
			else if("N".equals(_switch))
				param.setUse_yn("N");
			else {
				// 400
				response.sendError(HttpStatus.BAD_REQUEST.value(), "필수 파라미터 부재! : switch");
				return null;
			}	
		}

		PointBombInfo result = pointBombService.updatePointBombInfoByLockScreenNoti(param);
//		modelAndView.addObject("result_code", result.getResult_code());
//		modelAndView.addObject("day_count", result.getDay_count());
//		
//		return modelAndView;
		
		map.put("result_code", result.getResult_code());
		map.put("day_count", result.getDay_count());
		
		return map;
	}

	/**
	 * 버즈빌 용 - 레벨업 알림
	 * @param param
	 * @param request
	 * @param response
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(value = "/pointbomb/levelUp.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public Map<String, Object> levelUp(PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("levelUp start!!!");
		
//		ModelAndView modelAndView = new ModelAndView(new MappingJacksonJsonView());
//		
//		PointBombInfo result = pointBombService.levelUp(param);
//		modelAndView.addObject("result_code", result.getResult_code());
//		modelAndView.addObject("day_count", result.getDay_count());
//		modelAndView.addObject("level", result.getLevel());
//		
//		return modelAndView;
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		PointBombInfo result = pointBombService.levelUp(param);
		map.put("result_code", result.getResult_code());
		map.put("day_count", result.getDay_count());
		map.put("level", result.getLevel());
		
		return map;
	}

	/**
	 * 버즈빌 용 - 현재 포인트 폭탄 상태 조회
	 * @param param
	 * @param request
	 * @param response
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(value = "/pointbomb/getInfo.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public Map<String, Object> getInfo(PointBombInfo param,
			HttpServletRequest request , HttpServletResponse response) throws java.lang.Exception {
		logger.debug("getInfo start!!!");
		
//		ModelAndView modelAndView = new ModelAndView(new MappingJacksonJsonView());
//		
//		PointBombInfo result = pointBombService.getPointBombInfo(param);
//		modelAndView.addObject("result_code", result.getResult_code());
//		modelAndView.addObject("cust_id", result.getCust_id());
//		modelAndView.addObject("day_count", result.getDay_count());
//		modelAndView.addObject("day_content", result.getDay_content());
//		modelAndView.addObject("day_link", result.getDay_link());
//		modelAndView.addObject("button_expose_yn", result.getButton_expose_yn());
//		modelAndView.addObject("level", result.getLevel());
//		modelAndView.addObject("bomb_count", result.getBomb_count());
//
//		return modelAndView;
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		PointBombInfo result = pointBombService.getPointBombInfo(param);
		map.put("result_code", result.getResult_code());
		map.put("cust_id", result.getCust_id());
		map.put("day_count", result.getDay_count());
		map.put("day_content", result.getDay_content());
		map.put("day_link", result.getDay_link());
		map.put("button_expose_yn", result.getButton_expose_yn());
		map.put("level", result.getLevel());
		map.put("bomb_count", result.getBomb_count());
		
		return map;
	}
	
	/**
	 * 포인트 폭탄 오퍼월 광고 화면
	 * @param param
	 * @param request
	 * @param response
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(value = "/pointbomb/offerwallMain.do")
	public ModelAndView offerwallMain(@ModelAttribute PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("pointBombMain start!!!");
		
		ModelAndView mav = new ModelAndView("/clip/pointcollect/offerwallMain");
		handOverUserKey(request, mav);

		return mav;
	}
	
	@RequestMapping(value = "/pointbomb/getOfferwallAdList.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ModelAndView  getOfferwallAdList(PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("getOfferwallAdList start!!!");
		
		ModelAndView modelAndView = new ModelAndView(new MappingJacksonJsonView());		
		List<BuzzAdItem> result = pointBombService.getOfferwallAdList();
		
		if(result.size() > 0)
			modelAndView.addObject("result", "S");
		else
			modelAndView.addObject("result", "F");
		
		modelAndView.addObject("offerwallAdList", result);
		
		return modelAndView;
	}

	@RequestMapping(value = "/pointbomb/checkPointBombAuth.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ModelAndView checkBombAuth(PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("pointBombMain start!!!");
		
		ModelAndView mav = new ModelAndView(new MappingJacksonJsonView());

		PointBombInfo result = pointBombService.checkPointBombAuth(param);
		mav.addObject("result", result.getResult());
		mav.addObject("resultMsg", result.getResultMsg());
		
		return mav;
	}
	
	@RequestMapping(value = "/pointbomb/offerwallAdJoin.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ModelAndView offerwallAdJoin(PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("pointBombMain start!!!");
		
		ModelAndView mav = new ModelAndView(new MappingJacksonJsonView());

		PointBombInfo result = pointBombService.checkOfferwallAdAuth(param, request);
		
		mav.addObject("result", result.getResult());
		mav.addObject("resultMsg", result.getResultMsg());
		mav.addObject("landing_url", result.getLanding_url());
		
		return mav;
	}
	
	/**
	 * 포인트 폭탄 메인 화면
	 * @param param
	 * @param request
	 * @param response
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(value = "/pointbomb/pointBombMain.do")
	public ModelAndView pointBombMain(@ModelAttribute PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("pointBombMain start!!!");
		
		ModelAndView mav = new ModelAndView("/clip/pointcollect/pointBombMain");
		handOverUserKey(request, mav);

		return mav;
	}

	@RequestMapping(value = "/pointbomb/pointBombJoin.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ModelAndView  pointBombJoin(PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("pointBombComplete start!!!");
		
		ModelAndView mav = new ModelAndView(new MappingJacksonJsonView());		

		PointBombInfo result = pointBombService.getPointRouletteJoin(param);
		mav.addObject("result", result.getResult());
		mav.addObject("resultMsg", result.getResultMsg());
		mav.addObject("pointBombInfo", result);
		
		return mav;
	}	

	@RequestMapping(value = "/pointbomb/pointBombComplete.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ModelAndView  pointBombComplete(PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("pointBombComplete start!!!");
		
		ModelAndView mav = new ModelAndView(new MappingJacksonJsonView());		
		
		PointBombInfo result = pointBombService.rouletteComplete(param, request);
		
        mav.addObject("reward_point", result.getReward_point());
        mav.addObject("reward_level", result.getReward_level());
        mav.addObject("reward_base_point", result.getReward_base());
        
		mav.addObject("result", result.getResult());
		mav.addObject("resultMsg", result.getResultMsg());
		
		return mav;
	}
	
	/**
	 * 포인트 폭탄 결과 화면
	 * @param param
	 * @param request
	 * @param response
	 * @return
	 * @throws java.lang.Exception
	 */
	@RequestMapping(value = "/pointbomb/pointBombResult.do")
	public ModelAndView pointBombResult(@ModelAttribute PointBombInfo param,
			HttpServletRequest request, HttpServletResponse response ) throws java.lang.Exception {
		logger.debug("pointBombResult start!!!");
		
		ModelAndView mav = new ModelAndView("/clip/pointcollect/pointBombResult");
		handOverUserKey(request, mav);

		return mav;
	}
}
