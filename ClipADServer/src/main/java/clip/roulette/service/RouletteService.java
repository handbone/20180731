/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.service;

import javax.servlet.http.HttpServletRequest;

import clip.roulette.bean.PointRoulette;
import clip.roulette.bean.PointRouletteJoin;
import clip.roulette.bean.RouletteDto;

public interface RouletteService {

	public PointRoulette selectPointRoulette(String roulette_id, HttpServletRequest request) throws java.lang.Exception;
	
	public PointRoulette pointRouletteNoChkDate(String roulette_id, HttpServletRequest request)  throws java.lang.Exception;
	
	public int getDailyCntChk(RouletteDto rouletteDto) throws java.lang.Exception;

	public PointRouletteJoin getPointRouletteJoin(PointRoulette pointRoulette, RouletteDto rouletteDto, PointRouletteJoin pointRouletteJoin, HttpServletRequest request) throws java.lang.Exception;

	public RouletteDto selectId(RouletteDto rouletteDto)  throws java.lang.Exception;

	public PointRouletteJoin selectId(RouletteDto rouletteDto, PointRouletteJoin pointRouletteJoin) throws java.lang.Exception;

	public int rouletteComplete(RouletteDto rouletteDto, PointRouletteJoin pointRouletteJoin,
			HttpServletRequest request)  throws java.lang.Exception;
	
	public int agreeInsert(RouletteDto rouletteDto, HttpServletRequest request)  throws java.lang.Exception;

	public int agreeChk(RouletteDto rouletteDto)  throws java.lang.Exception;

	public String smsAuth(String tel, String roulette_id, HttpServletRequest request)  throws java.lang.Exception;
	
	public int smsAuthChk(String tel, String roulette_id, String smscode, HttpServletRequest request)  throws java.lang.Exception;
	
	

}
