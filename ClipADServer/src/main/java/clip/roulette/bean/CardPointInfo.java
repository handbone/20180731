package clip.roulette.bean;

public class CardPointInfo {
	private String cardName;
	private String result;
	private String resultMsg;
	private String point;
	private boolean pointOver;
	
	
	
	
	public boolean isPointOver() {
		return pointOver;
	}
	public void setPointOver(boolean pointOver) {
		this.pointOver = pointOver;
	}
	public String getCardName() {
		return cardName;
	}
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	public String getPoint() {
		return point;
	}
	public void setPoint(String point) {
		this.point = point;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
}
