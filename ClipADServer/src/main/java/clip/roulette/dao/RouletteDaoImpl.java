/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import clip.roulette.bean.PointApicallLog;
import clip.roulette.bean.PointRoulette;
import clip.roulette.bean.PointRouletteAuth;
import clip.roulette.bean.PointRouletteConnLog;
import clip.roulette.bean.PointRouletteJoin;
import clip.roulette.bean.PointRouletteJoinSum;
import clip.roulette.bean.PointRouletteOptin;
import clip.roulette.bean.PointRouletteSetLog;
import clip.roulette.bean.RouletteDto;

@Repository
public class RouletteDaoImpl implements RouletteDao {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public PointRoulette selectPointRoulette(String roulette_id) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.selectPointRoulette", roulette_id);
	}
	
	@Override
	public PointRoulette pointRouletteNoChkDate(String roulette_id) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.pointRouletteNoChkDate", roulette_id);
	}

	@Override
	public int getDailyCntChk(RouletteDto rouletteDto) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.getDailyCntChk", rouletteDto);
	}

	@Override
	public PointRoulette getSelectedPointRoulette(PointRoulette pointRoulette) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.getSelectedPointRoulette", pointRoulette);
	}

	@Override
	public PointRoulette getSelectedPointRoulette2(PointRoulette pointRoulette) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.getSelectedPointRoulette2", pointRoulette);
	}
	
	@Override
	public int insertPointRouletteJoin(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception {
		return sqlSession.insert("Roulette.insertPointRouletteJoin", pointRouletteJoin);
	}

	@Override
	public void insertPointRouletteConnLog(PointRouletteConnLog pointRouletteConnLog) throws java.lang.Exception {
		sqlSession.insert("Roulette.insertPointRouletteConnLog", pointRouletteConnLog);
	}

	@Override
	public int insertPointApicallLog(PointApicallLog pointApicallLog) throws java.lang.Exception {
		return sqlSession.insert("Roulette.insertPointApicallLog", pointApicallLog);
	}

	@Override
	public int updatePointRouletteJoin(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception {
		
		try{
			return sqlSession.update("Roulette.updatePointRouletteJoin", pointRouletteJoin);
		} catch (Exception e ){
			return sqlSession.update("Roulette.updatePointRouletteJoinLocal", pointRouletteJoin);
		}
	}

	@Override
	public int selectPointRouletteJoinSumByDate(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.selectPointRouletteJoinSumByDate", pointRouletteJoin);
	}

	@Override
	public void insertPointRouletteJoinSum(PointRouletteJoinSum pointRouletteJoinSum) throws java.lang.Exception {
		sqlSession.insert("Roulette.insertPointRouletteJoinSum", pointRouletteJoinSum);		
	}

	@Override
	public int updatePointRouletteJoinSum(PointRouletteJoinSum pointRouletteJoinSum) throws java.lang.Exception {
		return sqlSession.update("Roulette.updatePointRouletteJoinSum", pointRouletteJoinSum);
	}

	@Override
	public int pointRouletteSetLogCnt(String roulette_id) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.pointRouletteSetLogCnt", roulette_id);
	}

	@Override
	public void insertPointRouletteSetLog(PointRouletteSetLog pointRouletteSetLog) throws java.lang.Exception {
		sqlSession.insert("Roulette.insertPointRouletteSetLog", pointRouletteSetLog);	
	}

	@Override
	public void updatePointRoulette(PointRouletteSetLog pointRouletteSetLog) throws java.lang.Exception {
		sqlSession.update("Roulette.updatePointRoulette", pointRouletteSetLog);
	}

	@Override
	public int agreeInsert(PointRouletteOptin pointRouletteOptin) throws java.lang.Exception {
		return sqlSession.insert("Roulette.agreeInsert", pointRouletteOptin);
	}

	@Override
	public int agreeChk(RouletteDto rouletteDto) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.agreeChk",rouletteDto);
	}

	@Override
	public int smsAuthInsert(PointRouletteAuth pointRouletteAuth) throws java.lang.Exception {
		return sqlSession.insert("Roulette.smsAuthInsert", pointRouletteAuth);
	}

	@Override
	public int smsAuthChk(PointRouletteAuth pointRouletteAuth) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.smsAuthChk",pointRouletteAuth);
	}

	@Override
	public PointRouletteJoin selectPointRouletteJoin(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.selectPointRouletteJoin",pointRouletteJoin);
	}
	
	@Override
	public String selectRequesterCode(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.selectRequesterCode",pointRouletteJoin);
	}
	
	@Override
	public String selectRequesterDescription(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.selectRequesterDescription",pointRouletteJoin);
	}
	
	@Override
	public int max06Chk(PointRouletteJoin pointRouletteJoin) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.max06Chk",pointRouletteJoin);
	}

	@Override
	public int max06ChkByEndTime(RouletteDto rouletteDto) throws java.lang.Exception {
		return sqlSession.selectOne("Roulette.max06ChkByEndTime",rouletteDto);
	}

	@Override
	public int updatePointRouletteJoinSimple(String cust_id) throws Exception {
		return sqlSession.update("Roulette.updatePointRouletteJoinSimple",cust_id);
	}
	
}
