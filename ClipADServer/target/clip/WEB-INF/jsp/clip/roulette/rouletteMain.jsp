<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko">
<head>
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
<meta name="HandheldFriendly" content="true" />
<meta name="MobileOptimized" content="320" />

<title>EVENT</title> 

<link type="text/css" href="../css/roulette.css" rel="stylesheet"/>
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/roulette/jQueryRotate.js"></script>


<!-- Contact Form CSS files -->
<link type='text/css' href='../css/basic_simplemodal.css' rel='stylesheet' media='screen' />



<script type="text/javascript">

    jQuery(document).ready(function() { 
		
    	if( 'EndEvent' == '${msg}'){
			  alert("종료된 이벤트입니다.");
			  history.back();
			  return;
		  }
    	
    	var pArr = [ "10", "10000", "1", "2000", "0", "5" ];
    	
		  $("#image").click(function() {
	     
			  if( 'complete' == '${msg}'){
				  $("#dup_proc").val("Y");
			  }
			  
				//중복클릭 방지
				blockContents();
					
				var itemCenter = "5";
				<%//var inAngle = Int((50 * Rnd))+5 // 5~55까지 무작위값 선택된 item 에서 움직여질 각도%>
				var inAngle = (5 * Math.floor((10 - 1 + 1) * Math.random() + 1)) + 5; 
				var totalAngle = inAngle + itemCenter * 60 ;
				
				var selectedAngle = 0;
				switch('${resultPointRouletteJoin.result}'){
				case '1':
					selectedAngle = selectedAngle + 60 - 60 ;
				  break;
				case '2':
					selectedAngle = selectedAngle + 120 - 60 ;
				  break;
				case '3':
					selectedAngle = selectedAngle + 180 - 60 ;
				  break;
				case '4':
					selectedAngle = selectedAngle + 240 - 60 ;
				  break;
				case '5':
					selectedAngle = selectedAngle + 300 - 60 ;
				  break;
				case '6':
					selectedAngle = selectedAngle + 360 - 60 ;
				  break;
				}
				
		        formData = $("#mainform").serialize();

		        var dup_proc = $("#dup_proc").val();
				
		        if (dup_proc == "Y") {
		        	//modalPopup("중복참여");
		        	$("#resultImage").attr("src", "../images/roulette/error2.png");
					modalPopup();
		        	releaseContents();
		        	return;
		        }
				
		     // 룰렛먼저 돌리기 시작한다.
		        $("#image").rotate({
					angle: 0,
					animateTo: 360 * 5 + (360 - parseInt(totalAngle)) + selectedAngle,
					center: ["50%", "50%"],
					easing: $.easing.easeInOutElastic,
					callback: function () {
						var n = $(this).getRotateAngle();
					},
					duration: 10000
				});
		        
		        $.ajax({
		            type: "POST",
		            url: "../roulette/rouletteComplete.do",
		            data: formData,
		            dataType: "Json",
		            success: function (resData) {
		                //console.log(resData.result);  
		                if (resData.result == "success") {
							 $("#image").rotate({
								angle: 0,
								animateTo: 360 * 5 + (360 - parseInt(totalAngle)) + selectedAngle,
								center: ["50%", "50%"],
								easing: $.easing.easeInOutElastic,
								callback: function () {
									var n = $(this).getRotateAngle();
									endAnimate(n);
								},
								duration: 5000
							});
							 
		                }else if (resData.result == "max06Change") {
							 //alert("max06Change");
		                	$("#image").rotate({
								angle: 0,
								animateTo: 360 * 5 + (360 - parseInt(totalAngle)) + selectedAngle + 60,
								center: ["50%", "50%"],
								easing: $.easing.easeInOutElastic,
								callback: function () {
									var n = $(this).getRotateAngle();
									endAnimate(n, "max06Change");
								},
								duration: 5000
							});
			            } else if (resData.result == "complete") {
			            	$("#dup_proc").val("Y");
							
			            	$("#resultImage").attr("src", "../images/roulette/error2.png");
							modalPopup();
					        releaseContents();
					        
			            } else {
		                	$("#resultImage").attr("src", "../images/roulette/error1.png");
							modalPopup();
		                	releaseContents();
		                }
		                
		            },
		            error: function (e) {
		                //console.log("error:" + e.responseText);
		                //modalPopup("네트워크 오류가 발생하였습니다. 관리자에게 문의하세요(02-2008-7775)");
		                $("#resultImage").attr("src", "../images/roulette/error1.png");
						modalPopup();
		                releaseContents();
		            }
		        });
		    });
	     
	     
		$("#goMypoint").bind("click",function() {
			//$("#myPointForm").attr("action","/clip/mypoint/myPoint.do").attr("method", "post").submit();
			history.back();	
		});
		
		$("#goClose").bind("click",function() {
				$.modal.close();
		});
	
		$("#backBtn").bind("click",function() {
			if( "3" == $('#keytype').val() ){
				$(location).attr('href',$("#returnURL").val());
			}else{
				history.back();	
			}
		});
		
		function endAnimate($n, max06Message) {
			var n = $n;
			var real_angle = n % 360;// 이미지선을 중앙에
			var part = Math.floor((360 - real_angle) / 60);
			var strItem = "";

			if (part < 1) {
				strItem = pArr[0];
				part = 0;
			} else if (part >= 6) {
				strItem = pArr[pArr.length - 1];
				part = pArr.length - 1;
			} else {
				strItem = pArr[part];
			}

			if( "max06Change" == max06Message ){
				$("#resultImage").attr("src", "../images/roulette/win_0.png");
				modalPopup();
			}else if('${resultPointRouletteJoin.save_point}' == '0'){
				//modalPopup("꽝입니다! 내일 다시 도전해주세요.");
				$("#resultImage").attr("src", "../images/roulette/win_0.png");
				modalPopup();
			}else{
				if (strItem != "") {
					$("#dup_proc").val("Y");

					var imgString = "../images/roulette/win_";
					imgString = imgString + '${resultPointRouletteJoin.save_point}' + ".png";
					$("#resultImage").attr("src", imgString);
					
					modalPopup();
							
					//modalPopup("적립이 완료되었습니다.");
					//sendGift();
				}
			}
			$("#dup_proc").val("Y");
			
			releaseContents();
		}
	
	function modalPopup(msg) {
		if( null == msg ){
			$('#resultDiv').modal({
				opacity:80,
				overlayCss: {backgroundColor:"#fff"}
			});
			return;
		}else{
			
			$("#popupResultMsg").text(msg);
			$('#container').modal({
				opacity:80,
				overlayCss: {backgroundColor:"#fff"}
			});
			return;
		}
		
		
	}
	
	function blockContents(){
		releaseContents();
		var $div = $('<div id="progressDiv_z" style="position:fixed; top:0; left:0; width:100%; height:100%; z-index:1000;"></div>');
		$('body').append($div); 
	}
	
	function releaseContents(){
	   $('#progressDiv_z').remove();   
	}
		
	var mobilecheck = function () {
    var check = false;
    (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
    }
    if(mobilecheck()){
        
    }else{
    	
    	$("#titleDiv").hide();
    }
	
});
    
    
    
</script>



</head>
<body>


<div style="text-align:center">
 
		<!-- 타이틀 start -->
       <table id="titleDiv" name="titleDiv" style="width:100%; cellspace:0px; cellpadding:0px; border-spacing: 0px;">
            <tr>
	            <td style="width:15%; padding:0px 0px 0px 0px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img src="../images/roulette/title_01_${pointRoulette.imagepath}.png" style="width:100%; " alt="뒤로가기" id="backBtn" /></td>
	            <td style="width:85%; padding:0px 0px 0px 0px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img src="../images/roulette/title_02_${pointRoulette.imagepath}.png" style="width:100%; " alt="출석체크"/> </td>
	        </tr>
	         <tr >
	            <td colspan="2" style="height:3px; padding:0px 0px 0px 0px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img src="../images/roulette/title_03_${pointRoulette.imagepath}.png" style="width:100%; " alt="뒤로가기" id="backBtn" /></td>
	        </tr>
        </table>
        
        <!-- 타이틀 end -->
        <img src="../images/roulette/body_01_${pointRoulette.imagepath}.png" class="genie" alt="clip 매일매일 출석체크 이벤트! 매일 1번씩 클립포인트 당첨의 행운을 잡으세요!!!"/>
		<table style="background-image:url(../images/roulette/body_02_${pointRoulette.imagepath}.png);background-size: 100% 100%;background-position: center; background-repeat: no-repeat;width:100%; z-index:1" >
		    <tr>
			    <td>
			        <table style="background-image:url(../images/roulette/body_03_${pointRoulette.imagepath}.png);background-size:83% 97%;background-position: center; background-repeat: no-repeat;width:100%; z-index:2" >
			            <tr>
				            <td style="text-align:center; padding: 10% 10% 10% 10%;">
                               <img src="../images/roulette/body_04_${pointRoulette.imagepath}.png" id="image" name="image" style="z-index:1;width:83%;" alt="꽝! 10포인트 10000포인트 1포인트 2000포인트 5포인트"/></td>
			            </tr>
			        </table>
			    </td>
		    </tr>
		</table>
	</div>

	<div>
		<img src="../images/roulette/body_05_${pointRoulette.imagepath}.png" class="genie" alt="CLIP 포인트"/><img src="../images/roulette/body_06_${pointRoulette.imagepath}.png" class="genie" alt="꼭 확인하세요! *출석체크 프로모션은 1일 1회 참여 가능합니다. * 비정상적이거나 불법적인 방법으로 이벤트에 참여하신 경우 당첨내역이 취소될 수 있습니다. * 경품부족 시 다른경품으로 대체하여 지급 될 수 있습니다. * 본 이벤트는 운영 사정에 따라 사전 고지없이 종료 될 수 있습니다."/>
	</div>
<!-- <a href="javascript:showNoti('o');" id="notiA"><img src="../images/roulette/_04.png" class="genie" border="0" alt="이벤트 유의사항 보기"/></a> -->
<form name="mainform" id="mainform" method="post" >
	<input type="hidden" name="roulette_id"  id="roulette_id"  value="${resultPointRouletteJoin.roulette_id}"/>
	<input type="hidden" name="keytype" id="keytype" value="${rouletteDto.keytype}"/>
	<input type="hidden" name="keyid" id="keyid" value="${rouletteDto.keyid}"/>
	<input type="hidden" name="cust_id" id="cust_id" value="${resultPointRouletteJoin.cust_id}"/>
	<input type="hidden" name="ga_id" id="ga_id" value="${resultPointRouletteJoin.ga_id}"/>
	<input type="hidden" name="ctn" id="ctn" value="${resultPointRouletteJoin.ctn}"/>
	<input type="hidden" name="user_ci" id="user_ci" value="${resultPointRouletteJoin.user_ci}"/>
	<input type="hidden" name="sdate" id="sdate" value="${resultPointRouletteJoin.sdate}"/>
	<input type="hidden" name="tr_id" id="tr_id" value="${resultPointRouletteJoin.tr_id}"/>
	<input type="hidden" name="returnURL" id="returnURL" value="${rouletteDto.returnURL}"/>
	
    <input type="hidden" name="dup_proc" id="dup_proc" value="N"/>
</form>

<form name="myPointForm" id="myPointForm" method="post" >
	<input type="hidden" name="cust_id" id="cust_id" value="${resultPointRouletteJoin.cust_id}"/>
	<input type="hidden" name="ga_id" id="ga_id" value="${resultPointRouletteJoin.ga_id}"/>
	<input type="hidden" name="ctn" id="ctn" value="${resultPointRouletteJoin.ctn}"/>
</form>



	
<div  id='container' style="display:none; padding: 10 10 10 10;" >
<br>
<br>
<br>
<br>
<br>
<br>
<br>

			    <div id='popupResultMsg' style=" font-size:13px; text-align:center;"></div> 
</div>

<div  id='resultDiv' style="display:none; ">
	<img id="resultImage" name="resultImage" height="84%" style="display:block; margin:0 auto;"/>

       <table style="cellspace:0px; cellpadding:0px; border-spacing: 0px;">
            <tr>
	            <td style="width:50%; padding:0px 0px 0px 0px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img src="../images/roulette/btn_point.png" style="width:100%; " alt="포인트보기" id="goMypoint" /></td>
	            <td style="width:50%; padding:0px 0px 0px 0px;cellspace:0px; cellpadding:0px; border-spacing: 0px;"><img src="../images/roulette/btn_close.png" style="width:100%; " alt="닫기"  id="goClose" /> </td>
	        </tr>
        </table>
        
</div>



<!-- Load jQuery, SimpleModal and Basic JS files -->
<script type='text/javascript' src='../js/common/jquery.simplemodal.js'></script>
<script type='text/javascript' src='../js/common/basic.simplemodal.js'></script>



</body>
</html>
