<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLiP MyPoint 페이지</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/pointCommon.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
<!-- bx slider lib -->
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script>


<!-- to-be -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>

<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">

<script type="text/javascript">
	$(function() {
		/* common.invoker.invoke("myPoint"); */
		$(document).ready(function(){
			
			getPoint();
			goSearch();
			
			
		});
		
		
	});	
	
	
	var getPoint = function () {
		console.log("url : "+myPoint.functionURL2);
		var user_ci = $("#user_ci").val();
		var cust_id = $("#cust_id").val();
		var ctn = $("#ctn").val();
		var gaid = $("#gaid").val();
		var mid = $("#mid").val();
		var param = {
				"user_ci"				: user_ci,
				"cust_id"				: cust_id,
				"ctn"				: ctn,
				"gaid"				: gaid,
				"mid"				: mid
		};
		var	opts = {
				url		: myPoint.functionURL2,
				data	: param,
				type	: "post",
				sendDataType	: "json",
				success	: function(resJSON, resCode) {
					console.log(resJSON);
					if (resCode== "success") {
						var balance = myPoint.fn.addComma(resJSON.data.balance);
						var description = myPoint.fn.addComma(resJSON.data.description);
						if(balance == "-1"){
							alert("사용가능 포인트 조회중 오류가 발생하였습니다.\n 오른쪽 위 새로고침 버튼을 눌러 주세요.");
						}else{
							$("#mypoint").empty();
							$("#mypoint").append(balance);							
							
							if("" != description ){
								alert(description);
							}
							
							/*point history 조회*/
					    	/*myPoint.fn.getPointHistory();*/
							
						}
					}else{	
						alert("오류가 발생하였습니다.");
					}
				}
				
			};
			common.http.ajax(opts);
	}
	
	
	var goSearch = function(){

		var paramData = {
				"user_ci" : $("#user_ci").value
		};	
		
	    $.ajax({
	    	url: "/clip/pointcollect/getCardPoints.do",
	        type: "POST",
	        data: JSON.stringify(paramData),
	        dataType: "json",
	        contentType: 'application/json; charset=UTF-8',
	        success: function (resData) {
	        	
	            poInfo = resData.pointInfo;
	            
	            console.log("---------------------");
	            console.log(poInfo);
	            console.log("---------------------");
	            
/* 	            var hanaPoint = comma(poInfo.hanaPoint.point);
	            var kbPoint = comma(poInfo.kbPoint.point);
	            var shinhanPoint = comma(poInfo.shinhanPoint.point); */
	            var hanaPoint = 50;
	            var kbPoint = 100;
	            var shinhanPoint = 150;
	            var sumPoint = 0;
	            
	            if(hanaPoint != null && hanaPoint != '' ){
	            	$(".hanaPoint").text(hanaPoint);
	            	$(".hana .ip_point_box").find("em").text(hanaPoint);
	            	$(".hana .points").val(uncomma(hanaPoint));
	            	sumPoint = Number(sumPoint) + Number(hanaPoint);
	            }
	            if(kbPoint != null &&  kbPoint != '' ){
	            	$(".kbPoint").text(kbPoint);
	            	$(".kb .ip_point_box").find("em").text(kbPoint);
	            	$(".kb .points").val(uncomma(kbPoint));
	            	sumPoint = Number(sumPoint) + Number(kbPoint);
	            }
	            if(shinhanPoint != null && shinhanPoint != '' ){
	            	$(".shinhanPoint").text(shinhanPoint);
	            	$(".shinhan .ip_point_box").find("em").text(shinhanPoint);
	            	$(".shinhan .points").val(uncomma(shinhanPoint));
	            	sumPoint = Number(sumPoint) + Number(shinhanPoint);
	            }
	            
	            sumPoint = common.string.setComma(sumPoint);
	            
	            $(".sumPoint").text(sumPoint); 
	            $(".chgPoint").text(sumPoint); 
	            console.log("hanaPoint : "+hanaPoint+" kbPoint : "+kbPoint+" shinhanPoint : "+shinhanPoint);
	            
	            
	            
	        },
	        beforeSend: function () {
				alert(this.data);
	        },
	        error: function (e) {
	            console.log("error:" + e.responseText);
	            alert("네트워크 오류가 발생하였습니다.");
	        }
	    });
	}
	
	var goSwap = function(){

		 
		var paramData = {
			"user_ci" : $("#user_ci").val(),
			"cardNames" : [],
			"points" : []
		};	
		
		var cardNames = [];
		var points = [];
		
		$(".contents .ip_point_box .cardNames").each(function(){
			
			cardNames.push($(this).val());
				
		}); 
		$(".contents .ip_point_box .points").each(function(){
			
			points.push($(this).val());
				
		});
		
		paramData.cardNames = cardNames;
		paramData.points = points;
	
		var	opts = {
	    	url: "/clip/pointcollect/swapCardPoints.do",
	    	data	: paramData,
			type	: "post",
			sendDataType	: "json",
	        contentType: 'application/json; charset=UTF-8',
	        success: function (resData) {
	            console.log("resData : "+resData);
	        },
	        beforeSend: function () {
				alert(this.data);
	        },
	        error: function (e) {
	            console.log("error:" + e.responseText);
	            alert("네트워크 오류가 발생하였습니다.");
	        }
	    };
		common.http.ajax(opts);
	}
	
	function fnPopupSetting(obj){
	
		$("#confirmSetting").attr("data",obj);
		
		$("#"+obj+"PopDiv").show();
		
		layerAct.open('layer_pop_wrap');
	}
	
	function fnSetting(obj){
		if(obj == 'Y'){
			var data = $("#confirmSetting").attr("data");
			var point = $("#"+data+"PopDiv .ip_point_box").find("input").val();
			
			var allPoint = $("#"+data+"PopDiv .cash").text();
			
			console.log(Number(point) > Number(allPoint) + "point : "+point+" allPoint : " +allPoint);
			
			if(Number(point) > Number(allPoint)){
				alert("보유 포인트를 초과하였습니다.");
			
				return;
			}
			
			console.log("data : "+data+"  point : "+point);
			
			$("."+data+" .ip_point_box").find("em").text(point);
			$("."+data+" .points").val(uncomma(point));
			
			
			var chgPoint = 0;
			
			//전환포인트 합계 변경
			$(".contents .ip_point_box").find("em").each(function(){
					
				chgPoint = Number(chgPoint) + Number(uncomma($(this).text()));
					
			}); 
			
			//콤마 TODO
			chgPoint = common.string.setComma(chgPoint);
			
			$(".chgPoint").text(chgPoint);
		}
		
		$(".layer_pop").hide();
		
		layerAct.close('layer_pop_wrap')
	}
	
	function fnPointSwap(){
		goSwap();
	}
	
	
	/* ----------------------------------공통-------------------------------------- */
	function fnClear(obj){
		obj.value = '';
	}
	
	
	function inputNumberFormat(obj){
		obj.value = comma(uncomma(obj.value));
	}
		

	function comma(str) {
	    str = String(str);
	    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	}

	//콤마풀기
	function uncomma(str) {
	    str = String(str);
	    return str.replace(/[^\d]+/g, '');
	    
	}
	

	
	

</script>
</head>
<body>

<div class="wrap">
	<form id="mainForm" name="mainForm" action="">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden"  id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden"  id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden"  id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden"  id="mid" name="mid" value="${mid}"/>
	<input type="hidden"  id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden"  id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden"  id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden"  id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>

	<!-- header -->
	<div class="header">
		<h1 class="page_title">포인트 전환</h1>
		<a href="#" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<div class="contents">
		<div class="section">
			<h2 class="clippoint_top_title">
				분산되어 있는 신용카드 포인트를 모아 모아 <br>
				클립포인트로 전환하세요!
			</h2>
			<!-- CLIP 포인트 -->
			<a href="#" class="clippoint_box">
				<span class="title">CLIP 포인트</span>
				<strong id="mypoint" class="point usepointValue"><span>P</span></strong>
				<!-- <span id="mypoint" class="usepointValue">0</span> -->
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<!--  신용카드/멤버십 포인트  -->
		<div class="section">
			<div class="section_inner">
				<h2 class="section_title">신용카드/멤버십 포인트</h2>
				<!--  신한카드  -->
				<div class="card_point_box shinhan">
					<span class="card_name card_shihan"><img src="../images/temp/card_shihan.png" alt="ShinganCard"></span>
					<div class="point_area">
						<div class="inr_top">
							<span class="tit">보유 포인트</span>
							<em class="cash shinhanPoint">0<span>P</span></em>
						</div>
						<div class="inr_bot">
							<span class="tit">전환 포인트</span>
							<div class="ip_point_box" onclick="javascript:fnPopupSetting('shinhan');">
								<em>0</em>
								<span>P</span>
								<input type="hidden" class="cardNames" name="cardNames" value="shinhan" /> 
								<input type="hidden" class="points" name="point" value="" /> 
							</div>
						</div>
					</div>
				</div>
				<!--  // 신한카드  -->
				<!-- 하나카드 -->
				<div class="card_point_box hana">
					<span class="card_name card_hana"><img src="../images/temp/card_hana.png" alt="하나카드"></span>
					<div class="point_area">
						<div class="inr_top">
							<span class="tit">보유 포인트</span>
							<em class="cash hanaPoint">0<span>P</span></em>
						</div>
						<div class="inr_bot">
							<span class="tit">전환 포인트</span>
							<div class="ip_point_box" onclick="javascript:fnPopupSetting('hana');">
								<em>0</em>
								<span>P</span>
								<input type="hidden" class="cardNames" name="cardNames" value="hanamembers" /> 
								<input type="hidden" class="points" name="point" value="" /> 
							</div>
						</div>
					</div>
				</div>
				<!-- // 하나카드 -->
				<!-- KB국민카드 -->
				<div class="card_point_box kb">
					<span class="card_name card_kb"><img src="../images/temp/card_kb.png" alt="KB국민카드"></span>
					<div class="point_area">
						<div class="inr_top">
							<span class="tit">보유 포인트</span>
							<em class="cash kbPoint">0<span>P</span></em>
						</div>
						<div class="inr_bot">
							<span class="tit">전환 포인트</span>
							<div class="ip_point_box" onclick="javascript:fnPopupSetting('kb');">
								<em>0</em>
								<span>P</span>
								<input type="hidden" class="cardNames" name="cardNames" value="ktpointree" /> 
								<input type="hidden" class="points" name="point" value="" /> 
							</div>
						</div>
					</div>
				</div>
				<!-- // KB국민카드 -->
				<!-- NH농협카드 -->
				<div class="card_point_box">
					<span class="card_name card_nh"><img src="../images/temp/card_nh.png" alt="NH농협카드"></span>
					<a href="#" class="btn_srh">조회하기</a>
				</div>
				<!-- //NH농협카드 -->
				<!-- LOTTE CARD -->
				<div class="card_point_box">
					<span class="card_name card_lotte"><img src="../images/temp/card_lotte.png" alt="CARD"></span>
					<a href="#" class="btn_srh">조회하기</a>
				</div>
				<!-- //LOTTE CARD -->
				<div class="point_two_wrap">
					<div class="point_two_box">
						<div class="inr">
							<span class="tit">보유 포인트 합계</span>
							<strong class="cash sumPoint">0<span>P</span></strong>
						</div>
						<div class="inr">
							<span class="tit">전환 포인트 합계</span>
							<strong class="cash chgPoint">0<span>P</span></strong>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--  //신용카드/멤버십 포인트  -->
		<div class="section">
			<div class="section_inner">
				<!-- 포인트 전환 -->
				<div class="point_transform_wrap">
					<p class="top_title">
						<strong class="point chgPoint">0<span>P</span></strong>를 클립포인트로<br>
						전환 하시겠습니까?
					</p>
					<div class="btn_multi_box">
						<button type="button" class="btn_lp_cancel">취소</button>
						<button type="button" class="btn_confirm" onclick="javascript:fnPointSwap('Y');">확인</button>
					</div>
					<span class="tit">TIP 암호잠금</span>
					<p class="cont_text">
						핸드폰 분실시 고객님의 정보 및 포인트를 안전하게 지키기 위해 
						앱 잠금 서비스를 제공하고있습니다.
					</p>
					<a href="#" class="btn_view">앱 잠금설정 하기</a>
				</div>
				<!-- // 포인트 전환 -->
			</div>
		</div>
	</div><!-- // contents -->
	</form>
	<!--  popup 포인트 전환 -->
	<!-- <div class="layer_pop_wrap">
		<div class="deem"></div>
		<div class="layer_pop">
			<div class="lp_hd">
				<strong class="title">포인트 전환</strong>
			</div>
			<div class="lp_ct">
				point_tf_cont
				<div class="point_tf_cont" id="shinhanPopDiv" style="display:none;">
					<div class="inr">
						<span class="card_name card_shihan"><img src="../images/temp/card_shihan.png" alt="ShinganCard"></span>
						<span class="cash shinhanPoint">0<span>P</span></span>
						<div class="ip_point_box">
							<input type="text" value="0" title="전환포인트" onkeyup="inputNumberFormat(this);" onclick="fnClear(this);" >
							<button type="button" class="btn_init" style="display: block;">삭제</button>
						</div>
					</div>
				</div>
				<div class="point_tf_cont" id="hanaPopDiv" style="display:none;">
					<div class="inr">
						<span class="card_name card_hana"><img src="../images/temp/card_hana.png" alt="하나카드"></span>
						<span class="cash hanaPoint">0<span>P</span></span>
						<div class="ip_point_box">
							<input type="text" value="0" title="전환포인트" onkeyup="inputNumberFormat(this);" onclick="fnClear(this);">
							<button type="button" class="btn_init" style="display: block;">삭제</button>
						</div>
					</div>
				</div>
				<div class="point_tf_cont" id="kbPopDiv" style="display:none;">
					<div class="inr">
						<span class="card_name card_kb"><img src="../images/temp/card_kb.png" alt="KB국민카드"></span>
						<span class="cash kbPoint">0<span>P</span></span>
						<div class="ip_point_box">
							<input type="text" value="0" title="전환포인트" onkeyup="inputNumberFormat(this);" onclick="fnClear(this);">
							<button type="button" class="btn_init" style="display: block;">삭제</button>
						</div>
					</div>
				</div>
				// point_tf_cont
				<div class="btn_multi_box">
					<button type="button" class="btn_lp_cancel" onclick="javascript:fnSetting('N');">취소</button>
					<button type="button" class="btn_lp_confirm" id="confirmSetting" onclick="javascript:fnSetting('Y');" >확인</button>
				</div>
			</div>
		</div>
	</div> -->
	<!--  popup 포인트 전환 -->
	<div class="layer_pop_wrap">
		<div class="deem"></div>
		<div class="layer_pop" id="shinhanPopDiv" style="display:none;">
			<div class="lp_hd">
				<strong class="title"><img src="../images/temp/card_shihan.png" alt="ShinganCard" class="sinhancard"></strong>
			</div>
			<div class="lp_ct">
				<!-- point_tf_cont -->
				<div class="point_tf_cont">
					<div class="inr">
						<span class="txt">전환 가능 포인트</span>
						<span class="cash shinhanPoint">0<span>P</span></span>
						<div class="ip_point_box">
							<input type="text" value="0" title="전환포인트" onkeyup="inputNumberFormat(this);" onclick="fnClear(this);" >
							<button type="button" class="btn_init">삭제</button>
						</div>
					</div>
				</div>
				<!-- // point_tf_cont -->
				<div class="btn_multi_box">
					<!-- <button type="button" class="btn_lp_cancel" onclick="javascript:layerAct.close('layer_pop_wrap');">취소</button>[D]2017.07.14 수정
					<button type="submit" class="btn_lp_confirm">확인</button> -->
					<button type="button" class="btn_lp_cancel" onclick="javascript:fnSetting('N');">취소</button>
					<button type="button" class="btn_lp_confirm" id="confirmSetting" onclick="javascript:fnSetting('Y');" >확인</button>
				</div>
			</div>
		</div>
		<div class="layer_pop" id="hanaPopDiv" style="display:none;">
			<div class="lp_hd">
				<strong class="title">
				<img src="../images/temp/card_hana.png" alt="하나카드" class="hanacard">
				</strong>
			</div>
			<div class="lp_ct">
				<!-- point_tf_cont -->
				<div class="point_tf_cont">
					<div class="inr">
						<span class="txt">전환 가능 포인트</span>
						<span class="cash hanaPoint">0<span>P</span></span>
						<div class="ip_point_box">
							<input type="text" value="0" title="전환포인트" onkeyup="inputNumberFormat(this);" onclick="fnClear(this);">
							<button type="button" class="btn_init" style="display: block;">삭제</button>
						</div>
					</div>
				</div>
				<!-- // point_tf_cont -->
				<div class="btn_multi_box">
					<button type="button" class="btn_lp_cancel" onclick="javascript:fnSetting('N');">취소</button>
					<button type="button" class="btn_lp_confirm" id="confirmSetting" onclick="javascript:fnSetting('Y');" >확인</button>
				</div>
			</div>
		</div>
		<div class="layer_pop" id="kbPopDiv" style="display:none;">
			<div class="lp_hd">
				<strong class="title">
				<img src="../images/temp/card_kb.png" alt="KB국민카드" class="sinhancard">
				</strong>
			</div>
			<div class="lp_ct">
				<!-- point_tf_cont -->
				<div class="point_tf_cont">
					<div class="inr">
						<span class="txt">전환 가능 포인트</span>
						<span class="cash kbPoint">0<span>P</span></span>
						<div class="ip_point_box">
							<input type="text" value="0" title="전환포인트" onkeyup="inputNumberFormat(this);" onclick="fnClear(this);">
							<button type="button" class="btn_init" style="display: block;">삭제</button>
						</div>
					</div>
				</div>
				<!-- // point_tf_cont -->
				<div class="btn_multi_box">
					<button type="button" class="btn_lp_cancel" onclick="javascript:fnSetting('N');">취소</button>
					<button type="button" class="btn_lp_confirm" id="confirmSetting" onclick="javascript:fnSetting('Y');" >확인</button>
				</div>
			</div>
		</div>
	</div>
	<!--  //popup 포인트 전환 -->

</div><!-- // wrap -->
</body>
</html>