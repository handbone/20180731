<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- <script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/> 
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> -->
<!-- bx slider lib -->


<!-- to-be -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>

<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">

<script type="text/javascript">
	
</script>
</head>
<body>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">포인트 전환</h1>
		<a href="#" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<!-- CLIP 포인트 -->
			<a href="#" class="clippoint_box">
				<span class="title">CLIP 포인트</span>
				<strong class="point">37,200<span>P</span></strong>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<div class="section">
			<div class="section_inner">
				<!-- 포인트 전환 -->
				<div class="point_transform_wrap">
					<p class="top_title">
						<strong class="point">13,000<span>P</span></strong>가 성공적으로<br>
						전환 되었습니다.
					</p>
					<!-- [D]2017.07.14 수정-->
					<ul class="point_transform_list">
						<li>
							<span class="card_name card_shihan"><img src="../images/temp/card_shihan.png" alt="ShinganCard"></span>
							<strong id="shinhanPoint">13,000<i>P</i></strong>
						</li>
						<li>
							<span class="card_name card_hana"><img src="../images/temp/card_hana.png" alt="하나카드"></span>
							<strong id="hanaPoint">13,000<i>P</i></strong>
						</li>
						<li>
							<span class="card_name card_kb"><img src="../images/temp/card_kb.png" alt="KB국민카드"></span>
							<strong id="kbPoint">13,000<i>P</i></strong>
							<em>포인트 전환 실패<button type="button"></button></em>
						</li>
					</ul>
					<!-- //[D]2017.07.14 수정-->
					<a href="#" class="btn_point_more">포인트 더 모으러 가기!</a>
					<a href="#" class="btn_point_use">포인트 사용하러 가기!</a>
					<!-- [D]2017.07.14 수정-->
					<p class="point_info">
						포인트 전환 실패의 경우 보유포인트가 부족하거나 서버 통신이 원활하지 않을 수 있습니다. 잠시 후 다시 시도해 주세요
					</p>
					<!-- //[D]2017.07.14 수정-->
				</div>
				<!-- // 포인트 전환 -->
			</div>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->

</body>
</html>