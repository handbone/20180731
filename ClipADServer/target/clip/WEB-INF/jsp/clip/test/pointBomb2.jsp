<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>CLIP</title>
	
	<!-- 이전 CLIP JS 및 CSS 파일
	<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="../js/common/common.js"></script>
	<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
	<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
	bx slider lib
	<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> 
	-->
	
	<!-- to-be  -->
	<link rel="stylesheet" href="../css/lib/swiper.min.css">
	<link rel="stylesheet" href="../css/common.css?20170905">
</head>
<body>
<form id="form" name="form" action="<c:url value="/pointmain/main.do" />">
<!-- user_ci : <input type="text"  id="text_user_ci" name="text_user_ci" value=""/><br/> -->
	<input type="hidden" id="cust_id" name="cust_id" value="${cust_id}"/>
	<input type="hidden" id="ctn" name="ctn" value="${ctn}"/>
	<input type="hidden" id="gaid" name="gaid" value="${gaid}"/>
	<input type="hidden" id="mid" name="mid" value="${mid}"/>
	<input type="hidden" id="user_ci" name="user_ci" value="${user_ci}"/>
	<input type="hidden" id="app_ver" name="app_ver" value="${app_ver}"/>
	<input type="hidden" id="offerwall" name="offerwall" value="${offerwall}"/>
	<input type="hidden" id="lockScreen_on_off" name="lockScreen_on_off" value="0"/>
	
	<input type="hidden" id="pageType" name="pageType" value="" />
</form>
<!-- wrap -->
<div class="wrap noheader">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">랜덤 포인트 폭탄</h1>
		<a href="#" class="btn_back" onclick="javascript:history.back()"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="bomb_wrap other_bg">
			<div class="bomb_top fixed_top">
				<img src="../images/bomb/bomb_top.png" class="img_top_txt" alt="3일마다 팡팡 터지는 랜덤 포인트 폭탄!">
			</div>
			<!-- 당첨 -->
			<div class="bomb_win_box">
				<div class="inner">
					<p class="win_txt"><strong>330<span>P</span></strong>에<br>당첨되셨습니다!</p>
					<div class="exp_point_box">
						<span class="exp_point">폭탄포인트<em>300<span>P</span></em></span>
						<span class="ico_plus">플러스</span>
						<span class="exp_point">레벨포인트<em>30<span>P</span></em></span>
					</div>
				</div>
			</div>
			<!-- // 당첨 -->
			<div class="bomb_bot fixed_bot">
				<div class="btn_wrap">
					<a href="#" class="btn_point_save" onclick="javascript:fn.btnAct.redirect('0')">포인트 더모으러 가기!</a>
					<a href="#" class="btn_point_use" onclick="javascript:fn.btnAct.redirect('1')">포인트 사용하러 가기!</a>
				</div>
			</div>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
<script>
	var fn = {
			btnAct : {
				redirect : function(pageType) {
					$('#pageType').val(pageType);
					$('#form').submit();
				}
			}
	}
</script>
</body>
</html>