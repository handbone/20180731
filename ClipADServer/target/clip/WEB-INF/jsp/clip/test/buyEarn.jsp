<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<title>CLIP</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
<!-- 이전 CLIP JS 및 CSS 파일
<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
bx slider lib
<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> 
-->


<!-- to-be -->
<!-- <link rel="stylesheet" href="../css/jquery.bxslider.css"/> -->
<link rel="stylesheet" href="../css/lib/swiper.min.css">
<link rel="stylesheet" href="../css/common.css?20170905">

<script type="text/javascript">
	
</script>
</head>
<body>
<!-- wrap -->
<div class="wrap">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">구매 적립</h1>
		<a href="#" class="btn_back"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="section">
			<h2 class="clippoint_top_title">
				구매하면 포인트가 덤!<br>
				구매하고 클립포인트도 모으세요!
			</h2>
			<!-- CLIP 포인트 -->
			<a href="#" class="clippoint_box">
				<span class="title">CLIP 포인트</span>
				<strong class="point">37,200<span>P</span></strong>
			</a>
			<!-- // CLIP 포인트 -->
		</div>
		<!--  적립 예정 포인트  -->
		<div class="section">
			<div class="section_inner">
				<div class="point_two_wrap">
					<h2 class="section_title">적립 예정 포인트</h2>
					<div class="point_two_box type_color">
						<div class="inr">
							<span class="tit">익월 적립</span>
							<strong class="cash">1,200<span>P</span></strong>
						</div>
						<div class="inr">
							<span class="tit">익익월 적립</span>
							<strong class="cash">300<span>P</span></strong>
						</div>
					</div>
					<a href="#" class="btn_view">내역보기</a>
				</div>
			</div>
		</div>
		<!--  //적립 예정 포인트  -->
		<div class="section">
			<div class="section_inner2">
				<ul class="point_list_wrap">
					<c:forEach var="sampleList" begin="1" end="8">
						<li>
							<a href="#" onclick="javascript:layerAct.open('layer_pop_wrap');">
								<img src="../images/temp/@temp_point1.png" alt="">
								<span><strong>4%</strong> 적립</span>
							</a>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div><!-- // contents -->
	
	<!--  popup -->
	<div class="layer_pop_wrap pop_point">
		<div class="deem"></div>
		<div class="layer_pop">
			<div class="lp_hd">
				<strong class="title auction"><img src="../images/temp/brand_auction.png" alt=""></strong>
				<span>5% 적립</span>
			</div>
			<div class="lp_ct">
				<!-- point_tf_cont -->
				<div class="point_tf_cont">
					<div class="inr ex_txt">
						KT CLIP을 통해 쇼핑몰을 방문해서 상품을
						구매하면 추가로 포인트를 적립해 드립니다.<br><br>

						<strong>[적립 제외 상품 및 유의사항]</strong>
						상품권/도서/음반/공연/영화/여행/항공권/
						숙박/보험상품/교육/특정 이벤트 상품은 적립
						대상에서 제외됩니다.
					</div>
				</div>
				<!-- // point_tf_cont -->
				<div class="btn_multi_box">
					<button type="button" class="btn_lp_cancel" onclick="javascript:layerAct.close('layer_pop_wrap');">취소</button>
					<button type="submit" class="btn_lp_confirm">쇼핑하러가기</button>
				</div>
			</div>
		</div>
	</div>
	<!--  //popup -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
</body>
</html>