//170804 수정 스크롤 막음 제거 원복
/* 레이어 동작 */
var layerAct ={
    open:function(obj){
        $('.'+obj).show();
        $('.'+obj).on('scroll touchmove mousewheel', function(event) {
           event.preventDefault();
           event.stopPropagation();
           return false;
        });
        $('.deem').on('scroll touchmove mousewheel', function(event) {
           event.preventDefault();
           event.stopPropagation();
           return false;
        });
    },
    close:function(obj){
        $('.'+obj).hide();
        $('.deem').off('scroll touchmove mousewheel');
    }
}

//170721 수정
/* 바코드 */
var barcode ={
    init:function(){
        this.btn = $('.btn_point_pay');
        this.addEvent();
    },
    addEvent:function(){
        var _this = this;
        this.btn.on('click',function(){
            var $this = jQuery(this);
            $this.hide().next().show();
        });
    }
}
$('.btn_point_pay').length && barcode.init(); 

//170719 수정
/* 탭  */
var tabAct = {
    init:function(){
        this.$tab_tg1 = jQuery('.tab_act li');
        this.$tab_tg2 = jQuery('.tab_con');
        this.$tab_tg2.eq(1).css('visibility','hidden');
        this.$body = $('body');
        this.tabEvent();
    },
    tabEvent:function(){
        var _this = this;
        this.$tab_tg1.click(function(e){
            var $this = jQuery(this);
            var $index = $this.index();
            var $bg = $this.attr('data-bg');
            console.log($bg);
            tabAct.tabHide($index);
            $this.addClass('active');
            if($bg === 'bg_on'){
                _this.$body.addClass('bg_on');
            }else{
                _this.$body.removeClass('bg_on');
            }
        });       
    },
    tabHide:function(num){
        this.$tab_tg1.removeClass('active')
        this.$tab_tg2.hide();
        this.$tab_tg2.eq(num).show();
        this.$tab_tg2.eq(1).css('visibility','visible');
    }
}
tabAct.init();

/* 전환 가능 포인트 input */
var pointBox ={
    init:function(){
        this.wrap = $('.ip_point_box');
        this.input = this.wrap.find('input');
        this.init = this.wrap.find('.btn_init');

        this.addEvent();
    },
    addEvent:function(){
        var _this = this;
        var str = this.input.val().length;
        if(str > 0){
            this.init.show();
        }else{
            this.init.hide();
        }
        this.init.on('click',function(){
            var $this = $(this);
            $this.hide();
            _this.input.val("");
        });
        this.input.focusout(function() { 
            if(str > 0){
                _this.init.show();
            }
        });
    }
}
$('.ip_point_box').length && pointBox.init(); 

//1700808 변경
/* 클립포인트 메인 하단 배너 슬라이더 */
var adArea = {
        adAreaSliderEv:function(){
            if($('.ad_area_slider li').length  > 1) {
                var adAreaSlider = new Swiper('.ad_area_slider', {
                    pagination: '.ad_area_slider .pagination_n',
                    paginationClickable: true,
                    loop: true,
                    autoplayDisableOnInteraction: false,
                    autoplay: 3000
                });
            };
        }
    };
//	$('.ad_area_slider').length && adArea.adAreaSliderEv();
    

//170719 추가
/* 클립포인트 메인 tab fixed */
function TopFixed(obj){
    var TopFixed = jQuery('.'+obj);

    var h = 155;

    jQuery(window).scroll(function () {
        if(parseInt(jQuery(window).scrollTop()) > h){
            TopFixed.addClass('jbFixed');
        }else{
        TopFixed.removeClass('jbFixed');
        }
    });

}
$('.earn_tab').length && TopFixed('earn_tab');

//170728 추가
//170803 수정 스크롤 막음
/* 로딩*/
var loadingClip ={
    openClip:function(){
        $('.loading_clip').show();
        $('.loading_clip').on('scroll touchmove mousewheel', function(event) {
           event.preventDefault();
           event.stopPropagation();
           return false;
        });
    },
    closeClip:function(){
        $('.loading_clip').hide();
        $('.loading_clip').off('scroll touchmove mousewheel');
    }
}